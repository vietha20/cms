-- phpMyAdmin SQL Dump
-- version 4.2.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 22, 2014 at 11:08 AM
-- Server version: 10.0.12-MariaDB-log
-- PHP Version: 5.5.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `i8ul8r_cms`
--

DROP TABLE IF EXISTS `tbl_authassignment`;
DROP TABLE IF EXISTS `tbl_authitemchild`;
DROP TABLE IF EXISTS `tbl_authitem`;
DROP TABLE IF EXISTS `tbl_comment`;
DROP TABLE IF EXISTS `tbl_tag`;
DROP TABLE IF EXISTS `tbl_lookup`;
DROP TABLE IF EXISTS `tbl_lyric`;
DROP TABLE IF EXISTS `tbl_post`;
DROP TABLE IF EXISTS `tbl_category`;
DROP TABLE IF EXISTS `tbl_friend`;
DROP TABLE IF EXISTS `tbl_message`;
DROP TABLE IF EXISTS `tbl_user`;
DROP TABLE IF EXISTS `tbl_role`;
DROP TABLE IF EXISTS `tbl_album`;
DROP TABLE IF EXISTS `tbl_gallery`;
-- DROP TABLE IF EXISTS `tbl_airport`;
-- DROP TABLE IF EXISTS `tbl_airline`;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_authassignment`
--

CREATE TABLE IF NOT EXISTS `tbl_authassignment` (
  `itemname` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `userid` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `bizrule` text COLLATE utf8_unicode_ci,
  `data` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_authassignment`
--

INSERT INTO `tbl_authassignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
  ('admin', '1', NULL, 'N;'),
  ('admin', '2', NULL, 'N;'),
  ('editor', '3', NULL, 'N;'),
  ('editor', '4', NULL, 'N;'),
  ('user', '10', NULL, 'N;'),
  ('user', '5', NULL, 'N;'),
  ('user', '6', NULL, 'N;'),
  ('user', '7', NULL, 'N;'),
  ('user', '8', NULL, 'N;'),
  ('user', '9', NULL, 'N;');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_authitem`
--

CREATE TABLE IF NOT EXISTS `tbl_authitem` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `bizrule` text COLLATE utf8_unicode_ci,
  `data` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_authitem`
--

INSERT INTO `tbl_authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
  ('admin', 2, 'Administrators can do anything but must have roleid=1 in user table', 'return !Yii::app()->user->isGuest && Yii::app()->user->roleid==1;', 'N;'),
  ('editor', 2, 'Only allow to add/edit/delete category, comments, posts', 'return !Yii::app()->user->isGuest && Yii::app()->user->roleid==3;', 'N;'),
  ('guest', 2, 'Visitors are allowed to do a certain actions only', 'return Yii::app()->user->isGuest;', 'N;'),
  ('updateSelf', 1, 'If own id matches then the person are allowed to perform the action', 'return Yii::app()->user->getId()==$params["userid"];', 'N;'),
  ('user', 2, 'Authenticated users are allowed for instance post comments without approval', 'return !Yii::app()->user->isGuest && in_array(Yii::app()->user->roleid, array(1,2,3));', 'N;');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_authitemchild`
--

CREATE TABLE IF NOT EXISTS `tbl_authitemchild` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_authitemchild`
--

INSERT INTO `tbl_authitemchild` (`parent`, `child`) VALUES
  ('admin', 'user'),
  ('editor', 'user'),
  ('user', 'updateSelf');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE IF NOT EXISTS `tbl_category` (
  `id` int(11) NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `name`) VALUES
  (1, 'General'),
  (2, 'Magic'),
  (3, 'Rider of the universe'),
  (4, 'Rider of the Sky'),
  (5, 'Projecteuler'),
  (6, 'Misc.');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_comment`
--

CREATE TABLE IF NOT EXISTS `tbl_comment` (
  `id` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `createtime` int(11) NOT NULL,
  `author` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_friend`
--

CREATE TABLE IF NOT EXISTS `tbl_friend` (
  `userid` int(11) NOT NULL,
  `friendid` int(11) NOT NULL,
  `accepted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_friend`
--

INSERT INTO `tbl_friend` (`userid`, `friendid`, `accepted`) VALUES
  (2, 1, 1),
  (2, 3, 1),
  (3, 1, 1),
  (3, 7, 1),
  (4, 1, 1),
  (5, 1, 1),
  (6, 1, 1),
  (7, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_lookup`
--

CREATE TABLE IF NOT EXISTS `tbl_lookup` (
  `id` int(11) NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `code` int(11) NOT NULL,
  `type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_lookup`
--

INSERT INTO `tbl_lookup` (`id`, `name`, `code`, `type`, `position`) VALUES
  (1, 'Draft', 1, 'PostStatus', 1),
  (2, 'Published', 2, 'PostStatus', 2),
  (3, 'Archived', 3, 'PostStatus', 3),
  (4, 'Pending Approval', 1, 'CommentStatus', 1),
  (5, 'Approved', 2, 'CommentStatus', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_lyric`
--

CREATE TABLE IF NOT EXISTS `tbl_lyric` (
  `id` int(10) NOT NULL,
  `title` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `artist` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `sendername` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `senderemail` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `postdate` int(11) NOT NULL,
  `lyriccount` int(10) NOT NULL DEFAULT '0',
  `file` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_lyric`
--

INSERT INTO `tbl_lyric` (`id`, `title`, `artist`, `content`, `sendername`, `senderemail`, `postdate`, `lyriccount`, `file`) VALUES
  (1, 'Heaven lyrics ', 'Bryan Adams', 'Oh, thinkin'' about all our younger years\r\nThere was only you and me\r\nWe were young and wild and free\r\nNow nothin'' can take you away from me\r\nWe''ve been down that road before\r\nBut that''s over now\r\nYou keep me comin'' back for more\r\n\r\n[Chorus]\r\nBaby you''re all that I want\r\nWhen you''re lyin'' here in my arms\r\nI''m findin'' it hard to believe\r\nWe''re in heaven\r\nAnd love is all that I need\r\nAnd I found it there in your heart\r\nIt isn''t too hard to see\r\nWe''re in heaven\r\n\r\nOh, once in your life you find someone\r\nWho will turn your world around\r\nBring you up when you''re feelin'' down\r\nYeah, nothin'' could change what you mean to me\r\nOh, there''s lots that I could say\r\nBut just hold me now\r\n''Cause our love will light the way\r\n\r\n[Chorus]\r\n\r\nI''ve been waitin'' for so long\r\nFor somethin'' to arrive\r\nFor love to come along\r\nNow our dreams are comin'' true\r\nThrough the good times and the bad\r\nYeah, I''ll be standin'' there by you\r\n\r\n[Chorus]', 'Viet Ha', 'vietha@vietha.se', 1362909837, 576, 'http://www.youtube.com/watch?v=2dfn3wesPSg'),
  (2, 'Happy New Year', 'ABBA', 'No more champagne\r\nAnd the fireworks are through\r\nHere we are, me and you\r\nFeeling lost and feeling blue\r\nIt''s the end of the party\r\nAnd the morning seems so grey\r\nSo unlike yesterday\r\nNow''s the time for us to say...\r\n\r\nHappy new year\r\nHappy new year\r\nMay we all have a vision now and then\r\nOf a world where every neighbour is a friend\r\nHappy new year\r\nHappy new year\r\nMay we all have our hopes, our will to try\r\nIf we don''t we might as well lay down and die\r\nYou and I\r\n\r\nSometimes I see\r\nHow the brave new world arrives\r\nAnd I see how it thrives\r\nIn the ashes of our lives\r\nOh yes, man is a fool\r\nAnd he thinks he''ll be okay\r\nDragging on, feet of clay\r\nNever knowing he''s astray\r\nKeeps on going anyway...\r\n\r\nHappy new year\r\nHappy new year\r\nMay we all have a vision now and then\r\nOf a world where every neighbour is a friend\r\nHappy new year\r\nHappy new year\r\nMay we all have our hopes, our will to try\r\nIf we don''t we might as well lay down and die\r\nYou and I\r\n\r\nSeems to me now\r\nThat the dreams we had before\r\nAre all dead, nothing more\r\nThan confetti on the floor\r\nIt''s the end of a decade\r\nIn another ten years time\r\nWho can say what we''ll find\r\nWhat lies waiting down the line\r\nIn the end of eighty-nine...\r\n\r\nHappy new year\r\nHappy new year\r\nMay we all have a vision now and then\r\nOf a world where every neighbour is a friend\r\nHappy new year\r\nHappy new year\r\nMay we all have our hopes, our will to try\r\nIf we don''t we might as well lay down and die\r\nYou and I', 'Viet Ha', 'vietha@vietha.se', 1362909837, 394, ''),
  (3, 'Call Me Maybe', 'Carly Rae Jepsen', 'I threw a wish in the well,\r\nDon''t ask me, I''ll never tell\r\nI looked to you as it fell,\r\nAnd now you''re in my way\r\n\r\nI''d trade my soul for a wish,\r\nPennies and dimes for a kiss\r\nI wasn''t looking for this,\r\nBut now you''re in my way\r\n\r\nYour stare was holdin'',\r\nRipped jeans, skin was showin''\r\nHot night, wind was blowin''\r\nWhere you think you''re going, baby?\r\n\r\nHey, I just met you,\r\nAnd this is crazy,\r\nBut here''s my number,\r\nSo call me, maybe?\r\n\r\nIt''s hard to look right,\r\nAt you baby,\r\nBut here''s my number,\r\nSo call me, maybe?\r\n\r\nHey, I just met you,\r\nAnd this is crazy,\r\nBut here''s my number,\r\nSo call me, maybe?\r\n\r\nAnd all the other boys,\r\nTry to chase me,\r\nBut here''s my number,\r\nSo call me, maybe?\r\n\r\nYou took your time with the call,\r\nI took no time with the fall\r\nYou gave me nothing at all,\r\nBut still, you''re in my way\r\n\r\nI beg, and borrow and steal\r\nHave foresight and it''s real\r\nI didn''t know I would feel it,\r\nBut it''s in my way\r\n\r\nYour stare was holdin'',\r\nRipped jeans, skin was showin''\r\nHot night, wind was blowin''\r\nWhere you think you''re going, baby?\r\n\r\nHey, I just met you,\r\nAnd this is crazy,\r\nBut here''s my number,\r\nSo call me, maybe?\r\n\r\nIt''s hard to look right,\r\nAt you baby,\r\nBut here''s my number,\r\nSo call me, maybe?\r\n\r\nHey, I just met you,\r\nAnd this is crazy,\r\nBut here''s my number,\r\nSo call me, maybe?\r\n\r\nAnd all the other boys,\r\nTry to chase me,\r\nBut here''s my number,\r\nSo call me, maybe?\r\n\r\nBefore you came into my life\r\nI missed you so bad\r\nI missed you so bad\r\nI missed you so, so bad\r\n\r\nBefore you came into my life\r\nI missed you so bad\r\nAnd you should know that\r\nI missed you so, so bad\r\n\r\nIt''s hard to look right,\r\nAt you baby,\r\nBut here''s my number,\r\nSo call me, maybe?\r\n\r\nHey, I just met you,\r\nAnd this is crazy,\r\nBut here''s my number,\r\nSo call me, maybe?\r\n\r\nAnd all the other boys,\r\nTry to chase me,\r\nBut here''s my number,\r\nSo call me, maybe?\r\n\r\nBefore you came into my life\r\nI missed you so bad\r\nI missed you so bad\r\nI missed you so, so bad\r\n\r\nBefore you came into my life\r\nI missed you so bad\r\nAnd you should know that\r\n\r\nSo call me, maybe?', 'Viet Ha', 'vietha@vietha.se', 1362909837, 63, ''),
  (4, 'Dancing Queen', 'ABBA', 'You can dance, you can jive, having the time of your life \r\nSee that girl, watch that scene, diggin'' the dancing queen \r\n\r\nFriday night and the lights are low \r\nLooking out for the place to go \r\nWhere they play the right music, getting in the swing \r\nYou come in to look for a king \r\nAnybody could be that guy \r\nNight is young and the music''s high \r\nWith a bit of rock music, everything is fine \r\nYou''re in the mood for a dance \r\nAnd when you get the chance... \r\n\r\nYou are the dancing queen, young and sweet, only seventeen \r\nDancing queen, feel the beat from the tambourine \r\nYou can dance, you can jive, having the time of your life \r\nSee that girl, watch that scene, diggin'' the dancing queen \r\n\r\nYou''re a teaser, you turn ''em on \r\nLeave them burning and then you''re gone \r\nLooking out for another, anyone will do \r\nYou''re in the mood for a dance \r\nAnd when you get the chance... \r\n\r\nYou are the dancing queen, young and sweet, only seventeen \r\nDancing queen, feel the beat from the tambourine \r\nYou can dance, you can jive, having the time of your life \r\nSee that girl, watch that scene, diggin'' the dancing queen', 'vietha20', 'vietha20@ellusioner.se', 1362909837, 153, ''),
  (5, 'Gimme gimme gimme', 'ABBA', 'Half past twelve \r\nAnd I''m watching the late show in my flat all alone \r\nHow I hate to spend the evening on my own \r\nAutumn winds \r\nBlowing outside my window as I look around the room \r\nAnd it makes me so depressed to see the gloom \r\nIs there a man out there \r\nSomeone to hear my prays \r\n\r\nGimme gimme gimme a man after midnight \r\nWon''t somebody help me chase the shadows away \r\nGimme gimme gimme a man after midnight \r\nTake me through the darkness to the break of the day \r\n\r\nMovie stars \r\nFind the end of the rainbow, with a fortune to win \r\nIt''s so different from the world I''m living in \r\nTired of T.V. \r\nI open the window and I gaze into the night \r\nBut there''s nothing there to see, no one in sight \r\nIs there a man out there \r\nSomeone to hear my prays \r\n\r\nGimme gimme gimme a man after midnight \r\nWon''t somebody help me chase the shadows away \r\nGimme gimme gimme a man after midnight \r\nTake me through the darkness to the break of the day \r\n\r\nGimme gimme gimme a man after midnight... \r\nGimme gimme gimme a man after midnight... \r\n\r\nIs there a man out there \r\nSomeone to hear my prays \r\n\r\nGimme gimme gimme a man after midnight \r\nWon''t somebody help me chase the shadows away \r\nGimme gimme gimme a man after midnight \r\nTake me through the darkness to the break of the day \r\nGimme gimme gimme a man after midnight \r\nWon''t somebody help me chase the shadows away \r\nGimme gimme gimme a man after midnight \r\nTake me through the darkness to the break of the day', 'vietha20', 'vietha20@ellusioner.se', 1362909837, 208, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_message`
--

CREATE TABLE IF NOT EXISTS `tbl_message` (
  `id` int(11) NOT NULL,
  `senderid` int(11) NOT NULL,
  `receiverid` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `content` text NOT NULL,
  `sendtime` int(11) NOT NULL,
  `receiverdelete` tinyint(1) NOT NULL DEFAULT '0',
  `senderdelete` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_post`
--

CREATE TABLE IF NOT EXISTS `tbl_post` (
  `id` int(11) NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `tags` text COLLATE utf8_unicode_ci,
  `status` int(11) NOT NULL,
  `createtime` int(11) NOT NULL,
  `updatetime` int(11) NOT NULL,
  `authorid` int(11) NOT NULL,
  `categoryid` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

--
-- Dumping data for table `tbl_post`
--

INSERT INTO `tbl_post` (`id`, `title`, `content`, `tags`, `status`, `createtime`, `updatetime`, `authorid`, `categoryid`) VALUES
  (1, 'Funny prove', '<p>I saw this funny prove during a lecture when I was studying the 1st or 2nd year of the Engineering Program.</p><p>	<strong>\r\n	Question: Prove that: "papa = mama"</strong></p><p>	<strong><br>\r\n	</strong></p><p>	These are some of the answers of the students from different programs.</p><p>	<strong>\r\n	Answer:</strong></p><p>	<em>\r\n	1) Art students: "out of course" </em></p><p>	<em>2) Commerce students: "wrong question" </em></p><p>	<em>3) medical students: "wtf? it''s impossible" </em></p><p>	<em>4) engineers: "hmm...!!! so simple</em></p><p>	First assumption: According to Newton''s 2nd law:</p><pre>F = ma (I)\r\n</pre><p>\r\n	we know that</p>\r\n<pre>Force (F) = Pressure (p) / Area (a)\r\n</pre>\r\n<pre>p = F/a =&gt; F = p*a (II)\r\n</pre><p>\r\n	from equation (I) and (II), we have this connection</p>\r\n<pre>p*a = m*a (III)\r\n</pre><p>\r\n	and by squaring both sides of (III), we have</p>\r\n<pre>papa = mama\r\n</pre><p>\r\n	The lecturer''s conclusion: "<strong>Engineers are the species with solution of any problem</strong>"</p>', 'prove, engineer, physics', 2, 1171532823, 1171533363, 1, 1),
  (2, 'Back!', '<p>Hi again, Hope you all have a happy new year, cause that is what i had. This site has been up and down many times.</p><p>			Hopefully this is the last time it''''s going up and down again. Something new to say well i get rid of the domain ellusioner.com and haliko.com (aplus.net were the host of my domains but they seem so retarded in some way), so from now on you can access the site with the Swedish domain name ellusioner.se (only 19 kr) instead.</p><p>			All those domain names above are not accessible anymore. I have bought a domain vietha.se and have been using it for several years now. There is a trick to buy Swedish domain names really really cheap, not only the first time. I bought mine from loopia and they do have campaigns of cheap domains each Christmas. What I usually do is I quit y domain and re-buy it :D. I know it is so lame and I have to face the fact that while the domain is available to the public, I can lose it if someone buys it before me :)). Anyways 9 kr instead of 99 kr for a Swedish domain is ok I think.</p><p>			I have a few ideas what the site shall be about but mostly it will be about magic (one of my biggest hobby), RC (Radio Controlled) stuff, some astronomy and some programming related.</p><p>			I''m in the last year in Computer Science so it''s getting kinda busy so do not expect to see a post once a week :P</p>', 'back, domainname', 2, 1199717763, 1199717763, 1, 1),
  (3, 'Problem 1 - Multiples of 3 and 5', '<p>If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.</p><p>Find the sum of all the multiples of 3 or 5 below 1000.</p><pre>''''''\r\nProblem 1: find the sum of all multiple of both 3 and 5 below n\r\n@author: Viet Ha\r\n''''''\r\ndef multiple(n):\r\n    list = []\r\n    sum = 0;\r\n    for i in range (0,n):\r\n        if i%5 == 0 or i%3 == 0:\r\n            list.append(i)\r\n        else:\r\n            continue\r\n    for j in range(0,len(list)):\r\n        sum += list[j]     \r\n    print ''Sum of multiples of 3 and 5 is:'', sum\r\n\r\nSum of multiples of 3 and 5 is: 233168\r\n</pre>', 'projecteuler', 2, 1202557354, 1202557354, 1, 5),
  (4, 'Problem 2 - Even Fibonacci numbers', '<p>Problem 2: Each new term in the Fibonacci sequence is generated by adding the previous two terms. By starting with 1 and 2, the first 10 terms will be: 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ... </p>By considering the terms in the Fibonacci sequence whose values do not exceed four million, find the sum of the even-valued terms.\r\n<pre>''''''\r\nProblem 2: find the sum of all even values where the last term\r\nin fibonacci sequence does not exceed n\r\n@author: Viet Ha\r\n''''''\r\ndef fibonacci(s1, s2, n):\r\n    fibonacciList, evenList = [s1, s2], [s2,]\r\n    sumtotal, sumeven = 0, 0;\r\n    while s2 &lt; n:\r\n        s1, s2 = s2, s1+s2\r\n        fibonacciList.append(s2)\r\n        sumtotal = sum(fibonacciList)\r\n    for i in range(0,len(fibonacciList)):\r\n        if fibonacciList[i]%2 == 0:\r\n            sumeven += fibonacciList[i]\r\n        else:\r\n            continue\r\n    print ''Sum of all even fibonacci numbers is:'', sumeven\r\n\r\nSum of all even fibonacci numbers is: 4613732\r\n</pre>', 'projecteuler, fibonacci', 2, 1202558483, 1202558483, 1, 5),
  (5, 'Problem 3 - Largest prime factor', '<p>The prime factors of 13195 are 5, 7, 13 and 29. What is the largest prime factor of the number 600851475143 ?</p><pre>''''''\r\nProblem 3: finding the largest prime factor for a number n\r\n@author: Viet Ha\r\n''''''\r\ndef findlargestprimefactor(n):\r\n    divisor = 2\r\n    while n &gt; divisor:\r\n        if n%divisor == 0:\r\n            n = n/divisor\r\n            divisor = 2\r\n        else:\r\n            divisor += 1\r\n    print ''Largest prime factor:'', divisor\r\n\r\nLargest prime factor: 6857\r\n</pre>', 'projecteuler, largest, prime', 2, 1202814083, 1202814083, 1, 5),
  (6, 'Problem 4 - Largest palindrome product', '<p>A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.</p>Find the largest palindrome made from the product of two 3-digit numbers.\r\n<pre>''''''\r\nProblem 4: find the largest palindrome product made\r\nfrom product of 2 3-digit numbers, s and e are start\r\nand end respectively\r\n@author: Viet Ha\r\n''''''\r\ndef larpalinprod(s,e):\r\n    largestproduct = []\r\n    for i in range(s,e):\r\n        for j in range(s,e):\r\n            if str(i*j) == str(i*j)[::-1]:\r\n                largestproduct.append(i*j)\r\n    print ''The largest palindrome product is:'', max(largestproduct)\r\n\r\nThe largest palindrome product is: 906609\r\n</pre>', 'largest, palindrome, projecteuler', 2, 1202827523, 1202827523, 1, 5),
  (7, 'Problem 5 - Smallest multiple', '<p>2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.</p>What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?\r\n<pre>''''''\r\nProblem 5: find the least common multiple within a range and\r\nn is the max range\r\n@author: Viet Ha\r\n''''''\r\ndef lcmrange(n):\r\n    def gcd(a,b):\r\n        while b:\r\n            a, b = b, a % b\r\n        return a\r\n    def lcm(a, b):        \r\n        return a * b / gcd(a, b)\r\n    print ''The lcm within range(1,'' + str(n) + '') is:'',reduce(lcm, range(1,n))\r\n\r\nThe lcm within range(1,20) is: 232792560\r\n</pre>', 'projecteuler, lcm', 2, 1203035003, 1203035003, 1, 5),
  (8, 'Problem 6 - Sum square difference', 'The sum of the squares of the first ten natural numbers is,\r\n\r\n1² + 2² + ... + 10² = 385\r\nThe square of the sum of the first ten natural numbers is,\r\n\r\n(1 + 2 + ... + 10)² = 55² = 3025\r\nHence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.\r\n\r\nFind the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.\r\n<pre>\r\n''''''\r\nProblem 6: finds the difference between the sum of all\r\nsquares and the square of all the sums. n is the number\r\nfirst n:nth numbers\r\n@author: Viet Ha\r\n''''''\r\ndef diff(n):\r\n    sumofsquare, squareofsum, i = 0, 0, 1\r\n    while i &lt;= n:\r\n        sumofsquare += i*i\r\n        squareofsum += i\r\n        i += 1\r\n    squareofsum *= squareofsum\r\n    print ''The difference is:'', squareofsum-sumofsquare\r\n\r\nThe difference is: 25164150\r\n</pre>', 'projecteuler', 2, 1203038403, 1203038403, 1, 5),
  (9, 'Problem 7 - 10001st prime', 'By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.\r\n\r\nWhat is the 10001st prime number?\r\n<pre>\r\n''''''\r\nProblem 7: find the 10001st prime number\r\n@author: Viet Ha\r\n''''''\r\ndef findprime(n):\r\n    primeList = []\r\n    for i in range(2,n):\r\n        if isprime(i):\r\n            primeList.append(i)\r\n            if len(primeList) == 10001:\r\n                print ''The 10001st prime is:'', i\r\n                break\r\n''''''\r\nHelp function to check for primes\r\n''''''\r\ndef isprime(nr):\r\n    return all(nr % i for i in xrange(2, nr))\r\n\r\nThe 10001st prime is: 104743\r\n</pre>', 'projecteuler, prime', 2, 1203049403, 1203049403, 1, 5),
  (10, 'Problem 8 - Largest product in a series', '<p>The four adjacent digits in the 1000-digit number that have the greatest product are 9 x 9 x 8 x 9 = 5832.</p><p>7316717653133062491922511967442657474235534919493496983520312774506326239578318016984801869478851843858615607891129494954595017379583319528532088055111254069874715852386305071569329096329522744304355766896648950445244523161731856403098711121722383113622298934233803081353362766142828064444866452387493035890729629049156044077239071381051585930796086670172427121883998797908792274921901699720888093776657273330010533678812202354218097512545405947522435258490771167055601360483958644670632441572215539753697817977846174064955149290862569321978468622482839722413756570560574902614079729686524145351004748216637048440319989000889524345065854122758866688116427171479924442928230863465674813919123162824586178664583591245665294765456828489128831426076900422421902267105562632111110937054421750694165896040807198403850962455444362981230987879927244284909188845801561660979191338754992005240636899125607176060588611646710940507754100225698315520005593572972571636269561882670428252483600823257530420752963450</p><p>Find the thirteen adjacent digits in the 1000-digit number that have the greatest product. What is the value of this product?</p>\r\n<pre>''''''\r\nProblem 8: find the greatest product of five consecutive\r\ndigits in a 1000-digit number.\r\n@author: Viet Ha\r\n''''''\r\ndef largestproductcons():\r\n    f = open(''~/vieng095/1000digit'', ''r'')\r\n    line = f.readline()\r\n    intList, savedList = [], []\r\n    intList = map(int, str(line))\r\n    for i in range(0,len(intList)):        \r\n        if i &lt;= 995:\r\n            savedList.append(intList[i]*\r\n                             intList[i+1]*\r\n                             intList[i+2]*\r\n                             intList[i+3]*\r\n                             intList[i+4])\r\n        else:\r\n            continue\r\n    print ''The largest product is:'', max(savedList)\r\n\r\nThe largest product is: 40824\r\n</pre>', 'projecteuler, product, consecutive', 2, 1203050987, 1203050987, 1, 5),
  (11, 'Problem 9 - Special Pythagorean triplet', '<p>A Pythagorean triplet is a set of three natural numbers, a &lt; b &lt; c, for which,</p><p>a² + b² = c²\r\nFor example, 3² + 4² = 9 + 16 = 25 = 5².</p>There exists exactly one Pythagorean triplet for which a + b + c = 1000.\r\nFind the product abc.\r\n<pre>''''''\r\nProblem 9: find the product of the pythagorian triplet for which\r\na + b + c = 1000 and a² + b² =c²\r\n@author: Viet Ha\r\n''''''\r\nfrom math import ceil, sqrt\r\n\r\ndef pythagorian():\r\n    def gcd(a,b):\r\n        while b:\r\n            a, b = b, a % b\r\n        return a\r\n    totalSum = 1000\r\n    halfSum = totalSum/2\r\n    limit = int(ceil(sqrt(halfSum))-1)\r\n    for m in range(2,limit):\r\n        if halfSum%m == 0:\r\n            sm = halfSum/m\r\n            while sm%2 == 0:\r\n                sm = sm/2\r\n            if m%2 == 1:\r\n                k = m+2\r\n            else:\r\n                k = m+1\r\n            while k &lt; 2*m and k &lt;= sm:\r\n                if sm%k == 0 and gcd(k,m) == 1:\r\n                    d = halfSum/(k*m)\r\n                    n = k-m\r\n                    a = d*(m*m-n*n)\r\n                    b = 2*d*m*n\r\n                    c = d*(m*m+n*n)\r\n                    print ''The product is:'', a*b*c\r\n                k = k+2\r\n\r\nThe product is: 31875000\r\n</pre>', 'projecteuler, pythagorian, triplet', 2, 1236850529, 1236850529, 1, 5),
  (12, 'Problem 10 - Summation of primes', 'The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.\r\n\r\nFind the sum of all the primes below two million.\r\n<pre>\r\n''''''\r\nProblem 10: find the sum of all primes below 2kk\r\n@author: Viet Ha\r\n''''''\r\ndef sumprimes(n):\r\n    primeSum = 0\r\n    for i in range(2,n):\r\n        print i\r\n        if isprime(i):\r\n            primeSum += i\r\n    print ''The sum of all primes is:'', primeSum\r\n''''''\r\nHelp function to check for primes\r\n''''''\r\ndef isprime(nr):\r\n    return all(nr % i for i in xrange(2, nr))\r\n\r\nThe sum of all primes is: 142913828922\r\n</pre>', 'projecteuler, prime', 2, 1236958529, 1236958529, 1, 5),
  (13, 'Problem 12 - Highly divisible triangular number', '<p>The sequence of triangle numbers is generated by adding the natural numbers. So the 7th triangle number would be 1 + 2 + 3 + 4 + 5 + 6 + 7 = 28. The first ten terms would be:</p><p>1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...</p><p>Let us list the factors of the first seven triangle numbers:</p><p><strong>1:</strong> 1 </p><p><strong>3: </strong>1,3 </p><p><strong>6: </strong>1,2,3,6 </p><p><strong>10:</strong> 1,2,5,10 </p><p><strong>1</strong><strong>5:</strong> 1,3,5,15 </p><p><strong>21: </strong>1,3,7,21 </p><p><strong>28: </strong>1,2,4,7,14,28 </p><p>We can see that 28 is the first triangle number to have over five divisors.</p>\r\nWhat is the value of the first triangle number to have over five hundred divisors?\r\n<pre>''''''\r\nProblem 12: finds the triangle number with 500 divisors\r\n@author: Viet Ha\r\n''''''\r\nfrom math import pow\r\n\r\ndef triangledivisors(randnr):\r\n    def nrofDivisors(n):\r\n        nrofdivisors, squareroot = 0, int(sqrt(n))\r\n        for i in range(1,squareroot):\r\n            if n%i == 0:\r\n                nrofdivisors += 2\r\n        if int(pow(squareroot,2)) == n:\r\n            nrofdivisors -= 1\r\n        return nrofdivisors\r\n    number, j = 0, 1\r\n    while nrofDivisors(number) &lt; randnr:\r\n        number += j\r\n        j += 1\r\n    print ''The first triangle number with over 500 divisors is:'', number\r\n\r\nThe first triangle number with over 500 divisors is: 76576500\r\n</pre>', 'projecteuler, divisors, trianglenumber', 2, 1236958945, 1236958945, 1, 5),
  (14, 'Problem 13 - Large sum', '<p>Work out the first ten digits of the sum of the following one-hundred 50-digit numbers. The numbers are being processed by taking input from user.</p><pre>''''''\r\nProblem 13: find the first 10 digit from the sum of\r\none hundred 50 digit number\r\n@author: Viet Ha\r\n''''''\r\ndef sumdigit():\r\n    f = open(''~/vieng095/50digit'', ''r'')\r\n    numberList, savedList, sumtotal = [], [], 0\r\n    for line in f:\r\n        numberList.append(int(line.rstrip(''\\n'')))\r\n    sumtotal = sum(numberList)\r\n    numberList = map(int, str(sum(numberList)))\r\n    for i in range(0,10):\r\n        savedList.append(numberList[i])\r\n    print ''The first 10 digits are:'', savedList\r\n\r\nThe first 10 digits are: 5537376230\r\n</pre>', 'projecteuler', 2, 1240180949, 1240180949, 1, 5),
  (15, 'Problem 14 - Longest Collatz sequence', '<p>The following iterative sequence is defined for the set of positive integers:</p><p>n → n/2 (n is even) </p><p>n → 3n + 1 (n is odd)</p><p>Using the rule above and starting with 13, we generate the following sequence: </p><p>13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1 </p><p>It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.</p><p>Which starting number, under one million, produces the longest chain?\r\nNOTE: Once the chain starts the terms are allowed to go above one million.</p>\r\n<pre>''''''\r\nProblem 14: find the longest collatz chain. Loop from 1 to n\r\n@author: Viet Ha\r\n''''''\r\ndef findcollatz(n):\r\n    def longestcollatz(n):\r\n        numberList = [n,]\r\n        if n &gt; 1000000:\r\n            print ''The starting number should be less than 1000000''\r\n        else:\r\n            while n &gt; 1:\r\n                if n == 1:\r\n                    print numberList\r\n                else:\r\n                    if n%2 == 0:\r\n                        n = n/2\r\n                    else:\r\n                        n = (3*n)+1\r\n                    numberList.append(n)\r\n        return len(numberList)\r\n    sizeCollatz, startingNumbers = [], []\r\n    for j in range(n,1,-1):\r\n        startingNumbers.append(j)\r\n        sizeCollatz.append(longestcollatz(j))\r\n    maximum, maxIndex = 0, 0\r\n    for i in range(len(sizeCollatz)):\r\n        if sizeCollatz[i] &gt; maximum:\r\n            maximum = sizeCollatz[i]\r\n            maxIndex = i\r\n    print ''The starting number is:'', startingNumbers[maxIndex]\r\n\r\nThe starting number is: 837799\r\n</pre>', 'projecteuler, collatz', 2, 1260180949, 1260180949, 1, 5),
  (16, 'Problem 16 - Power digit sum', '2¹⁵ = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.\r\n\r\nWhat is the sum of the digits of the number 2¹⁰⁰⁰?\r\n<pre>\r\n''''''\r\nProblem 16: find the power digit sum of 2¹⁰⁰⁰\r\n@author: Viet Ha\r\n''''''\r\ndef powerdigitsum():\r\n    number, numberList = long(2**1000), []\r\n    numberList = map(int, str(number))\r\n    print ''The power digit sum is:'', sum(numberList)\r\n\r\nThe power digit sum is: 1366\r\n</pre>', 'projecteuler', 2, 1290180949, 1290180949, 1, 5),
  (17, 'Problem 20 - Factorial digit sum', '<p>n! means n × (n − 1) × ... × 3 × 2 × 1</p><p>For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,\r\nand the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.</p>Find the sum of the digits in the number 100!\r\n<pre>''''''\r\nProblem 20: find the sum of all digits in factorial of n\r\n@author: Viet Ha\r\n''''''\r\nfrom math import factorial\r\n\r\ndef sumfact(n):\r\n    print ''The sum is:'', sum(map(int, str(factorial(n))))\r\n\r\nThe sum is: 648\r\n</pre>', 'projecteuler, factorialsum', 2, 1300180949, 1300180949, 1, 5),
  (18, 'Problem 25 - 1000-digit Fibonacci number', '<p>The Fibonacci sequence is defined by the recurrence relation:</p><p>Fn = Fn−1 + Fn−2, where F1 = 1 and F2 = 1.\r\nHence the first 12 terms will be:</p><p>F1 = 1 </p><p>F2 = 1 </p><p>F3 = 2 </p><p>F4 = 3 </p><p>F5 = 5 </p><p>F6 = 8 </p><p>F7 = 13 </p><p>F8 = 21 </p><p>F9 = 34 </p><p>F10 = 55 </p><p>F11 = 89 </p><p>F12 = 144</p><p>The 12th term, F12, is the first term to contain three digits.</p><p>What is the first term in the Fibonacci sequence to contain 1000 digits?</p><pre>''''''\r\nProblem 25: find the first fibonacci term that contain 1000 digits\r\n@author: Viet Ha\r\n''''''\r\ndef findfibonacci():\r\n    s1, s2, i = 1, 1, 2\r\n    fibolenlist = [],\r\n    while len(fibolenlist) &lt; 1000:\r\n        s1, s2 = s2, s1+s2\r\n        fibolenlist = map(int, str(s2))\r\n        i += 1\r\n\r\n    print ''The first term containing 1000-digit is:'', i\r\n\r\nThe first term containing 1000-digit is: 4782\r\n</pre>', 'projecteuler, fibonacci', 2, 1300290949, 1300290949, 1, 5),
  (19, 'Problem 29 - Distinct powers', '<p>Consider all integer combinations of ab for 2 ≤ a ≤ 5 and 2 ≤ b ≤ 5:</p><p>2²=4, 2³=8, 2⁴=16, 2⁵=32 </p><p>3²=9, 3³=27, 3⁴=81, 3⁵=243 </p><p>4²=16, 4³=64, 4⁴=256, 4⁵=1024 </p><p>5²=25, 5=125, 5⁴=625, 5⁵=3125</p><p>If they are then placed in numerical order, with any repeats removed, we get the following sequence of 15 distinct terms: </p><p>4, 8, 9, 16, 25, 27, 32, 64, 81, 125, 243, 256, 625, 1024, 3125 </p><p>How many distinct terms are in the sequence generated by <img height="18" width="15" src="http://www5a.wolframalpha.com/Calculate/MSP/MSP421hc5ei9bba767440000452f97128fcd5c3c?MSPStoreType=image/gif&s=52&w=15.&h=18." id="i_0100_1" data-attribution="" alt="">  for 2 ≤ a ≤ 100 and 2 ≤ b ≤ 100?</p>\r\n<pre>''''''\r\nProblem 29: finds the length of all unique powers within\r\nthe range s and n\r\n@author: Viet Ha\r\n''''''\r\nfrom math import pow\r\n\r\ndef distinctpowers(s, n):\r\n    list = []\r\n    for i in range(s,n+1):\r\n        for j in range(s,n+1):\r\n            list.append(pow(i,j))\r\n    list.sort()\r\n    print ''The length is:'', len(set(list))\r\n\r\nThe length is: 9183\r\n</pre>', 'projecteuler, distinct, powers', 2, 1302290949, 1302290949, 1, 5),
  (20, 'Problem 30 - Digit fifth powers', '<p>Surprisingly there are only three numbers that can be written as the sum of fourth powers of their digits:</p><p>1634 = 1⁴ + 6⁴ + 3⁴ + 4⁴ </p><p>8208 = 8⁴ + 2⁴ + 0⁴ + 8⁴ </p><p>9474 = 9⁴ + 4⁴ + 7⁴ + 4⁴ </p><p>As 1 = 1⁴ is not a sum it is not included. The sum of these numbers is 1634 + 8208 + 9474 = 19316. Find the sum of all the numbers that can be written as the sum of fifth powers of their digits.</p><pre>''''''\r\nProblem 30: find the sum of the nth grade of each digit in\r\na number where the sum is equal to that number within the\r\nrange of n\r\n@author: Viet Ha\r\n''''''\r\nfrom math import pow\r\n\r\ndef sumgrade(n, power):\r\n    def grade(n):\r\n        return pow(n, power)\r\n    numberlist, savedfound = [], []\r\n    for i in range(2,n):\r\n        intersum, numberlist = 0, map(int,str(i))\r\n        length = len(numberlist)\r\n        for j in range(0,length):\r\n            intersum += grade(numberlist[j])            \r\n        if intersum == i:\r\n            savedfound.append(i)\r\n            print savedfound\r\n        else:\r\n            continue\r\n    print ''The sum is:'', sum(savedfound) \r\n\r\nThe sum is: 443839\r\n</pre>', 'projecteuler', 2, 1302490949, 1302490949, 1, 5),
  (21, 'Google API', '<p>Recently I have been trying out Google API for web applications. All you need to do is sign up for a developer account and get the API access key. </p><p>You can get the API access key from <a target="_blank" href="https://console.developers.google.com/">here</a>, go to Credentials and find the API key. The key should look like this: </p><pre>API KEY: AIzaSyA7s0xwUTeujkuN_O2cvfIlgsryuUGvZkY</pre><p>You can check out two of my projects with Google API from the cool features section. I have not put any time integrating the Google API together with Yii framework. Right now, I access the Google API scripts from outside via an iframe tag.</p><p><a href="http://cms.vietha.se/feature/youtube">Youtube Data API v3</a></p><p><a href="http://cms.vietha.se/feature/googlemap">Google Map Route Planner</a></p>', 'google, youtube, api, maps, route, planner', 2, 1405334489, 1405334515, 1, 1),
  (22, 'Up in the air', '<p>The art of being able to fly has always tempted mankind. For decades, we tried to mimic the birds in order to experience the freedom they have - In Greek mythology, the out of luck trial of Ikaros and Daidalos to Leonardo da Vinci''s flying-machine sketches by the end of the 1400s / beginning of 1500s. Year 1783, did man leave the ground when the brothers Montgolfier performed some of the first attended balloon rides. But we had to wait for another 120 years for the first engine-powered air voyage. It was not until 1903, Orville Wright flew the plane Flyer 1 for roughly 12 seconds long flight. Until now the airplanes have developed with an incredible speed. Soon spaceflight maybe reality.</p><table>\r\n<tbody>\r\n<tr>\r\n	<td>\r\n		<p>\r\n			<img src="/uploads/post/Wright1stFlight.jpg" alt="Wright''s 1st Flight" style="width: 400px;">\r\n		</p>\r\n	</td>\r\n	<td>\r\n		<strong>Name:</strong> Flyer 1\r\n		<br>\r\n		<strong>	Range:</strong> 259.7 meters\r\n		<br>\r\n		<strong>	Max speed:</strong> 48 km/h\r\n		<br>\r\n		<strong>	Wing span:</strong> 12.29 meters\r\n		<br>\r\n		<strong>	Engine:</strong> a water-cooled Wright, 12 horsepowers\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table><h3>Earlier air voyages</h3><p>\r\n	The history of engine-powered flight begins in Kittyhawk, North Carolina, USA, year 1903, precisely 111 years ago. Orville Wright with his Flyer 1 was the first and only engine-powered airplane that flew in a controlled manner. But the first flight was not so long: Wright flew for 12 seconds and reached 30 meters. Before the day we could only fly with air balloons And airships were popular until the end of 1930''s. Airships could stay up in the air thanks to the the big gas-filled balloons inside the chassis of the vessel. The airships drive forward with the help of the engine propellers. The passengers travel safely and comfortably inside a car underneath the ship, which often had bars and walking deck.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n	<td>\r\n		<img src="/uploads/post/hindenburg.gif" alt="The Hindenburg ship" style="width: 400px;">\r\n	</td>\r\n	<td>\r\n		<strong>\r\n		Type: </strong>Hindenburg-class airship\r\n		<br>\r\n		<strong>Manufacturer:</strong>	Luftschiffbau Zeppelin GmbH\r\n		<br>\r\n		<strong>Construction number:</strong> LZ129\r\n		<br>\r\n		<strong>Manufactured:</strong>	1931-36\r\n		<br>\r\n		<strong>Registration:</strong>	D-LZ129\r\n		<br>\r\n		<strong>First flight: </strong>March 4, 1936\r\n		<br>\r\n		<strong>Owners and operators:</strong>	Deutsche Zeppelin Reederei\r\n		<br>\r\n		<strong>In service:</strong>	1936–37\r\n		<br>\r\n		<strong>Flights:</strong>	63\r\n		<br>\r\n		<strong>Fate:</strong>	Destroyed in fire May 6, 1937\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table><p>\r\n	The German airship Hindenburg was 245 meters long and 41 meters in diameter. The ship exploded and burned up when it landed the 6th May 1937 in New Jersey, USA. Incredibly enough, 61 souls survived of 97 on board. Year 1927, Charles Lindbergh became the first to fly over the Atlantic Ocean all by himself. He started 20th May from Long Island, New York, and flew 5778 long journey all the way to France. After 33 hours and 39 minutes of flying he landed in Paris safely.</p>\r\n<table>\r\n<tbody>\r\n<tr>\r\n	<td>\r\n		<p>\r\n			<img src="/uploads/post/charleslindbergh.jpg" alt="Charles Lindbergh''s flight over the Atlantic" style="width: 400px;">\r\n		</p>\r\n	</td>\r\n	<td>\r\n		Charles Lindbergh flew alone over the Atlantic Ocean in the plane "Spirit of St. Louis"\r\n		<br>\r\n		<br>\r\n		<strong>Fun fact: </strong> The British, John Alcock and Arthur Brown were the first to flew over the Atlantic Ocean in 1919.\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table><p>To be continued...</p>', 'flying, airplane, flyingmachine', 2, 1281609689, 1281609689, 1, 4),
  (23, 'Without parachutes to classic fighters', '<p>Air combat fighters waged for the first time during World War 1. The brave pilots were considered heroes. They often sat next to the fuel tank and had no parachutes, because they were not invented yet. The most famous pilot was Manfred von Richthofen (aka "<strong>The Red Baron</strong>"), who flew a bright red Fokker Dr. 1 with triple wing levels. Richthofen died in 1918 when his plane  was shoot down by a group pf British Sopwith-Camel airplanes.</p><p><img src="/uploads/post/redbaron.jpg" alt="The Red Baron" style="width: auto;"></p><p>In the beginning of the war, the bombs were thrown out by the pilot barehanded - sometimes right on the plane flying below! But the bomb drop became less risky for the pilots when they started to use proper mounting for the bombs and real rear sights.</p><p>Photographs from the air was also another important area that was developed during the first World War. The plane flew high above the enemy bases and taking photos. The pictures were used for creating maps over the enemy''s bases and identify possible military targets.</p><h3>Classical war-fighters</h3><p>The second World War took place in September 1939 when the German forces invaded Poland. The war ended in 1945 when the Japanese cities Hiroshima and Nagasaki were completely destroyed by the American atom bombs. Air combats became a more important role during this period. Gradually, air combats became most important for the allied (England, USA and the Soviet Union) to win the war against Germany, Italy and Japan. The development of air fighters went sky-rocket when people noticed the different possibilities of air combats gave. During the six years period of the World War, the airplanes were developed from the slow "double-deck airplane" (planes with two wings) from the first World War to the first jet planes. With the help of new long-distance bomber, attacking of enemy''s military industries and other important targets could be done for the first time. Jet fighters became more faster, heavier armed and longer flying range.</p><table>\r\n<tbody>\r\n<tr>\r\n	<td>\r\n		<img src="/uploads/post/P-51.jpg" alt="P-51 D Mustang" style="width: 400px;">\r\n	</td>\r\n	<td>\r\n		<strong>\r\n		Name:</strong> P-51 Mustang<br>\r\n		<strong>\r\n		Type:</strong> One-sit jet fighter<br>\r\n		<strong>\r\n		No. built:</strong> 7956<br>\r\n		<strong>Range: </strong>1529 kilometers<br>\r\n		<strong>Max speed:</strong> 703 km/h<br>\r\n		<strong>Arm:</strong> 6 wing-mounted machine guns, 900 kg bombs or 8 rockets.<br>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td colspan="2">\r\n		<strong>\r\n		Fun fact: <br>\r\n		</strong><br>\r\n		<ul>\r\n			<li>The first weapon armored planes were shown at aircraft exhibitions in London 1913. One problem with the machine-gun planes was the bullets were shoot on its own propeller. Therefore, the propeller blades were constructed in such a way the bullets were not fired if the blade is in front of the shooting pipes.</li>\r\n			<li>During the second World War, England and German built over 100000 jet fighters.</li>\r\n			<li>Boeing B-17G (the "American infestor") was the most common day-time bomber, while Avro Lancaster was used during night-time attacks.</li>\r\n		</ul>\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table><p>To be continued...</p>', 'worldwar, red, barone, fokker', 2, 1282228889, 1282228889, 1, 4),
  (24, 'Modern jet fighters', 'When the British Frank Whittle invented the jet-engine during the 1930''s was a new significant era for the flight. 1931, one group of international scientists gathered in Soviet Union to investigate the possible options the jet engines had. Five years later, the German scientist Werner von Braun, did a trial run of the first jet fighter. The plane exploded but the pilot lived. In England, Whittle had carried on the work with his jet engine. He succeeded with the production of a jet combat fighter before the Germans and before the second World War ended. This was important for development of flying machines because the jet engines made it possible for the planes to fly faster and higher. The old jet fighters with piston engines starting to be replaced. But the earliest jet fighters like Messerschmidt Me 262 were too late for any contributions  to the war. After the invention of the jet engines, military flights had developed quite a lot. One important technique is the "stealth technology". Military airplanes which uses stealth technology are very difficult to be detected by the radar. One of these planes is the Lockheed F-117A Nighthawk which made its maiden flight 1981. The advanced computer system helps the pilot gets into attack position and drop the laser-steered bombs.\r\n<table>\r\n<tbody>\r\n<tr>\r\n	<td>\r\n		<img src="/uploads/post/F117ANighthawk.jpg" alt="Lockheed F-117A Nighthawk" style="width: 400px;">\r\n	</td>\r\n	<td>\r\n		<strong>\r\n		Name:</strong> Lockheed F-117<br>\r\n		<strong>\r\n		Type:</strong> One-sit jet fighter with stealth technology<br>\r\n		<strong>\r\n		Engines:</strong> 2 General Electric  F404 turbofan engines, 4899 kilopond<br>\r\n		<strong>Range: </strong>1111 kilometers<br>\r\n		<strong>Max speed:</strong> 1040 km/h<br>\r\n		<strong>Arm:</strong> 2 laser-steered bombs<br>\r\n	</td>\r\n</tr>\r\n<tr>\r\n	<td colspan="2">\r\n		<strong>Fun facts:</strong>\r\n		<ul>\r\n			<li>Gloster Meteor was the first jet plane that was put to use in combat. Max speed was 800 km/h</li>\r\n			<li>Nowadays, some planes can even fly with speed higher than 3000 km/h</li>\r\n			<li>Hawker Harrier Jump Jet can angle the jet-streams downwards in order to lift and land upwards and downwards respectively like a helicopter. The plane can stay balanced in the air at one place and even flying backwards.</li>\r\n		</ul>\r\n	</td>\r\n</tr>\r\n</tbody>\r\n</table>', 'jet, fighters', 2, 1284989009, 1284989009, 1, 4),
  (25, 'Install ArchLinux super quick', '<p>I have been using both Gentoo and Archlinux and is an experienced user. I personally prefer Archlinux over Gentoo. I found that the installation guide from ArchLinux''s wiki is really hard to follow, therefore I''m writing this as a personal reference and also a beginner reference for those who are new to installing Linux OS from scratch. </p><p>Follow these simple steps in order to install Archlinux successfully.\r\nFirst create a bootable usbdrive. Assuming you have downloaded the iso from Archlinux''s \r\nhomepage and the usbdrive name is /dev/sdb and also assuming that you have the usb device \r\nformatted to vfat. I also assumed that you have a x64 processor.</p><p>\r\n	If you do not know where to get the image then go <a target="_blank" href="https://www.archlinux.org/download/">here</a>.</p><p>\r\n	<strong>Step 1:</strong> Create bootable usb</p>\r\n<pre>sudo dd if=arch.iso of=/dev/sdb bs=4M; sync\r\n</pre><p>\r\n	or if you are using Windows, you can use <a target="_blank" href="http://rufus.akeo.ie/">Rufus</a>. Boot into the installation media you just created.</p><p>\r\n	<strong>Step 2:</strong> Making partitions (Assuming you have more than 4GB RAM so no need for SWAP partitions). Fdisk command that you need to know</p><ul>\r\n	\r\n<li><em>n </em><em>= new partition</em></li>	\r\n<li><em>w = write changes</em></li>	\r\n<li><em>q = quit</em> </li>	\r\n<li><em>a = toogle bootable flag</em></li></ul>\r\n<pre>#fdisk /dev/sda\r\n</pre><p>\r\n	When you enter the fdisk menu, you can either press the following keys or explore the process by yourself.</p>\r\n<pre>n =&gt; p =&gt; Enter =&gt; Enter =&gt; +20G (for root partition)\r\na =&gt; Enter (togle bootable flag)\r\nn =&gt; p =&gt; Enter (couple of times) (for home partition)\r\nw (write changes)\r\n</pre><p>\r\n	Assuming you have <em>/dev/sda1 </em>&&<em> /dev/sda2</em>, now run these 2 commands:</p>\r\n<pre>#mkfs.ext4 /dev/sda1\r\n#mkfs.ext4 /dev/sda2\r\n</pre><p>\r\n	<strong>Step 3:</strong> Mount the newly created partitions</p>\r\n<pre>#mount /dev/sda1 /mnt\r\n#mkdir -p /mnt/home\r\n#mount /dev/sda2 /mnt/home\r\n</pre><p>\r\n	<strong>Step 4: </strong>Install the base system (install the base and base-devel packages)</p>\r\n<pre>&lt;strong&gt;&lt;/strong&gt;#pacstrap /mnt base base-devel\r\n</pre><p>\r\n	<strong>Step 5:</strong> Generate fstab</p>\r\n<pre>#genfstab -U -p /mnt &gt;&gt; /mnt/etc/fstab\r\n#nano /mnt/etc/fstab\r\n</pre><p>\r\n	just to make sure that the partitions you just created is added to fstab</p><p>\r\n	<strong>Step 6: </strong>Change root, change to the newly installed system and start the configurations or the new system.</p>\r\n<pre>#arch-chroot /mnt\r\n</pre><p>\r\n	<strong>Step 7:</strong> Locale stuff</p>\r\n<pre>#nano /etc/locale.gen\r\n</pre><p>\r\n	uncomment the following lines (you can choose whatever languages you want to but I stick with EN), so i uncommented the # from the lines. Should loook like this:</p>\r\n<pre>en_US.UTF-8 UTF-8\r\nen_US ISO-8859-1\r\n</pre><pre>#locale-gen\r\n#echo LANG=en_US.UTF-8 &gt; /etc/locale.conf\r\n#export LANG=en_US.UTF-8\r\n</pre><p>\r\n	<strong>Step 8: </strong>Vconsole font and keymap (for me sv-latin1), probably you want to change this according to your keyboard layout.</p>\r\n<pre>#nano /etc/vconsole.conf\r\n</pre><p>\r\n	and write</p>\r\n<pre>KEYMAP=sv-latin1\r\nFONT=Lat2-Terminus16\r\n</pre><p>\r\n	<strong>Step 9:</strong> Enable networking deamon</p>\r\n<pre>#systemctl enable dhcpcd.service\r\n</pre><p>\r\n	<strong>Step 10: </strong>Time (for me Europe/Stockholm as Zone/SubZone)</p>\r\n<pre>#ln -s /usr/share/zoneinfo/Europe/Stockholm &gt; /etc/localtime\r\n</pre><p>\r\n	<strong>Step 11: </strong>Hostname</p>\r\n<pre>#echo Hostname &gt; /etc/hostname\r\n</pre><p>\r\n	<strong>Step 12:</strong> Root password (write in the root password twice)</p>\r\n<pre>#passwd\r\n</pre><p>\r\n	<strong>Step 13: </strong>Add a normal user (change loginname to your desired name)</p>\r\n<pre>#useradd -m -g users -G audio,lp,optical,storage,power,video,games,network,wheel -s /bin/bash loginname\r\n</pre><p>\r\n	Write in user password twice</p>\r\n<pre>#passwd loginname\r\n</pre><p>\r\n	At this stage, if you did not install the base-devel package you will have to manually install sudo</p>\r\n<pre>#pacman -S sudo\r\n#EDITOR=nano visudo\r\n</pre><p>\r\n	Uncomment the following line:</p>\r\n<pre>%wheel ALL=(ALL) ALL (remove the # from the line)\r\n</pre><p>\r\n	<strong>Step 14:</strong> Install GRUB and followed configs</p>\r\n<pre>#pacman -S grub-bios\r\n#grub-install --target=i386-pc --recheck /dev/sda\r\n#cp /usr/share/locale/en\\@quot/LC_MESSAGES/grub.mo /boot/grub/locale/en.mo\r\n#grub-mkconfig -o /boot/grub/grub.cfg\r\n</pre><p>\r\n	You are now done with installing the base working system</p><p>\r\n	<strong>Step 15: </strong>Exit and reboot</p>\r\n<pre>#exit\r\n#umount /mnt/home\r\n#umount /mnt\r\n#reboot\r\n</pre><p>\r\n	Voila you have now a fully working ArchLinux but you still need to install some more stuff.\r\nLogin as normal user you''ve created earlier.</p><p>\r\n	<strong>Step 16:</strong> Pacman configs (you can choose to config these options as you desired), uncomment all the other libs except those related to testing.</p>\r\n<pre>$sudo nano /etc/pacman.conf\r\n</pre><p>\r\n	This command will help to find the fastest mirror for downloading the updates and run a full system upgrade.</p>\r\n<pre>$sudo pacman-optimize\r\n$sudo pacman -Syy; sudo pacman -Syu\r\n</pre><p>\r\n	A reboot is recommended</p><p>\r\n	<strong>Step 17:</strong> Next install X, video driver, sound, 3D driver</p>\r\n<pre>$sudo pacman -S xorg-server xorg-server-utils xorg-xinit mesa libgl alsa-utils xf86-input-synaptics xf86-input-evdev rxvt-unicode dwm\r\n</pre><p>\r\n	I personally do not like xterm nor xorg-twm so if you do like then install.</p><p>\r\n	<em>xorg-twm</em> and <em>xterm</em> are the default window manager and terminal in order to get X working. My personal\r\nfavourite window manager (WM) is dwm so if you like any other or desktop \r\nenvironments (DE) you can choose something else.</p><p>\r\n	Next you will have to find which VGA driver you have</p>\r\n<pre>$lspci | grep VGA\r\n</pre><p>\r\n	The answer you get is quite clear, so for instance if the answer is like this:</p><p>\r\n	<em>00:02.0 VGA compatible controller: Intel Corporation 3rd Gen Core processor Graphics Controller (rev 09)</em></p><p>\r\n	You will have to install the Intel driver. If you are unsure have a look at this <a target="_blank" href="https://wiki.archlinux.org/index.php/Xorg#Driver_installation" target="_blank">page</a>. Archlinux users have provided with a complete table of the drivers.</p><p>\r\n	<strong>Step 18:</strong> Copy the default xinitrc to your home</p>\r\n<pre>$cp /etc/X11/xinit/xinitrc ~/.xinitrc\r\n$nano .xinitrc\r\n</pre><p>\r\n	remove the last 5 lines and add "exec dwm" or "exec awesome" if you installed awesome window manager. Now you can just run this command</p>\r\n<pre>$startx\r\n</pre><p>\r\n	 and voila a fully working Archlinux without much efforts. I will cover some nice applications you can install for Archlinux in some later posts.</p><p>\r\n	Enjoy.</p>', 'archlinux', 2, 1339896209, 1339896209, 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_role`
--

CREATE TABLE IF NOT EXISTS `tbl_role` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_role`
--

INSERT INTO `tbl_role` (`id`, `name`) VALUES
  (1, 'admin'),
  (2, 'user'),
  (3, 'editor');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tag`
--

CREATE TABLE IF NOT EXISTS `tbl_tag` (
  `id` int(11) NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `frequency` int(11) DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=33 ;

--
-- Dumping data for table `tbl_tag`
--

INSERT INTO `tbl_tag` (`id`, `name`, `frequency`) VALUES
  (1, 'prove', 1),
  (2, 'engineer', 1),
  (3, 'physics', 1),
  (4, 'back', 1),
  (5, 'domainname', 1),
  (6, 'projecteuler', 18),
  (7, 'fibonacci', 2),
  (8, 'largest', 2),
  (9, 'prime', 3),
  (10, 'palindrome', 1),
  (11, 'lcm', 1),
  (12, 'product', 1),
  (13, 'consecutive', 1),
  (14, 'pythagorian', 1),
  (15, 'triplet', 1),
  (17, 'divisors', 1),
  (18, 'trianglenumber', 1),
  (19, 'collatz', 1),
  (20, 'factorialsum', 1),
  (21, 'distinct', 1),
  (22, 'powers', 1),
  (23, 'flying', 1),
  (24, 'airplane', 1),
  (25, 'flyingmachine', 1),
  (26, 'worldwar', 1),
  (27, 'red', 1),
  (28, 'baron', 1),
  (29, 'fokker', 1),
  (30, 'jet', 1),
  (31, 'fighters', 1),
  (32, 'test', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id` int(11) NOT NULL,
  `email` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `registered` int(11) NOT NULL,
  `roleid` int(11) NOT NULL,
  `avatar` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `email`, `name`, `username`, `password`, `registered`, `roleid`, `avatar`) VALUES
  (1, 'vietha20@gmail.com', 'Viet Ha Nguyen', 'vietha20', 'dbbda7839f6a1cfe49f630e70fdfa187', 1230952187, 1, '5451-rets-vinnarkorv.jpg'),
  (2, 'vietha21@gmail.com', 'Viet Ha Nguyen', 'vietha21', 'dbbda7839f6a1cfe49f630e70fdfa187', 1230952187, 1, '1090-2014-03-09-203821_1920x1080_scrot.png'),
  (3, 'vietha22@gmail.com', 'Viet Ha Nguyen', 'vietha22', 'dbbda7839f6a1cfe49f630e70fdfa187', 1230952187, 3, 'noavatar.png'),
  (4, 'vietha23@gmail.com', 'Viet Ha Nguyen', 'vietha23', 'dbbda7839f6a1cfe49f630e70fdfa187', 1230952187, 3, 'noavatar.png'),
  (5, 'vietha24@gmail.com', 'Viet Ha Nguyen', 'vietha24', 'dbbda7839f6a1cfe49f630e70fdfa187', 1230952187, 2, 'noavatar.png'),
  (6, 'vietha25@gmail.com', 'Viet Ha Nguyen', 'vietha25', 'dbbda7839f6a1cfe49f630e70fdfa187', 1230952187, 2, 'noavatar.png'),
  (7, 'vietha26@gmail.com', 'Viet Ha Nguyen', 'vietha26', 'dbbda7839f6a1cfe49f630e70fdfa187', 1230952187, 2, '7163-screenshoot_scrot.png'),
  (8, 'vietha27@gmail.com', 'Viet Ha Nguyen', 'vietha27', 'dbbda7839f6a1cfe49f630e70fdfa187', 1230952187, 2, 'noavatar.png'),
  (9, 'vietha28@gmail.com', 'Viet Ha Nguyen', 'vietha28', 'dbbda7839f6a1cfe49f630e70fdfa187', 1230952187, 2, 'noavatar.png'),
  (10, 'vietha29@gmail.com', 'Viet Ha Nguyen', 'vietha29', 'dbbda7839f6a1cfe49f630e70fdfa187', 1396015141, 2, 'noavatar.png');

--
-- Table structure for table `tbl_airline`
--

-- CREATE TABLE IF NOT EXISTS `tbl_airline` (
--   `id` int(11) NOT NULL,
--   `airlinename` varchar(64) NOT NULL,
--   `alias` varchar(64) NOT NULL,
--   `iata` char(2) NOT NULL,
--   `icao` char(3) NOT NULL,
--   `callsign` varchar(20) NOT NULL,
--   `country` varchar(64) NOT NULL,
--   `active` char(1) NOT NULL
-- ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;


--
-- Table structure for table `tbl_airport`
--

-- CREATE TABLE IF NOT EXISTS `tbl_airport` (
--   `id` int(11) NOT NULL,
--   `airportname` varchar(120) NOT NULL,
--   `city` varchar(120) NOT NULL,
--   `country` varchar(120) NOT NULL,
--   `iata` char(3) DEFAULT NULL,
--   `icao` char(4) DEFAULT NULL,
--   `latitude` float(10,7) NOT NULL,
--   `longitude` float(10,7) NOT NULL,
--   `altitude` int(11) NOT NULL,
--   `timezone` int(2) NOT NULL,
--   `dst` char(1) NOT NULL
-- ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;


--
-- Table structure for table `tbl_gallery`
--

CREATE TABLE IF NOT EXISTS `tbl_gallery` (
  `id` int(11) NOT NULL,
  `name` varchar(120) NOT NULL,
  `icon` text NOT NULL,
  `createtime` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

--
-- Dumping data for table `tbl_gallery`
--

INSERT INTO `tbl_gallery` (`id`, `name`, `icon`, `createtime`) VALUES
  (1, 'Seasons', '5467-Seasons.jpg', 1406238369);

--
-- Table structure for table `tbl_album`
--

CREATE TABLE IF NOT EXISTS `tbl_album` (
  `id` int(11) NOT NULL,
  `name` varchar(120) NOT NULL,
  `icon` text NOT NULL,
  `createtime` int(11) NOT NULL,
  `galleryid` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_authassignment`
--
ALTER TABLE `tbl_authassignment`
ADD PRIMARY KEY (`itemname`,`userid`);

--
-- Indexes for table `tbl_authitem`
--
ALTER TABLE `tbl_authitem`
ADD PRIMARY KEY (`name`);

--
-- Indexes for table `tbl_authitemchild`
--
ALTER TABLE `tbl_authitemchild`
ADD PRIMARY KEY (`parent`,`child`), ADD KEY `child` (`child`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_comment`
--
ALTER TABLE `tbl_comment`
ADD PRIMARY KEY (`id`), ADD KEY `FK_comment_post` (`postid`);

--
-- Indexes for table `tbl_friend`
--
ALTER TABLE `tbl_friend`
ADD PRIMARY KEY (`userid`,`friendid`), ADD KEY `FK_friend_user1` (`userid`), ADD KEY `FK_friend_user2` (`friendid`);

--
-- Indexes for table `tbl_lookup`
--
ALTER TABLE `tbl_lookup`
ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_lyric`
--
ALTER TABLE `tbl_lyric`
ADD PRIMARY KEY (`id`), ADD KEY `title` (`title`), ADD KEY `artist` (`artist`);

--
-- Indexes for table `tbl_message`
--
ALTER TABLE `tbl_message`
ADD PRIMARY KEY (`id`), ADD KEY `FK_message_user1` (`senderid`), ADD KEY `FK_message_user2` (`receiverid`);

--
-- Indexes for table `tbl_post`
--
ALTER TABLE `tbl_post`
ADD PRIMARY KEY (`id`), ADD KEY `FK_post_user` (`authorid`), ADD KEY `FK_post_category` (`categoryid`);

--
-- Indexes for table `tbl_role`
--
ALTER TABLE `tbl_role`
ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_tag`
--
ALTER TABLE `tbl_tag`
ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `username` (`username`), ADD UNIQUE KEY `email` (`email`), ADD KEY `role` (`roleid`);

--
-- Indexes for table `tbl_airport`
--
-- ALTER TABLE `tbl_airport`
-- ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_airline`
--
-- ALTER TABLE `tbl_airline`
-- ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_album`
--
ALTER TABLE `tbl_album`
ADD PRIMARY KEY (`id`), ADD KEY `FK_album_gallery` (`galleryid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_comment`
--
ALTER TABLE `tbl_comment`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_lookup`
--
ALTER TABLE `tbl_lookup`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_lyric`
--
ALTER TABLE `tbl_lyric`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_message`
--
ALTER TABLE `tbl_message`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_post`
--
ALTER TABLE `tbl_post`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `tbl_role`
--
ALTER TABLE `tbl_role`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_tag`
--
ALTER TABLE `tbl_tag`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;


--
-- AUTO_INCREMENT for table `tbl_airport`
--
-- ALTER TABLE `tbl_airport`
-- MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_airline`
--
-- ALTER TABLE `tbl_airline`
-- MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_album`
--
ALTER TABLE `tbl_album`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_authassignment`
--
ALTER TABLE `tbl_authassignment`
ADD CONSTRAINT `tbl_authassignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `tbl_authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_authitemchild`
--
ALTER TABLE `tbl_authitemchild`
ADD CONSTRAINT `tbl_authitemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `tbl_authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tbl_authitemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `tbl_authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_comment`
--
ALTER TABLE `tbl_comment`
ADD CONSTRAINT `FK_comment_post` FOREIGN KEY (`postid`) REFERENCES `tbl_post` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_friend`
--
ALTER TABLE `tbl_friend`
ADD CONSTRAINT `FK_friend_user1` FOREIGN KEY (`userid`) REFERENCES `tbl_user` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_friend_user2` FOREIGN KEY (`friendid`) REFERENCES `tbl_user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_message`
--
ALTER TABLE `tbl_message`
ADD CONSTRAINT `FK_message_user1` FOREIGN KEY (`senderid`) REFERENCES `tbl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_message_user2` FOREIGN KEY (`receiverid`) REFERENCES `tbl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_post`
--
ALTER TABLE `tbl_post`
ADD CONSTRAINT `FK_post_category` FOREIGN KEY (`categoryid`) REFERENCES `tbl_category` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_post_user` FOREIGN KEY (`authorid`) REFERENCES `tbl_user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_user`
--
ALTER TABLE `tbl_user`
ADD CONSTRAINT `tbl_user_ibfk_1` FOREIGN KEY (`roleid`) REFERENCES `tbl_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_album`
--
ALTER TABLE `tbl_album`
ADD CONSTRAINT `tbl_album_gallery` FOREIGN KEY (`galleryid`) REFERENCES `tbl_gallery` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
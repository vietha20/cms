<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name'=>'vietha.se',
    'timeZone' => 'Europe/Stockholm',
    'defaultController'=>'/frontend/post/list',

    'aliases' => array(
        'booster' => realpath(__DIR__ . '/../extensions/YiiBooster/src'), // change this if necessary
    ),

    // preloading 'log' component
    'preload'=>array(
        'log',
        'booster',
    ),

    // autoloading model and component classes
    'import'=>array(
        'application.helpers.*',
        'application.models.*',
        'application.components.*',
        'application.modules.backend.models.*',
        'application.modules.backend.components.*',
        'application.modules.frontend.models.*',
        'application.modules.frontend.components.*',
    ),

    'modules'=>array(
        // uncomment the following to enable the Gii tool
        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'a',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters'=>array('127.0.0.1','::1','85.231.30.*'),
        ),
        'backend',
        'frontend',
    ),

    // application components
    'components'=>array(
        'image'=>array(
            'class'=>'application.extensions.image.CImageComponent',
            // GD or ImageMagick
            'driver'=>'GD',
            // ImageMagick setup path
            'params'=>array('directory'=>'/opt/local/bin'),
        ),
        'booster' => array(
            'class' => 'booster.components.Booster',
            'fontAwesomeCss' => true,
        ),
        'user'=>array(
            // enable cookie-based authentication
            'allowAutoLogin'=>true,
            'autoUpdateFlash'=>true,
            'loginUrl'=>array('frontend/login'),
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager'=>array(
            'urlFormat'=>'path',
            'showScriptName'=>false,
            'caseSensitive'=>false,
            'rules'=>array(
                /** Backend part **/
                'backend'=>'/backend/home/index',

                '<module:(backend)>/<controller:(gallery|album)>/<action:(update)>/<id:\d+>'=>'/<module>/<controller>/<action>',
                '<module:(backend)>/<controller:(gallery|album)>/<action:(create)>'=>'/<module>/<controller>/<action>',

                '<module:(backend)>/<controller:(role|user)>/<action:(create)>'=>'/<module>/<controller>/<action>',
                '<module:(backend)>/<controller:(role)>/<action:(view)>/<id:\d+>'=>'/<module>/<controller>/<action>',
                '<module:(backend)>/<controller:(role|user)>/<action:(update)>/<id:\d+>'=>'/<module>/<controller>/<action>',

                '<module:(backend)>/<controller:(post|comment|category)>/<action:(update)>/<id:\d+>'=>'/<module>/<controller>/<action>',
                '<module:(backend)>/<controller:(post|comment|category)>/<action:(create)>'=>'/<module>/<controller>/<action>',

                '<module:(backend)>/<controller:(role|user)>'=>'/<module>/<controller>',
                '<module:(backend)>/<controller:(album|gallery)>'=>'/<module>/<controller>',
                '<module:(backend)>/<controller:(post|comment|category)>'=>'/<module>/<controller>',
                '<module:(backend)>/<controller:(authitem|authitemchild|authassignment)>'=>'/<module>/<controller>',

                /** Frontend part **/
                ''=>'/frontend/post/list',

                '<controller:(lyric)>/<action:(create|update|index)>'=>'/frontend/<controller>/<action>',

                'gallery'=>'/frontend/gallery/index',
                '<controller:(gallery)>/<action:(view)>/<id:\d+>'=>'/frontend/<controller>/<action>',

                '<controller:(message)>/<action:(inbox|outbox)>/<id:\d+>/page/<page:\d+>'=>'/frontend/<controller>/<action>',

                '<controller:(post|category)>/<action:(view)>/<id:\d+>/page/<page:\d+>'=>'/frontend/<controller>/<action>',

                '<controller:(profile)>/<action:(post|friend|findrequest)>/<id:\d+>/page/<page:\d+>'=>'/frontend/<controller>/<action>',

                '<controller:(friend)>/<action:(makefriend)>/<uid:\d+>/<fid:\d+>'=>'/frontend/<controller>/<action>',
                '<controller:(friend)>/<action:(confirmrequest)>/<uid:\d+>/<fid:\d+>/<b:\d+>'=>'/frontend/<controller>/<action>',

                '<controller:(profile)>/<action:(view)>/<id:\d+>'=>'/frontend/<controller>/<action>',

                '<action:(login|logout|contact|statistic|memberlist|register|gallery)>'=>'/frontend/<action>',

                '<action:(searchvideo|routeplanner)>'=>'/frontend/feature/<action>',

                '<controller:(statistic)>/<action:(airquality)>'=>'/frontend/<controller>/<action>',
                '<controller:(statistic)>/<action:(airportstatistic|airlinestatistic)>'=>'/frontend/<controller>/<action>',

                '<controller:(post)>/<action:(search)>'=>'/frontend/<controller>/<action>',
                '<controller:(post)>/<action:(tag)>'=>'/frontend/<controller>/<action>',

                '<controller:\w+>/<action:\w+>/<id:\d+>'=>'/frontend/<controller>/<action>',

                /** Gii module **/
                'gii'=>'gii',
                'gii/<controller:\w+>'=>'gii/<controller>',
                'gii/<controller:\w+>/<action:\w+>'=>'gii/<controller>/<action>',
            ),
        ),
        /*
        'db'=>array(
        'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
        ),*/
        // uncomment the following to use a MySQL database
        'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=i8ul8r_cms',
            'emulatePrepare' => true,
            'username' => 'i8ul8r_cms',
            'password' => '2EtaN2y8ecWtnwzQ',
            'charset' => 'utf8',
            'tablePrefix'=>'tbl_',
            'initSQLs'=>array('set names utf8'),
        ),
        'authManager'=>array(
            'class'=>'CDbAuthManager',
            'connectionID'=>'db',
            'defaultRoles'=>array('guest'),
            'itemTable'=>'{{authitem}}',
            'assignmentTable'=>'{{authassignment}}',
            'itemChildTable'=>'{{authitemchild}}',
        ),
        'errorHandler'=>array(
            // use 'site/error' action to display errors
            'errorAction'=>'frontend/error',
        ),
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning',
                ),
                // uncomment the following to show log messages on web pages
                /*
                array(
                'class'=>'CWebLogRoute',
                ),
                 */
            ),
        ),
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'=>require(dirname(__FILE__).'/params.php'),
);

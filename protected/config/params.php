<?php

// this contains the application parameters that can be maintained via GUI
return array(
    // this is displayed in the header section
    'title'=>'vietha.se',
    // this is used in error pages
    'adminEmail'=>'vietha20@gmail.com',
    // number of posts displayed per page
    'postsPerPage'=>5,
    // maximum number of comments that can be displayed in recent comments portlet
    'recentCommentCount'=>5,
    // avatar width (resize)
    'avatarWidth'=>'160',
    // avatar height (resize)
    'avatarHeight'=>'160',
    // avatar width (resize)
    'thumbnailWidth'=>'200',
    // avatar height (resize)
    'thumbnailHeight'=>'200',
    // maximum number of tags that can be displayed in tag cloud portlet
    'tagCloudCount'=>40,
    // whether post comments need to be approved before published
    'commentNeedApproval'=>true,
    // the upload path for avatar images
    'avatarPath'=>dirname(__FILE__).'/../../uploads/avatar/',
    // the upload path for gallery
    'galleryPath'=>dirname(__FILE__).'/../../uploads/gallery/',
    // the copyright information displayed in the footer section
    'copyrightInfo'=>'Copyright &copy; 2009 :: ' . date('Y') . ' by vietha.se.',
);

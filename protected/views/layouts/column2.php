<?php $this->beginContent('//layouts/main'); ?>
<div class="col-sm-4">
    <?php $this->widget('Frontend'); ?>
</div>
<div class="col-sm-8">
    <?php if(Yii::app()->user->checkAccess('user')) {
        $this->widget('booster.widgets.TbTabs', array(
            'type'=>'tabs',
            'stacked'=>false,
            'tabs'=>$this->menu,
        ));
    } else {
        $this->widget('booster.widgets.TbTabs', array(
            'type'=>'tabs',
            'stacked'=>false,
            'tabs'=>$this->menu,
        ));
    } ?>
    <br>
    <?php echo $content; ?>
</div>
<?php $this->endContent(); ?>

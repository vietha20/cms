<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />
    <title>
        <?php echo CHtml::encode($this->pageTitle); ?>
    </title>
    <?php $baseUrl = Yii::app()->request->baseUrl; ?>
    <link rel="stylesheet" type="text/css"
          href="<?php echo $baseUrl; ?>/css/main.css" />
    <script type="text/javascript">
        $(document).ready(function(){

            //Check to see if the window is top if not then display button
            $(window).scroll(function(){
                if ($(this).scrollTop() > 100) {
                    $('.scrollToTop').fadeIn();
                } else {
                    $('.scrollToTop').fadeOut();
                }
            });

            //Click event to scroll to top
            $('.scrollToTop').click(function(){
                $('html, body').animate({scrollTop : 0},800);
                return false;
            });

        });
    </script>
</head>
<body
<div class="container">
    <?php require_once('navigation.php'); ?>
    <?php require_once('breadcrumbs.php'); ?>

    <hr class="style-one">

    <div class="row">
        <a href="#" class="scrollToTop"></a>
        <?php echo $content; ?>
    </div>

    <hr class="style-one">
    <div class="row">
        <?php require_once('menus.php'); ?>
    </div>

    <hr class="style-one">
    <div class="row">
        <?php require_once('footer.php'); ?>
    </div>
</div><!-- page -->
</body>
</html>

<footer>
    <small>
        <div class="col-md-8">
            <p class="pull-left">
                Powered by <a target="_blank" href="http://www.yiiframework.com">Yii</a> /
                <a target="_blank" href="http://yiibooster.clevertech.biz/">Yii-Booster</a>
            </p>
        </div>
        <div class="col-md-4">
            <p class="pull-right"><?php echo Yii::app()->params['copyrightInfo']; ?> </p>
        </div>
    </small>
</footer>


<?php $this->widget('booster.widgets.TbNavbar', array(
    'brand' => '<i class="fa fa-home"></i> '.Yii::app()->name,
    'brandOptions' => array(
        'style' => 'width:auto;margin-left: 0px;'
    ),
    'fluid' => false,
    'fixed' => 'false top',
    'collapse' => 'true',
    'items' => array(
        array(
            'class' => 'booster.widgets.TbMenu',
            'type' => 'navbar',
            'items' => array(
                array(
                    'label' => 'Contact',
                    'icon' => 'fa fa-envelope',
                    'url' => array('/frontend/contact'),
                    'active' => $this->id == 'contact'
                ),
                array(
                    'label' => 'Statistics',
                    'icon' => 'fa fa-signal',
                    'url' => array('/frontend/statistic'),
                    'active' => $this->id == 'statistic'
                ),
                array(
                    'label' => 'Galleries',
                    'icon' => 'fa fa-camera-retro',
                    'url' => array('/frontend/gallery'),
                    'active' => $this->id == 'gallery',
                ),
                array(
                    'label' => 'Cool Features',
                    'icon' => 'fa fa-globe',
                    'url' => '#',
                    'active' => $this->id == 'feature',
                    'items' => array(
                        array(
                            'label' => 'Search Videos',
                            'icon' => 'fa fa-youtube-play',
                            'url' => array('/frontend/feature/searchvideo'),
                            'active' => $this->action->id == 'searchvideo'
                        ),
                        array(
                            'label' => 'Route Planner',
                            'icon' => 'fa fa-map-marker',
                            'url' => array('/frontend/feature/routeplanner'),
                            'active' => $this->action->id == 'routeplanner'
                        ),
                    ),
                ),
            ),
        ),
        array(
            'class' => 'booster.widgets.TbMenu',
            'type' => 'navbar',
            'htmlOptions' => array('class' => 'pull-right'),
            'items' => array(
                array(
                    'label' => 'Backend',
                    'icon' => 'fa fa-cogs',
                    'url' => '/backend',
                    'active' => $this->module->id == 'backend',
                    'visible' => Yii::app()->user->checkAccess('admin') ||
                        Yii::app()->user->checkAccess('editor')
                ),
                array(
                    'label' => 'Messages',
                    'icon' => 'fa fa-inbox',
                    'url' => array(
                        '/frontend/message/inbox',
                        'id' => Yii::app()->user->id,
                    ),
                    'visible' => Yii::app()->user->checkAccess('user'),
                ),
                array(
                    'label' => 'Profile',
                    'icon' => 'fa fa-cog',
                    'url' => '#',
                    'visible' => Yii::app()->user->checkAccess('user'),
                    'active' => $this->id == 'profile',
                    'items' => array(
                        array(
                            'label' => 'View Profile',
                            'icon' => 'fa fa-eye',
                            'url' => array(
                                '/frontend/profile/view/',
                                'id' => Yii::app()->user->getId())
                        ),
                        array(
                            'label' => 'Edit Profile',
                            'icon' => 'fa fa-pencil',
                            'url' => array(
                                '/frontend/profile/update/',
                                'id' => Yii::app()->user->getId())
                        ),
                        '---',
                        array(
                            'label' => 'Logout (' . Yii::app()->user->name . ')',
                            'icon' => 'fa fa-sign-out',
                            'url' => array('/frontend/logout')
                        )
                    )
                ),
                array(
                    'label' => 'Login',
                    'icon' => 'fa fa-sign-in',
                    'url' => array('/frontend/login'),
                    'active' => $this->id == 'login',
                    'visible' => Yii::app()->user->checkAccess('guest')
                ),
                array(
                    'label' => 'Register',
                    'icon' => 'fa fa-key',
                    'url' => array('/frontend/register'),
                    'active' => $this->id == 'register',
                    'visible' => Yii::app()->user->checkAccess('guest')
                )
            )
        )
    )
));
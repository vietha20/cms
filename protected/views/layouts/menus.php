<div class="col-sm-4">
    <?php $this->widget('WeatherWarnings'); ?>
</div>
<div class="col-sm-4">
    <?php $this->widget('SmhiJobs'); ?>
</div>
<div class="col-sm-4">
    <?php $this->widget('TagCloud', array(
        'maxTags'=>Yii::app()->params['tagCloudCount'],)); ?>
</div>

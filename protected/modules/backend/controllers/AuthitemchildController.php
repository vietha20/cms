<?php

class AuthitemchildController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '/layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'roles' => array('admin'),
            ),
            array(
                'deny', // deny all users
                'roles' => array('guest', 'user'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param $parent
     * @param $child
     */
    public function actionView($parent, $child)
    {
        $this->render('view', array(
            'model' => $this->loadModel($parent, $child),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new BackendAuthitemchild('create');
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if (isset($_POST['BackendAuthitemchild'])) {
            $model->attributes = $_POST['BackendAuthitemchild'];
            if ($model->save()) {
                $this->redirect(array('authitemchild/'));
            }
        }
        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param $parent
     * @param $child
     */
    public function actionUpdate($parent, $child)
    {
        $model = $this->loadModel($parent, $child);
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if (isset($_POST['BackendAuthitemchild'])) {
            $model->attributes = $_POST['BackendAuthitemchild'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', Yii::t('backendcontroller', 'Authitemchild was updated successfully.'));
            }
            $this->redirect(array('/backend/authitemchild/view', 'parent' => $model->parent,
                'child' => $model->child));
        }
        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param $parent
     * @param $child
     */
    public function actionDelete($parent, $child)
    {
        $this->loadModel($parent, $child)->delete();
        // if AJAX request (triggered by deletion via admin grid view),
        // we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ?
                $_POST['returnUrl'] :
                array('index'));
        }

    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $model = new BackendAuthitemchild('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['BackendAuthitemchild'])) {
            $model->attributes = $_GET['BackendAuthitemchild'];
        }
        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param $parent
     * @param $child
     * @return mixed
     * @throws CHttpException
     */
    public function loadModel($parent, $child)
    {
        $model = BackendAuthitemchild::model()->findByPk(array(
            'parent' => $parent, 'child' => $child));
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Authitemchild $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'authitemchild-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}

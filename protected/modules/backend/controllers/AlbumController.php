<?php

class AlbumController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '/layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow', // allow admin users to do everything
                'roles' => array('admin'),
            ),
            array(
                'deny', // deny all users
                'roles' => array('guest', 'user'),
            ),
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new BackendAlbum('create');

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $thumbnailWidth = Yii::app()->params['thumbnailWidth'];
        $thumbnailHeight = Yii::app()->params['thumbnailHeight'];

        $noicon = "gallery-no-icon.png";
        if (isset($_POST['BackendAlbum'])) {
            $model->attributes = $_POST['BackendAlbum'];
            $albumIcon = CUploadedFile::getInstance($model, 'icon');
            $albumName = $_POST['BackendAlbum']['name'];
            $galleryName = $this->loadGalleryModel($_POST['BackendAlbum']['galleryid'])->name;
            $galleryPath = Yii::app()->params['galleryPath'];
            $albumPath = $galleryPath.$galleryName;

            //if the directory does not exists create it
            if(!is_dir($albumPath.'/'.$albumName)) {
                mkdir($albumPath.'/'.$albumName, 0755);
            } else {
                Yii::app()->user->setFlash('error', 'Album already exists.');
                $this->redirect(array('/backend/album/create'));
            }

            $rnd = rand(0, 9999); // generate random number between 0-9999

            if (!empty($albumIcon)) {
                $extension = $albumIcon->getExtensionName();
                $tempimage = Yii::app()->image->load($albumIcon->tempName);
                $tempimage->resize($thumbnailWidth,$thumbnailHeight);

                $fileName = "{$rnd}-{$albumName}.{$extension}"; // random number + file name

                $model->icon = $fileName;
                $tempimage->save($albumPath.'/'.$albumName.'/'.$fileName);
            } else {
                //if admin decides not to upload any image then change the default one.
                copy($galleryPath.$noicon, $albumPath.'/'.$albumName.'/'.$noicon);
                $tempimage = Yii::app()->image->load($albumPath.'/'.$albumName.'/'.$noicon);
                $extension = $tempimage->ext;
                $fileName = "{$rnd}-{$albumName}.{$extension}"; // random number + file name

                //also rename the thumbnail of the gallery
                rename($albumPath.'/'.$albumName.'/'.$noicon, $albumPath.'/'.$albumName.'/'.$fileName);
                $model->icon = $fileName;
            }

            $model->createtime = time();

            //try/catch database errors
            try {
                if ($model->save()) {
                    $this->redirect(array('/backend/album'));
                }
            } catch (CDbException $e) {
                Yii::app()->user->setFlash('error', $e->getMessage());
                $this->redirect(array('/backend/album/update/', 'id' => $model->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $thumbnailWidth = Yii::app()->params['thumbnailWidth'];
        $thumbnailHeight = Yii::app()->params['thumbnailHeight'];
        if (isset($_POST['BackendAlbum'])) {
            $galleryPath = Yii::app()->params['galleryPath'];

            $oldAlbumIcon = $model->icon;
            $newAlbumIcon = CUploadedFile::getInstance($model, 'icon');

            $oldAlbumName = $model->name;
            $newAlbumName = $_POST['BackendAlbum']['name'];

            $oldGalleryName = $this->loadGalleryModel($model->galleryid)->name;
            $newGalleryName = $this->loadGalleryModel($_POST['BackendAlbum']['galleryid'])->name;

            $newGalleryId = $_POST['BackendAlbum']['galleryid'];

            $oldAlbumPath = $galleryPath.$oldGalleryName;
            $newAlbumPath = $galleryPath.$newGalleryName;

            $rnd = rand(0, 9999); // generate random number between 0-9999

            //if changed album thumbnail
            if (!empty($newAlbumIcon)) { // check if uploaded file is set or not
                $extension = $newAlbumIcon->getExtensionName();
                $tempimage = Yii::app()->image->load($newAlbumIcon->tempName);
                $tempimage->resize($thumbnailWidth,$thumbnailHeight);

                $fileName = "{$rnd}-{$model->name}.{$extension}"; // random number + file name
                @unlink($oldAlbumPath.'/'.$model->name.'/'.$model->icon);
                $model->icon = $fileName;
                $tempimage->save($oldAlbumPath.'/'.$model->name.'/'.$fileName);
            }

            if ($model->name !== $newAlbumName) {
                if(!is_dir($oldAlbumPath.'/'.$newAlbumName)) {
                    rename($oldAlbumPath.'/'.$model->name, $oldAlbumPath.'/'.$newAlbumName);
                    $model->name = $newAlbumName;

                    $tempimage = Yii::app()->image->load($oldAlbumPath.'/'.$newAlbumName.'/'.$model->icon);
                    $extension = $tempimage->ext;
                    $fileName = "{$rnd}-{$newAlbumName}.{$extension}"; // random number + file name

                    //also rename the thumbnail of the gallery
                    rename($oldAlbumPath.'/'.$newAlbumName.'/'.$model->icon, $oldAlbumPath.'/'.$newAlbumName.'/'.$fileName);
                    $model->icon = $fileName;
                } else {
                    Yii::app()->user->setFlash('error', 'Album already exists.');
                    $this->redirect(array('/backend/album/update', 'id' => $model->id));
                }
            }

            //if changed gallery
            if ($model->galleryid !== $newGalleryId) {
                if(!is_dir($newAlbumPath.'/'.$newAlbumName)) { //not created the album in the new gallery
                    //rename the album to the new gallery location.
                    rename($oldAlbumPath.'/'.$model->name, $newAlbumPath.'/'.$newAlbumName);
                    $model->galleryid = $newGalleryId;
                } else { //if already created, redo the last action of changing the new of the album/icon name.
                    rename($oldAlbumPath.'/'.$newAlbumName, $oldAlbumPath.'/'.$oldAlbumName);

                    //also rename the thumbnail of the gallery
                    rename($oldAlbumPath.'/'.$oldAlbumName.'/'.$model->icon, $oldAlbumPath.'/'.$oldAlbumName.'/'.$oldAlbumIcon);

                    Yii::app()->user->setFlash('error', 'Gallery already exists.');
                    $this->redirect(array('/backend/album/update', 'id' => $model->id));
                }
            }

            //try/catch database errors
            try {
                if ($model->save()) {
                    $this->redirect(array('/backend/album/'));
                }
            } catch (CDbException $e) {
                Yii::app()->user->setFlash('error', $e->getMessage());
                $this->redirect(array('/backend/gallery/update/', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        $galleryPath = Yii::app()->params['galleryPath'];
        $galleryName = $this->loadGalleryModel($model->galleryid)->name;
        $albumName = $model->name;
        $albumPath = $galleryPath.$galleryName;

        $files = glob($albumPath.'/'.$albumName.'/*.*'); // get all file names

        foreach($files as $file) { // iterate files
            if(is_file($file)) {
                unlink($file); // delete file
            }
        }

        rmdir($albumPath.'/'.$albumName); //remove album folder

        $model->delete(); //remove from database
        // if AJAX request (triggered by deletion via admin grid view),
        // we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ?
                $_POST['returnUrl'] :
                array('index'));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $model = new BackendAlbum('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['BackendAlbum'])) {
            $model->attributes = $_GET['BackendAlbum'];
        }
        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Category the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = BackendAlbum::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Category the loaded model
     * @throws CHttpException
     */
    public function loadGalleryModel($id)
    {
        $model = BackendGallery::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Category $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'album-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}

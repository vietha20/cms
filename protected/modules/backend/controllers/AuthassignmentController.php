<?php

class AuthassignmentController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '/layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow', // allow admin user to perform 'admin' and 'delete' actions
                'roles' => array('admin'),
            ),
            array(
                'deny', // deny all users
                'roles' => array('guest', 'user'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param $itemname
     * @param $userid
     */
    public function actionView($itemname, $userid)
    {
        $this->render('view', array(
            'model' => $this->loadModel($itemname, $userid),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new BackendAuthassignment('create');

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if (isset($_POST['BackendAuthassignment'])) {
            $model->attributes = $_POST['BackendAuthassignment'];
            if ($model->save()) {
                $this->redirect(array('authassignment/'));
            }
        }
        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param $itemname
     * @param $userid
     */
    public function actionUpdate($itemname, $userid)
    {
        $model = $this->loadModel($itemname, $userid);
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if (isset($_POST['BackendAuthassignment'])) {
            $model->attributes = $_POST['BackendAuthassignment'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', Yii::t('backendcontroller', 'Authassignment was updated successfully.'));
            }
            $this->redirect(array('/backend/authassignment/view', 'itemname' => $model->itemname,
                'userid' => $model->userid));
        }
        $this->render('update', array(
            'model' => $model,
        ));
    }


    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param $itemname
     * @param $userid
     */
    public function actionDelete($itemname, $userid)
    {
        $this->loadModel($itemname, $userid)->delete();
        // if AJAX request (triggered by deletion via admin grid view),
        // we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ?
                $_POST['returnUrl'] :
                array('index'));
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $model = new BackendAuthassignment('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['BackendAuthassignment'])) {
            $model->attributes = $_GET['BackendAuthassignment'];
        }
        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param $itemname
     * @param $userid
     * @return mixed
     * @throws CHttpException
     */
    public function loadModel($itemname, $userid)
    {
        $model = BackendAuthassignment::model()->findByPk(array(
            'itemname' => $itemname, 'userid' => $userid));
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Authassignment $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'authassignment-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}

<?php

class CommentController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '/layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow', // allow user users to access all actions
                'roles' => array('admin', 'editor'),
            ),
            array(
                'deny', // deny all users
                'roles' => array('guest', 'user'),
            ),
        );
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if (isset($_POST['BackendComment'])) {
            $model->attributes = $_POST['BackendComment'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', Yii::t('backendcontroller', 'Comment was updated successfully.'));
            }
            $this->redirect(array($model->getUrl()));
        }
        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();
        // if AJAX request (triggered by deletion via admin grid view),
        // we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ?
                $_POST['returnUrl'] :
                array('index'));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $model = new BackendComment('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['BackendComment'])) {
            $model->attributes = $_GET['BackendComment'];
        }
        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Approves a particular comment.
     * If approval is successful, the browser will be redirected to the comment index page.
     * @param integer $id the ID of the model to be approved
     * @param integer $status the status of the comment
     */
    public function actionChangeStatus($id, $status)
    {
        $comment = $this->loadModel($id);
        switch ($status) {
            case 1:
                $comment->unapprove();
                break;
            case 2:
                $comment->approve();
                break;
        }
        $this->redirect(array('index'));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return BackendComment the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = BackendComment::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }
}

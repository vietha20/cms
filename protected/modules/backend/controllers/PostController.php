<?php

class PostController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '/layouts/column2';

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xF5F5F5,
            ),
        );
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow', // allow specific users to access all actions
                'roles' => array('admin', 'editor'),
            ),
            array(
                'deny', // deny all users
                'roles' => array('guest', 'user'),
            ),
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new BackendPost('create');
        if (isset($_POST['BackendPost'])) {
            $model->attributes = $_POST['BackendPost'];
            if ($model->save()) {
                $this->redirect(array('/frontend/post/view', 'id' => $model->id));
            }
        }
        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        if (isset($_POST['BackendPost'])) {
            $model->attributes = $_POST['BackendPost'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', Yii::t('backendcontroller', 'Post was updated successfully.'));
            }
            $this->redirect(array('/frontend/post/view', 'id' => $id));
        }
        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     */
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view),
            // we should not redirect the browser
            if (!isset($_GET['ajax'])) {
                $this->redirect(isset($_POST['returnUrl']) ?
                    $_POST['returnUrl'] :
                    array('index'));
            }
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $model = new BackendPost('search');
        if (isset($_GET['BackendPost'])) {
            $model->attributes = $_GET['BackendPost'];
        }
        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Suggests tags based on the current user input.
     * This is called via AJAX when the user is entering the tags input.
     */
    public function actionSuggestTags()
    {
        if (isset($_GET['term']) && ($keyword = trim($_GET['term'])) !== '') {
            $tags = BackendTag::model()->suggestTags($keyword);
            echo CJSON::encode($tags);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Post the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = BackendPost::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }
}

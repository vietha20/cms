<?php

class UserController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '/layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow', // allowance for admin users
                'roles' => array('admin'),
            ),
            array(
                'deny', // deny all users
                'roles' => array('guest', 'user'),
            ),
        );
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{user}}';
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new BackendUser('create');

        if (isset($_POST['BackendUser'])) {
            $model->attributes = $_POST['BackendUser'];
            if ($model->validate()) {
                $unhashedPassword = $model->password;
                $model->password = $model->hashPassword($model->password);
                $model->registered = time();
                if (isset($_POST['BackendUser']['roleid']) &&
                    !empty($_POST['BackendUser']['roleid'])
                ) {
                    $model->roleid = $_POST['BackendUser']['roleid'];
                } else {
                    $model->roleid = 2;
                }
                $model->avatar = "noavatar.png";
                try {
                    if ($model->save(false)) {
                        $model->password = $unhashedPassword;
                        $auth = Yii::app()->authManager;
                        $auth->assign('user', $model->id);
                        $this->redirect(array('/backend/user'));
                    }
                } catch (CDbException $e) {
                    Yii::app()->user->setFlash('error', $e->getMessage());
                }
            }
        }
        $this->render('/user/create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $model = $this->loadModel($id);
        if (isset($_POST['BackendUser'])) {
            $roleid = (int)$_POST['BackendUser']['roleid'];
            $password = $model->hashPassword($_POST['BackendUser']['password']);
            $username = $_POST['BackendUser']['username'];
            $email = $_POST['BackendUser']['email'];
            $roles = BackendRole::model()->findAll();

            //if changing username
            if ($model->username !== $username) {
                $model->username = $username;
            } else {
                unset($model->username);
            }

            //if changing role
            if ($model->roleid !== $roleid && (int)$model->id !== 1) {
                $auth = Yii::app()->authManager;
                foreach ($roles as $role) {
                    if ($roleid == $role->id) {
                        $newrole = $role->name;
                    }
                }
                //current user assignment
                $currentassignment = key($auth->getAuthAssignments($id));
                $model->roleid = $roleid;
                $auth->revoke($currentassignment, $model->id);
                $auth->assign($newrole, $model->id);
            } else {
                unset($model->roleid);
            }

            //if changing email
            if ($model->email !== $email) {
                $model->email = $email;
            } else {
                unset($model->email);
            }

            //if changing password
            if (isset($_POST['BackendUser']['password'])) {
                if ($model->password == $_POST['BackendUser']['password']) {
                    unset($model->password);
                } elseif ($model->password !== $password) {
                    $model->password = $password;
                } else {
                    unset($model->password);
                }
            }

            //try/catch database errors
            try {
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', Yii::t('backendcontroller', 'User profile was updated successfully.'));
                    $this->redirect(array('/frontend/profile/view', 'id' => $model->id));
                }
            } catch (CDbException $e) {
                Yii::app()->user->setFlash('error', $e->getMessage());
                $this->redirect(array('/backend/user/update', 'id' => $model->id));
            }
        }
        $this->render('/user/update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        // Cannot delete first user
        if ($id == 1) {
            throw new CHttpException(400);
        } else {
            $this->loadModel($id)->delete();
            // if AJAX request (triggered by deletion via admin grid view),
            // we should not redirect the browser
            $auth = Yii::app()->authManager;
            switch ($this->loadModel($id)->roleid) {
                case 1:
                    $auth->revoke('admin', $id);
                    break;
                case 2:
                    $auth->revoke('user', $id);
                    break;
            }
            if (!isset($_GET['ajax'])) {
                $this->redirect(isset($_POST['returnUrl']) ?
                    $_POST['returnUrl'] :
                    array('index'));
            }
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $model = new BackendUser('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['BackendUser'])) {
            $model->attributes = $_GET['BackendUser'];
        }
        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return User the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = BackendUser::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param User $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}

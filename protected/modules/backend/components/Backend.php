<?php

Yii::import('zii.widgets.CPortlet');

class Backend extends CPortlet
{
    protected function renderContent()
    {
        $module = Yii::app()->controller->module->id;
        $adminaccess = Yii::app()->user->checkAccess('admin');
        $editoraccess = Yii::app()->user->checkAccess('editor');
        $controller = Yii::app()->controller->id;

        if ($adminaccess && $module == 'backend') {
            switch ($controller) {
                case 'default':
                    $this->render('backendmain');
                    break;
                case 'post':
                case 'category':
                case 'comment':
                    $this->render('content');
                    break;
                case 'user':
                case 'role':
                    $this->render('userrole');
                    break;
                case 'authitem':
                case 'authitemchild':
                case 'authassignment':
                    $this->render('rbac');
                    break;
                case 'gallery':
                case 'album':
                    $this->render('gallery');
                    break;
            }
        } elseif ($editoraccess && $module == 'backend') {
            switch ($controller) {
                case 'default':
                    $this->render('backendmain');
                    break;
                case 'post':
                case 'category':
                case 'comment':
                    $this->render('content');
                    break;
            }
        }
    }
}

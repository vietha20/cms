<?php
$cid = $this->controller->id;
$aid = $this->controller->action->id;

$this->widget('booster.widgets.TbMenu', array(
    'type' => 'pills',
    'stacked' => true,
    'items' => array(
        array(
            'label' => Yii::t('backendmenu', 'Create Authitem'),
            'icon' => 'fa fa-check-circle',
            'url' => array('/backend/authitem/create')
        ),
        array(
            'label' => Yii::t('backendmenu', 'Manage Authitems'),
            'icon' => 'fa fa-list',
            'active' => $cid == 'authitem' && $aid == 'index',
            'url' => array('/backend/authitem'),
        ),
        array(
            'label' => Yii::t('backendmenu', 'Create Authitemchild'),
            'icon' => 'fa fa-check-circle-o',
            'url' => array('/backend/authitemchild/create')
        ),
        array(
            'label' => Yii::t('backendmenu', 'Manage Authitemchildren'),
            'icon' => 'fa fa-list',
            'active' => $cid == 'authitemchild' && $aid == 'index',
            'url' => array('/backend/authitemchild')
        ),
        array(
            'label' => Yii::t('backendmenu', 'Create Authassignment'),
            'icon' => 'fa fa-link',
            'url' => array('/backend/authassignment/create')
        ),
        array(
            'label' => Yii::t('backendmenu', 'Manage Authassignments'),
            'icon' => 'fa fa-list',
            'active' => $cid == 'authassignment' && $aid == 'index',
            'url' => array('/backend/authassignment')
        )
    )
));

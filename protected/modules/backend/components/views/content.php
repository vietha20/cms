<?php
$cid = $this->controller->id;
$aid = $this->controller->action->id;

$this->widget('booster.widgets.TbMenu', array(
    'type' => 'pills',
    'stacked' => true,
    'items' => array(
        array(
            'label' => Yii::t('backendmenu', 'Create Post'),
            'url' => array('/backend/post/create'),
            'icon' => 'fa fa-file-text-o',
        ),
        array(
            'label' => Yii::t('backendmenu', 'Manage Posts'),
            'active' => $cid == 'post' && $aid == 'index',
            'url' => array('/backend/post'),
            'icon' => 'fa fa-list',
        ),
        array(
            'label' => Yii::t('backendmenu', 'Create Category'),
            'url' => array('/backend/category/create'),
            'icon' => 'fa fa-folder-open-o',
        ),
        array(
            'label' => Yii::t('backendmenu', 'Manage Categories'),
            'active' => $cid == 'category' && $aid == 'index',
            'url' => array('/backend/category'),
            'icon' => 'fa fa-list',
        ),
        array(
            'label' => Yii::t('backendmenu', 'Manage Comments'),
            'active' => $cid == 'comment' && $aid == 'index',
            'url' => array('/backend/comment'),
            'icon' => 'fa fa-comments-o',
        ),
    )
));

<?php
$cid = $this->controller->id;
$aid = $this->controller->action->id;

$this->widget('booster.widgets.TbMenu', array(
    'type' => 'pills',
    'stacked' => true,
    'items' => array(
        array(
            'label' => Yii::t('backendmenu', 'Create User'),
            'icon' => 'fa fa-users',
            'url' => array('/backend/user/create')
        ),
        array(
            'label' => Yii::t('backendmenu', 'Manage Users'),
            'icon' => 'fa fa-list',
            'active' => $cid == 'user' && $aid == 'index',
            'url' => array('/backend/user')
        ),
        array(
            'label' => Yii::t('backendmenu', 'Create Role'),
            'icon' => 'fa fa-star',
            'url' => array('/backend/role/create')
        ),
        array(
            'label' => Yii::t('backendmenu', 'Manage Roles'),
            'icon' => 'fa fa-list',
            'active' => $cid == 'role' && $aid == 'index',
            'url' => array('/backend/role')
        )
    )
));
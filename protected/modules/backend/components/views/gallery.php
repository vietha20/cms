<?php
$cid = $this->controller->id;
$aid = $this->controller->action->id;

$this->widget('booster.widgets.TbMenu', array(
    'type' => 'pills',
    'stacked' => true,
    'items' => array(
        array(
            'label' => Yii::t('backendmenu', 'Create Gallery'),
            'icon' => 'fa fa-camera-retro',
            'url' => array('/backend/gallery/create')
        ),
        array(
            'label' => Yii::t('backendmenu', 'Manage Galleries'),
            'icon' => 'fa fa-list',
            'active' => $cid == 'gallery' && $aid == 'index',
            'url' => array('/backend/gallery')
        ),
        array(
            'label' => Yii::t('backendmenu', 'Create Album'),
            'icon' => 'fa fa-photo',
            'url' => array('/backend/album/create')
        ),
        array(
            'label' => Yii::t('backendmenu', 'Manage Albums'),
            'icon' => 'fa fa-list',
            'active' => $cid == 'album' && $aid == 'index',
            'url' => array('/backend/album')
        )
    )
));
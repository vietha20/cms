<?php
/* @var $this AuthitemchildController */
/* @var $model Authitemchild */

$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Authitemchildren' => array('/backend/authitemchild'),
    'Create',
);

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'active' => $this->id == 'authitemchild' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/authitemchild'
        )
    ),
    array(
        'icon' => 'fa fa-check-circle-o',
        'url' => array(
            '/backend/authitemchild/create'
        )
    ));

$this->renderPartial('_form', array('model' => $model)); ?>

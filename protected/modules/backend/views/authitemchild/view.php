<?php
/* @var $this AuthitemchildController */
/* @var $model Authitemchild */

$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Authitemchildren' => array('/backend/authitemchild'),
    $model->parent,
);

if (Yii::app()->user->hasFlash('success')) {
    $this->widget('booster.widgets.TbAlert', array(
        'fade' => true,
        'events' => array(),
        'htmlOptions' => array(),
        'alerts' => array( // configurations per alert type
            // success, info, warning, error or danger
            'success' => array('closeText' => 'YEEEEY')
        ),
    ));
}

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'active' => $this->id == 'authitem' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/authitemchild'
        )
    ),
    array(
        'icon' => 'fa fa-check-circle',
        'active' => $this->id == 'authitem' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/authitemchild/create'
        )
    ),
    array(
        'icon' => 'fa fa-pencil',
        'url' => array(
            '/backend/authitemchild/update',
            'parent' => $model->parent,
            'child' => $model->child)
    ),
    array(
        'icon' => 'fa fa-eye',
        'url' => array(
            '/backend/authitemchild/view',
            'parent' => $model->parent,
            'child' => $model->child)
    ),
    array(
        'icon' => 'fa fa-trash-o',
        'url' => 'delete',
        'itemOptions' => array(
            'class' => 'pull-right'
        ),
        'linkOptions' => array(
            'submit' => array(
                '/backend/authitemchild/delete',
                'parent' => $model->parent,
                'child' => $model->child
            ),
            'confirm' => 'Are you sure you want to delete this item?'
        )
    ));

$this->widget('booster.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        array('name' => 'parent'),
        array('name' => 'child'),
    ),
)); ?>

<?php
/* @var $this AuthitemchildController */
/* @var $model Authitemchild */

$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Authitemchildren' => array('/backend/authitemchild'),
    $model->parent => array('/backend/authitemchild/view',
        'parent' => $model->parent,
        'child' => $model->child),
    'Update',
);

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'active' => $this->id == 'authitem' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/authitemchild'
        )
    ),
    array(
        'icon' => 'fa fa-check-circle-o',
        'active' => $this->id == 'authitem' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/authitemchild/create'
        )
    ),
    array(
        'icon' => 'fa fa-pencil',
        'url' => array(
            '/backend/authitemchild/update',
            'parent' => $model->parent,
            'child' => $model->child)
    ),
    array(
        'icon' => 'fa fa-eye',
        'url' => array(
            '/backend/authitemchild/view',
            'parent' => $model->parent,
            'child' => $model->child)
    ),
    array(
        'icon' => 'fa fa-trash-o',
        'url' => 'delete',
        'itemOptions' => array(
            'class' => 'pull-right'
        ),
        'linkOptions' => array(
            'submit' => array(
                '/backend/authitemchild/delete',
                'parent' => $model->parent,
                'child' => $model->child
            ),
            'confirm' => 'Are you sure you want to delete this item?'
        )
    ));

$this->renderPartial('_form', array('model' => $model)); ?>

<?php
/* @var $this AuthitemchildController */
/* @var $model Authitemchild */

$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Authitemchildren' => array('/backend/authitemchild'),
    'Manage',
);

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'active' => $this->id == 'authitemchild' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/authitemchild'
        )
    ),
    array(
        'icon' => 'fa fa-check-circle-o',
        'url' => array(
            '/backend/authitemchild/create'
        )
    )); ?>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
        &lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php $this->widget('booster.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->search(),
    'enablePagination' => true,
    'template' => "{summary}{items}{pager}",
    'filter' => $model,
    'columns' => array(
        'parent',
        'child',
        array(
            'htmlOptions' => array('nowrap' => 'nowrap'),
            'class' => 'booster.widgets.TbButtonColumn',
            'buttons' => array(
                'view' => array('url' => 'Yii::app()->createUrl("/backend/authitemchild/view/",
                              array("parent"=>$data->parent, "child"=>$data->child))'),
                'update' => array('url' => 'Yii::app()->createUrl("/backend/authitemchild/update/",
                              array("parent"=>$data->parent, "child"=>$data->child))'),
                'delete' => array('url' => 'Yii::app()->createUrl("/backend/authitemchild/delete/",
                              array("parent"=>$data->parent, "child"=>$data->child))'),
            ),
        ),
    ),
));
?>



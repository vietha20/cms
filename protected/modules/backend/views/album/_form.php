<?php
/* @var $this AlbumController */
/* @var $model BackendAlbum */
/* @var $form CActiveForm */
?>
<div class="form">
    <?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
        'id' => 'album-form',
        'enableAjaxValidation' => false,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'type' => 'horizontal',
        'htmlOptions' => array(
            'class' => 'well',
            'enctype' => 'multipart/form-data'), // for inset effect
    )); ?>

    <?php $galleries = CHtml::listData(BackendGallery::model()->findAll(), 'id', 'name'); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldGroup(
        $model, 'name',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-4',
            ),
            'prepend' => '<i class="glyphicon glyphicon-asterisk"></i>',
        )
    ); ?>

    <?php echo $form->fileFieldGroup($model, 'icon',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-4',
            ),
        )
    ); ?>

    <?php echo $form->dropDownListGroup(
        $model, 'galleryid',
        array(
            'widgetOptions' => array(
                'data' => array_merge(array('--Select Gallery--'), $galleries),
            ),
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-4',
            ),
        )
    ); ?>

    <div class="form-actions">
        <?php $this->widget('booster.widgets.TbButton', array(
            'label' => $model->isNewRecord ? 'Create' : 'Save',
            'context' => 'success',
            'buttonType' => 'submit'
        )); ?>
    </div>

    <?php $this->endWidget();
    unset($form); ?>

</div>

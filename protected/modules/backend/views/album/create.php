<?php
/* @var $this AlbumController */
/* @var $model BackendAlbum */

$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Albums' => array('/backend/album'),
    'Create Album',
);

if (Yii::app()->user->hasFlash('error')) {
    $this->widget('booster.widgets.TbAlert', array(
        'fade' => true,
        'events' => array(),
        'htmlOptions' => array(),
        'alerts' => array( // configurations per alert type
            // success, info, warning, error or danger
            'error' => array('closeText' => 'ARGGHHHHH')
        ),
    ));
}

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'label' => 'Album List',
        'active' => $this->id == 'album' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/album'
        )
    ),
    array(
        'icon' => 'fa fa-camera-retro',
        'label' => 'Create Album',
        'url' => array(
            '/backend/album/create'
        )
    ));

echo $this->renderPartial('_form', array('model' => $model)); ?>

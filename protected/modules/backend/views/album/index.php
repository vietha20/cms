<?php
/* @var $this AlbumController */
/* @var $model BackendAlbum */

$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Albums' => array('/backend/album'),
    'Manage',
);

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'label' => 'Album List',
        'active' => $this->id == 'album' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/album'
        )
    ),
    array(
        'icon' => 'fa fa-photo',
        'label' => 'Create Album',
        'url' => array(
            '/backend/album/create'
        )
    )); ?>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
        &lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php $this->widget('booster.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->search(),
    'enablePagination' => true,
    'template' => "{summary}{items}{pager}",
    'filter' => $model,
    'columns' => array(
        'id',
        'name',
        'galleryid',
        'createtime' => array(
            'value' => 'date("F j, Y \a\t h:i a",$data->createtime)',
            'name' => 'createtime',
        ),
        array(
            'htmlOptions' => array('nowrap' => 'nowrap'),
            'class' => 'booster.widgets.TbButtonColumn',
            'buttons' => array(
                'view' => array('url' => 'Yii::app()->createUrl("/frontend/album/view/",
                              array("id"=>$data->id))'),
                'update' => array('url' => 'Yii::app()->createUrl("/backend/album/update/",
                              array("id"=>$data->id))'),
                'delete' => array('url' => 'Yii::app()->createUrl("/backend/album/delete/",
                              array("id"=>$data->id))'),
            ),
        ),
    ),
)); ?>

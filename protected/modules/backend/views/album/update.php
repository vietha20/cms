<?php
$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Albums' => array('/backend/album'),
    $model->name => array('/frontend/album/view', 'id' => $model->id),
    'Update',
);

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'label' => 'Album List',
        'active' => $this->id == 'album' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/album'
        )
    ),
    array(
        'icon' => 'fa fa-photo',
        'label' => 'Create Album',
        'url' => array(
            '/backend/album/create'
        )
    ),
    array(
        'icon' => 'fa fa-pencil',
        'label' => 'Update Album',
        'url' => array(
            '/backend/album/update',
            'id' => $model->id
        )
    ),
    array(
        'icon' => 'fa fa-eye',
        'label' => 'View Album',
        'url' => array(
            '/frontend/album/view',
            'id' => $model->id
        )
    ),
    array(
        'icon' => 'fa fa-trash-o',
        'label' => 'Remove Album',
        'url' => '#',
        'itemOptions' => array(
            'class' => 'pull-right'
        ),
        'linkOptions' => array(
            'submit' => array(
                '/backend/album/delete',
                'id' => $model->id
            ),
            'confirm' => 'Are you sure you want to delete this album?'
        )
    ));

if (Yii::app()->user->hasFlash('error')) {
    $this->widget('booster.widgets.TbAlert', array(
        'fade' => true,
        'events' => array(),
        'htmlOptions' => array(),
        'alerts' => array( // configurations per alert type
            // success, info, warning, error or danger
            'error' => array('closeText' => 'ARGGHHHHH')
        ),
    ));
}

echo $this->renderPartial('_form', array('model' => $model)); ?>

<?php $this->beginContent('//layouts/main'); ?>
<div class="col-sm-12">
    <?php $this->widget('booster.widgets.TbTabs', array(
        'type' => 'tabs',
        'stacked' => false,
        'tabs' => array(
            array(
                'label' => 'Home',
                'icon' => 'fa fa-cogs',
                'url' => array('/backend'),
                'active' => $this->id == 'home',
            ),
            array(
                'label' => 'User Related',
                'url' => '#',
                'active' => $this->id == 'user' || $this->id == 'role',
                'items' => array(
                    array(
                        'label' => 'Users',
                        'icon' => 'fa fa-users',
                        'url' => array('/backend/user'),
                        'active' => $this->id == 'user',
                        'visible' => Yii::app()->user->checkAccess('admin'),
                    ),
                    array(
                        'label' => 'Roles',
                        'icon' => 'fa fa-star',
                        'url' => array('/backend/role'),
                        'active' => $this->id == 'role',
                        'visible' => Yii::app()->user->checkAccess('admin'),
                    ),
                ),
            ),

            array(
                'label' => 'Post Related',
                'url' => '#',
                'active' => $this->id == 'post' || $this->id == 'category' || $this->id == 'comment',
                'items' => array(
                    array(
                        'label' => 'Posts',
                        'icon' => 'fa fa-file-text-o',
                        'url' => array('/backend/post'),
                        'active' => $this->id == 'post',
                        'visible' => Yii::app()->user->checkAccess('admin') ||
                            Yii::app()->user->checkAccess('editor'),
                    ),
                    array(
                        'label' => 'Categories',
                        'icon' => 'fa fa-folder-open-o',
                        'url' => array('/backend/category'),
                        'active' => $this->id == 'category',
                        'visible' => Yii::app()->user->checkAccess('admin') ||
                            Yii::app()->user->checkAccess('editor'),
                    ),
                    array(
                        'label' => 'Comments',
                        'icon' => 'fa fa-comments',
                        'url' => array('/backend/comment'),
                        'active' => $this->id == 'comment',
                        'visible' => Yii::app()->user->checkAccess('admin') ||
                            Yii::app()->user->checkAccess('editor'),
                    ),
                )
            ),

            array(
                'label' => 'Authorization Related',
                'url' => '#',
                'active' => $this->id == 'authitem' || $this->id == 'authitemchild' || $this->id == 'authassignment',
                'items' => array(
                    array(
                        'label' => 'Authitems',
                        'icon' => 'fa fa-check-circle',
                        'url' => array('/backend/authitem'),
                        'active' => $this->id == 'authitem',
                        'visible' => Yii::app()->user->checkAccess('admin'),
                    ),
                    array(
                        'label' => 'Authitemchildren',
                        'icon' => 'fa fa-check-circle-o',
                        'url' => array('/backend/authitemchild'),
                        'active' => $this->id == 'authitemchild',
                        'visible' => Yii::app()->user->checkAccess('admin'),
                    ),
                    array(
                        'label' => 'Authassignment',
                        'icon' => 'fa fa-link',
                        'url' => array('/backend/authassignment'),
                        'active' => $this->id == 'authassignment',
                        'visible' => Yii::app()->user->checkAccess('admin'),
                    )
                ),
            ),

            array(
                'label' => 'Gallery Related',
                'url' => '#',
                'active' => $this->id == 'gallery' || $this->id == 'album',
                'items' => array(
                    array(
                        'label' => 'Galleries',
                        'icon' => 'fa fa-camera-retro',
                        'url' => array('/backend/gallery'),
                        'active' => $this->id == 'gallery',
                        'visible' => Yii::app()->user->checkAccess('admin'),
                    ),
                    array(
                        'label' => 'Albums',
                        'icon' => 'fa fa-photo',
                        'url' => array('/backend/album'),
                        'active' => $this->id == 'album',
                        'visible' => Yii::app()->user->checkAccess('admin'),
                    ),
                ),
            ),
        )
    )); ?>
    <br>
</div>
<div class="col-sm-3">
    <?php $this->widget('Backend'); ?>
</div>
<div class="col-sm-9">
    <?php $this->widget('booster.widgets.TbTabs', array(
        'type' => 'tabs',
        'stacked' => false,
        'tabs' => $this->menu
    )); ?>
    <br>
    <?php echo $content; ?>
</div>
<?php $this->endContent(); ?>

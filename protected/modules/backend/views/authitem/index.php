<?php
/* @var $this AuthitemController */
/* @var $model Authitem */

$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Authitems' => array('/backend/authitem'),
    'Manage',
);

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'active' => $this->id == 'authitem' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/authitem'
        )
    ),
    array(
        'icon' => 'fa fa-check-circle',
        'url' => array(
            '/backend/authitem/create'
        )
    )); ?>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
        &lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php $this->widget('booster.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->search(),
    'enablePagination' => true,
    'template' => "{summary}{items}{pager}",
    'filter' => $model,
    'columns' => array(
        'name',
        'type',
        'description',
        'bizrule',
        'data',
        array(
            'htmlOptions' => array('nowrap' => 'nowrap'),
            'class' => 'booster.widgets.TbButtonColumn',
            'buttons' => array(
                'view' => array('url' => 'Yii::app()->createUrl("/backend/authitem/view/",
                              array("id"=>$data->name))'),
                'update' => array('url' => 'Yii::app()->createUrl("/backend/authitem/update/",
                              array("id"=>$data->name))'),
                'delete' => array('url' => 'Yii::app()->createUrl("/backend/authitem/delete/",
                              array("id"=>$data->name))'),
            ),
        ),
    ),
)); ?>

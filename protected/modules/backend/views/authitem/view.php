<?php
/* @var $this AuthitemController */
/* @var $model Authitem */

$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Authitems' => array('/backend/authitem'),
    $model->name,
);

if (Yii::app()->user->hasFlash('success')) {
    $this->widget('booster.widgets.TbAlert', array(
        'fade' => true,
        'events' => array(),
        'htmlOptions' => array(),
        'alerts' => array( // configurations per alert type
            // success, info, warning, error or danger
            'success' => array('closeText' => 'YEEEEY')
        ),
    ));
}

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'active' => $this->id == 'authitem' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/authitem'
        )
    ),
    array(
        'icon' => 'fa fa-check-circle',
        'active' => $this->id == 'authitem' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/authitem/create'
        )
    ),
    array(
        'icon' => 'fa fa-pencil',
        'url' => array(
            '/backend/authitem/update',
            'id' => $model->name
        )
    ),
    array(
        'icon' => 'fa fa-eye',
        'url' => array(
            '/backend/authitem/view',
            'id' => $model->name
        )
    ),
    array(
        'icon' => 'fa fa-trash-o',
        'url' => '#',
        'itemOptions' => array(
            'class' => 'pull-right'
        ),
        'linkOptions' => array(
            'submit' => array(
                '/backend/authitem/delete',
                'id' => $model->name
            ),
            'confirm' => 'Are you sure you want to delete this item?'
        )
    ));

$this->widget('booster.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        array('name' => 'name'),
        array('name' => 'type'),
        array('name' => 'description'),
        array('name' => 'bizrule'),
        array('name' => 'data'),
    ),
)); ?>

<?php
/* @var $this AuthitemController */
/* @var $model Authitem */

$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Authitems' => array('/backend/authitem'),
    'Create',
);

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'active' => $this->id == 'authitem' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/authitem'
        )
    ),
    array(
        'icon' => 'fa fa-check-circle',
        'url' => array(
            '/backend/authitem/create'
        )
    ));

$this->renderPartial('_form', array('model' => $model)); ?>

<?php
/* @var $this AuthitemController */
/* @var $model Authitem */

$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Authitems' => array('/backend/authitem'),
    $model->name => array('/backend/authitem/view',
        'id' => $model->name),
    'Update',
);

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'active' => $this->id == 'authitem' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/authitem'
        )
    ),
    array(
        'icon' => 'fa fa-check-circle',
        'active' => $this->id == 'authitem' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/authitem/create'
        )
    ),
    array(
        'icon' => 'fa fa-pencil',
        'url' => array(
            '/backend/authitem/update',
            'id' => $model->name
        )
    ),
    array(
        'icon' => 'fa fa-eye',
        'url' => array(
            '/backend/authitem/view',
            'id' => $model->name
        )
    ),
    array(
        'icon' => 'fa fa-trash-o',
        'url' => '#',
        'itemOptions' => array(
            'class' => 'pull-right'
        ),
        'linkOptions' => array(
            'submit' => array(
                '/backend/authitem/delete',
                'id' => $model->name
            ),
            'confirm' => 'Are you sure you want to delete this item?'
        )
    ));

$this->renderPartial('_form', array('model' => $model)); ?>

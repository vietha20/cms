<?php
/* @var $this AuthitemController */
/* @var $model Authitem */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
        'id' => 'authitem-form',
        'enableAjaxValidation' => false,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'type' => 'horizontal',
        'htmlOptions' => array('class' => 'well'), // for inset effect
    )); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldGroup(
        $model, 'name',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-4',
            ),
            'prepend' => '<i class="glyphicon glyphicon-asterisk"></i>',
        )
    ); ?>

    <?php echo $form->textFieldGroup(
        $model, 'type',
        array(
            'widgetOptions' => array(
                'htmlOptions' => array(
                    'placeholder' => '0, 1 or 2'
                ),
            ),
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-4',
            ),
            'prepend' => '#',
        )
    ); ?>

    <?php echo $form->textAreaGroup(
        $model, 'description',
        array(
            'widgetOptions' => array(
                'htmlOptions' => array(
                    'rows' => 15
                ),
            ),
        )
    ); ?>

    <?php echo $form->textAreaGroup(
        $model, 'bizrule',
        array(
            'widgetOptions' => array(
                'htmlOptions' => array(
                    'rows' => 15,
                    'placeholder' => 'return boolean value'
                ),
            ),
        )
    ); ?>

    <?php echo $form->textFieldGroup(
        $model, 'data',
        array(
            'widgetOptions' => array(
                'htmlOptions' => array(
                    'value' => 'N;',
                    'placeholder' => 'return boolean value'
                ),
            ),
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-4',
            ),
            'prepend' => '<i class="glyphicon glyphicon-asterisk"></i>'
        )
    ); ?>

    <div class="form-actions">
        <?php $this->widget('booster.widgets.TbButton', array(
            'label' => $model->isNewRecord ? 'Create' : 'Save',
            'context' => 'success',
            'buttonType' => 'submit'
        )); ?>
    </div>

    <?php $this->endWidget();
    unset($form); ?>

</div><!-- form -->

<?php
/* @var $this PostController */
/* @var $model BackendPost */

$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Posts' => array('/backend/post'),
    'Manage',
);

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'label' => 'Post List',
        'active' => $this->id == 'post' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/post'
        )
    ),
    array(
        'icon' => 'fa fa-file-text-o',
        'label' => 'Create Post',
        'url' => array(
            '/backend/post/create'
        )
    )); ?>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
        &lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php $this->widget('booster.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->search(),
    'enablePagination' => true,
    'template' => "{summary}{items}{pager}",
    'filter' => $model,
    'columns' => array(
        'title',
        'status' => array(
            'value' => 'BackendLookup::item("PostStatus",$data->status)',
            'filter' => BackendLookup::items('PostStatus'),
            'name' => 'status',
        ),
        'createtime' => array(
            'value' => 'date("F j, Y \a\t h:i a",$data->createtime)',
            'name' => 'createtime',
        ),
        array(
            'htmlOptions' => array('nowrap' => 'nowrap'),
            'class' => 'booster.widgets.TbButtonColumn',
            'buttons' => array(
                'view' => array('url' => 'Yii::app()->createUrl("/frontend/post/view/",
                              array("id"=>$data->id))'),
                'update' => array('url' => 'Yii::app()->createUrl("/backend/post/update/",
                              array("id"=>$data->id))'),
                'delete' => array('url' => 'Yii::app()->createUrl("/backend/post/delete/",
                              array("id"=>$data->id))'),
            )
        )
    )
)); ?>

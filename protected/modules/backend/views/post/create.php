<?php
/* @var $this PostController */
/* @var $model BackendPost */

$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Posts' => array('/backend/post'),
    'Create Post',
);

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'label' => 'Post List',
        'url' => array(
            '/backend/post'
        )
    ),
    array(
        'icon' => 'fa fa-file-text-o',
        'label' => 'Create Post',
        'url' => array(
            '/backend/post/create'
        )
    ));

echo $this->renderPartial('_form', array('model' => $model)); ?>

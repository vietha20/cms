<?php
/* @var $this PostController */
/* @var $model BackendPost */

$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Posts' => array('/backend/post'),
    $model->title => array('/frontend/post/view', 'id' => $model->id),
    'Update',
);

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'label' => 'Post List',
        'active' => $this->id == 'post' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/post'
        )
    ),
    array(
        'icon' => 'fa fa-file-text-o',
        'label' => 'Create Post',
        'url' => array(
            '/backend/post/create'
        )
    ),
    array(
        'icon' => 'fa fa-pencil',
        'label' => 'Edit Post',
        'url' => array(
            '/backend/post/update',
            'id' => $model->id
        )
    ),
    array(
        'icon' => 'fa fa-eye',
        'label' => 'View Post',
        'url' => array(
            '/frontend/post/view',
            'id' => $model->id
        )
    ),
    array(
        'icon' => 'fa fa-trash-o',
        'label' => 'Remove Post',
        'url' => '#',
        'itemOptions' => array(
            'class' => 'pull-right'
        ),
        'linkOptions' => array(
            'submit' => array(
                '/backend/post/delete',
                'id' => $model->id
            ),
            'confirm' => 'Are you sure you want to delete this category?'
        )
    ));

echo $this->renderPartial('_form', array('model' => $model)); ?>

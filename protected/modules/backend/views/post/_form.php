<div class="form">
    <?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
        'id' => 'create-form',
        'type' => 'horizontal',
        'htmlOptions' => array('class' => 'well'), // for inset effect
    )); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldGroup(
        $model, 'title',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-4',
            ),
            'prepend' => '<i class="fa fa-asterisk"></i>'
        )
    ); ?>

    <?php $categories = CHtml::listData(BackendCategory::model()->findAll(), 'id', 'name'); ?>
    <?php $status = BackendLookup::items('PostStatus'); ?>

    <?php echo $form->dropDownListGroup(
        $model, 'categoryid',
        array(
            'widgetOptions' => array(
                'data' => array_merge(array('--Select Category--'), $categories),
            ),
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-4',
            ),
        )
    ); ?>

    <?php echo $form->dropDownListGroup(
        $model, 'status',
        array(
            'widgetOptions' => array(
                'data' => array_merge(array('--Select Status--'), $status),
            ),
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-4',
            ),
        )
    ); ?>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'tags', array('class' => 'control-label col-sm-3')); ?>
        <div class="col-sm-4">
            <?php $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                'model' => $model,
                'name' => 'tags',
                'attribute' => 'tags',
                'source' => "js:function(request, response) {
                function split( val ) {
                    return val.split( /,\s*/ );
                }
                function extractLast( term ) {
                     return split( term ).pop();
                }
		$.getJSON('" . $this->createUrl('suggestTags') . "', {
		     term: extractLast(request.term)
		}, response);
	}",
                'options' => array(
                    'minLength' => 2,
                    'showAnim' => 'fold',
                    'select' => "js:function(event, ui) {
                function split( val ) {
                    return val.split( /,\s*/ );
                }
		var terms = split(this.value);
		// remove the current input
		terms.pop();
		// add the selected item
		terms.push( ui.item.value );
		// add placeholder to get the comma-and-space at the end
		terms.push('');
		this.value = terms.join(', ');
		return false;
	}"),
                'htmlOptions' => array(
                    'placeholder' => 'Insert Tags',
                    'class' => 'form-control'
                ),
            )); ?>
        </div>
    </div>

    <?php /*echo $form->ckEditorGroup(
        $model,
        'content',
        array(
            'widgetOptions' => array(
                'editorOptions' => array(
                    'fullpage' => 'js:true',
                    'plugins' => 'basicstyles,toolbar,wysiwygarea,justify,link,button,font,removeformat,image,smiley,resize,div,sourcearea,stylescombo,colorbutton,table,format'
                )
            )
        )
    );*/  echo $form->redactorGroup($model,'content',
        array(
            'widgetOptions' => array(
                'editorOptions' =>array(
                    'plugins' => array('textdirection', 'fontfamily', 'fontsize', 'fullscreen'))
            ),
        )
    ); ?>

    <div class="form-actions">
        <?php $this->widget('booster.widgets.TbButton', array(
            'label' => $model->isNewRecord ? 'Create' : 'Save',
            'context' => 'success',
            'buttonType' => 'submit'
        )); ?>
    </div>
    <?php $this->endWidget();
    unset($form); ?>
</div><!-- form -->

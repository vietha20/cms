<?php
/* @var $this AuthassignmentController */
/* @var $model Authassignment */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
        'id' => 'authassignment-form',
        'enableAjaxValidation' => false,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'type' => 'horizontal',
        'htmlOptions' => array('class' => 'well'), // for inset effect
    )); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldGroup(
        $model, 'itemname',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-4',
            ),
            'prepend' => '<i class="glyphicon glyphicon-asterisk"></i>',
        )
    ); ?>

    <?php echo $form->textFieldGroup(
        $model, 'userid',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-4',
            ),
            'prepend' => '<i class="glyphicon glyphicon-user"></i>',
        )
    ); ?>

    <?php echo $form->textAreaGroup(
        $model, 'bizrule',
        array(
            'wrapperHtmlOptions' => array(
                'placeholder' => 'return boolean value',
            ),
            'widgetOptions' => array(
                'htmlOptions' => array('rows' => 15)
            )
        )
    ); ?>

    <?php echo $form->textFieldGroup(
        $model, 'data',
        array(
            'wrapperHtmlOptions' => array(
                'placeholder' => 'return boolean value',
                'class' => 'col-sm-4',
            ),
            'widgetOptions' => array(
                'htmlOptions' => array('value' => 'N;')
            ),
            'prepend' => '<i class="glyphicon glyphicon-asterisk"></i>',
        )
    ); ?>

    <div class="form-actions">
        <?php $this->widget('booster.widgets.TbButton', array(
            'label' => $model->isNewRecord ? 'Create' : 'Save',
            'context' => 'success',
            'buttonType' => 'submit'
        )); ?>
    </div>

    <?php $this->endWidget();
    unset($form); ?>

</div><!-- form -->

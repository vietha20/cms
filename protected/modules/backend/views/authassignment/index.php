<?php
/* @var $this AuthassignmentController */
/* @var $model Authassignment */

$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Authassignments' => array('/backend/authassignment'),
    'Manage',
);

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'active' => $this->id == 'authassignment' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/authassignment'
        )
    ),
    array(
        'icon' => 'fa fa-check-circle-o',
        'url' => array(
            '/backend/authassignment/create'
        )
    )); ?>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
        &lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php $this->widget('booster.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->search(),
    'enablePagination' => true,
    'template' => "{summary}{items}{pager}",
    'filter' => $model,
    'columns' => array(
        'itemname',
        'userid',
        'bizrule',
        'data',
        array(
            'htmlOptions' => array('nowrap' => 'nowrap'),
            'class' => 'booster.widgets.TbButtonColumn',
            'buttons' => array(
                'view' => array('url' => 'Yii::app()->createUrl("/backend/authassignment/view/",
                                     array("itemname"=>$data->itemname, "userid"=>$data->userid))',),
                'update' => array('url' => 'Yii::app()->createUrl("/backend/authassignment/update/",
				       array("itemname"=>$data->itemname, "userid"=>$data->userid))',),
                'delete' => array('url' => 'Yii::app()->createUrl("/backend/authassignment/delete/",
                                     array("itemname"=>$data->itemname, "userid"=>$data->userid))',),
            ),
        ),
    ),
)); ?>


<?php
/* @var $this AuthassignmentController */
/* @var $model Authassignment */

$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Authassignments' => array('/backend/authassignment'),
    $model->itemname,
);

if (Yii::app()->user->hasFlash('success')) {
    $this->widget('booster.widgets.TbAlert', array(
        'fade' => true,
        'events' => array(),
        'htmlOptions' => array(),
        'alerts' => array( // configurations per alert type
            // success, info, warning, error or danger
            'success' => array('closeText' => 'YEEEEY')
        ),
    ));
}

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'active' => $this->id == 'authassignment' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/authassignment'
        )
    ),
    array(
        'icon' => 'fa fa-check-circle-o',
        'url' => array(
            '/backend/authassignment/create'
        ),
    ),
    array(
        'icon' => 'fa fa-pencil',
        'url' => array(
            '/backend/authassignment/update',
            'itemname' => $model->itemname,
            'userid' => $model->userid)
    ),
    array(
        'icon' => 'fa fa-eye',
        'url' => array(
            '/backend/authassignment/view',
            'itemname' => $model->itemname,
            'userid' => $model->userid)
    ),
    array(
        'icon' => 'fa fa-trash-o',
        'url' => 'delete',
        'itemOptions' => array(
            'class' => 'pull-right'
        ),
        'linkOptions' => array(
            'submit' => array(
                '/backend/authassignment/delete',
                'itemname' => $model->itemname,
                'userid' => $model->userid
            ),
            'confirm' => 'Are you sure you want to delete this item?'
        )
    ));

$this->widget('booster.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        array('name' => 'itemname'),
        array('name' => 'userid'),
        array('name' => 'bizrule'),
        array('name' => 'data'),
    ),
)); ?>

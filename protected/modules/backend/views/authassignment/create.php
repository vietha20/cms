<?php
/* @var $this AuthassignmentController */
/* @var $model Authassignment */

$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Authassignments' => array('/backend/authassignment'),
    'Create',
);

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'active' => $this->id == 'authassignment' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/authassignment'
        )
    ),
    array(
        'icon' => 'fa fa-check-circle-o',
        'url' => array(
            '/backend/authassignment/create'
        )
    ));

$this->renderPartial('_form', array('model' => $model)); ?>

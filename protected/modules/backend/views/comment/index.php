<?php
/* @var $this CommentController */
/* @var $model BackendComment */

$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Comments' => array('/backend/comment'),
    'Manage',
);

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'label' => 'Comment List',
        'active' => $this->id == 'comment' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/comment'
        )
    )); ?>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
        &lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php $this->widget('booster.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->search(),
    'enablePagination' => true,
    'template' => "{summary}{items}{pager}",
    'filter' => $model,
    'columns' => array(
        'id',
        'status' => array(
            'value' => 'BackendLookup::item("CommentStatus",$data->status)',
            'filter' => BackendLookup::items('CommentStatus'),
            'name' => 'status',
        ),
        'author',
        'email',
        'url',
        array(
            'htmlOptions' => array('nowrap' => 'nowrap'),
            'class' => 'booster.widgets.TbButtonColumn',
            'buttons' => array(
                'view' => array('url' => 'Yii::app()->createUrl($data->getUrl())'),
                'update' => array('url' => 'Yii::app()->createUrl("/backend/comment/update/",
                              array("id"=>$data->id))'),
                'delete' => array('url' => 'Yii::app()->createUrl("/backend/comment/delete/",
                              array("id"=>$data->id))'),
            ),
        ),
    ),
)); ?>

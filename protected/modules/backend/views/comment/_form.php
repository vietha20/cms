<div class="form">

    <h3> Comments</h3>

    <?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
        'id' => 'comment-form',
        'type' => 'horizontal',
        'enableAjaxValidation' => true,
        'htmlOptions' => array('class' => 'well'), // for inset effect
    )); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php if (Yii::app()->user->checkAccess('guest')): ?>

        <?php echo $form->textFieldGroup(
            $model, 'author',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-4',
                ),
                'prepend' => '<i class="glyphicon glyphicon-user"></i>',
            )
        ); ?>

        <?php echo $form->textFieldGroup(
            $model, 'email',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-4',
                ),
                'prepend' => '@',
            )
        ); ?>

    <?php else: ?>

        <?php echo $form->textFieldGroup(
            $model, 'author',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-4',
                ),
                'prepend' => '<i class="glyphicon glyphicon-user"></i>',
            )
        ); ?>

        <?php echo $form->textFieldGroup(
            $model, 'email',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-4',
                ),
                'prepend' => '@',
            )
        ); ?>

    <?php endif; ?>
    <?php echo $form->textFieldGroup(
        $model, 'url',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-4',
            ),
            'prepend' => '<i class="glyphicon glyphicon-share"></i>',
        )
    ); ?>

    <?php echo $form->textAreaGroup(
        $model, 'content',
        array('class' => 'span6', 'rows' => 15)); ?>

    <div class="form-actions">
        <?php $this->widget('booster.widgets.TbButton', array(
            'label' => $model->isNewRecord ? 'Post' : 'Save',
            'context' => 'success',
            'buttonType' => 'submit'
        )); ?>
    </div>
    <?php $this->endWidget();
    unset($form); ?>
</div><!-- form -->

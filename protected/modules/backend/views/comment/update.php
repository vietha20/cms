<?php
$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Comments' => array('/backend/comment'),
    $model->email => array($model->getUrl()),
    'Update',
);

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'label' => 'Comment List',
        'active' => $this->id == 'comment' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/comment'
        )
    ),
    array(
        'icon' => 'fa fa-pencil',
        'label' => 'Edit Comment',
        'url' => array(
            '/backend/comment/update',
            'id' => $model->id
        )
    ),
    array(
        'icon' => 'fa fa-eye',
        'label' => 'View Comment',
        'visible' => $model->status == 2,
        'url' => array(
            $model->getUrl()
        )
    ),
    array(
        'icon' => 'fa fa-check',
        'label' => 'Approve',
        'itemOptions' => array(
            'class' => 'pull-right'
        ),
        'visible' => $model->status == 1,
        'url' => array(
            '/backend/comment/changestatus',
            'id' => $model->id,
            //approve
            'status' => 2
        )
    ),
    array(
        'icon' => 'fa fa-ban',
        'label' => 'Unapprove',
        'itemOptions' => array(
            'class' => 'pull-right'
        ),
        'visible' => $model->status == 2,
        'url' => array(
            '/backend/comment/changestatus',
            'id' => $model->id,
            //unapprove
            'status' => 1
        )
    ),
    array(
        'icon' => 'fa fa-trash-o',
        'label' => 'Remove Comment',
        'url' => '#',
        'itemOptions' => array(
            'class' => 'pull-right'
        ),
        'linkOptions' => array(
            'submit' => array(
                '/backend/comment/delete',
                'id' => $model->id
            ),
            'confirm' => 'Are you sure you want to delete this comment?'
        )
    ));

echo $this->renderPartial('_form', array('model' => $model)); ?>

<?php
/* @var $this RoleController */
/* @var $model Role */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
        'id' => 'role-form',
        'enableAjaxValidation' => false,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'type' => 'horizontal',
        'htmlOptions' => array('class' => 'well'), // for inset effect
    )); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldGroup(
        $model, 'name',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-4',
            ),
            'prepend' => '<i class="glyphicon glyphicon-star"></i>',
        )
    ); ?>

    <div class="form-actions">
        <?php $this->widget('booster.widgets.TbButton', array(
            'label' => $model->isNewRecord ? 'Create' : 'Save',
            'context' => 'success',
            'buttonType' => 'submit'
        )); ?>
    </div>

    <?php $this->endWidget();
    unset($form); ?>

</div><!-- form -->

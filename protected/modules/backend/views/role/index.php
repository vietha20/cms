<?php
/* @var $this RoleController */
/* @var $model Role */

$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Roles' => array('/backend/role'),
    'Manage',
);

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'label' => 'Role List',
        'active' => $this->id == 'role' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/role'
        )
    ),
    array(
        'icon' => 'fa fa-star',
        'label' => 'Create Role',
        'url' => array(
            '/backend/role/create'
        )
    )); ?>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
        &lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php $this->widget('booster.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->search(),
    'template' => "{summary}{items}{pager}",
    'filter' => $model,
    'columns' => array(
        'id',
        'name',
        array(
            'htmlOptions' => array('nowrap' => 'nowrap'),
            'class' => 'booster.widgets.TbButtonColumn',
            'buttons' => array(
                'view' => array('url' => 'Yii::app()->createUrl("/backend/role/view/",
                              array("id"=>$data->id))'),
                'update' => array('url' => 'Yii::app()->createUrl("/backend/role/update/",
                              array("id"=>$data->id))'),
                'delete' => array('url' => 'Yii::app()->createUrl("/backend/role/delete/",
                              array("id"=>$data->id))'),
            ),
        ),
    ),
)); ?>

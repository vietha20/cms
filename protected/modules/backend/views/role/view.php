<?php
/* @var $this RoleController */
/* @var $model Role */

$this->breadcrumbs = array(
    'Admin' => array('/backend'),
    'Roles' => array('/backend/role'),
    $model->name,
);

if (Yii::app()->user->hasFlash('success') ||
    Yii::app()->user->hasFlash('error')
) {
    $this->widget('booster.widgets.TbAlert', array(
        'fade' => true,
        'events' => array(),
        'htmlOptions' => array(),
        'alerts' => array( // configurations per alert type
            // success, info, warning, error or danger
            'success' => array('closeText' => 'YEEEEY'),
            'error' => array('closeText' => false)
        ),
    ));
}

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'label' => 'Role List',
        'active' => $this->id == 'role' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/role'
        )
    ),
    array(
        'icon' => 'fa fa-pencil',
        'label' => 'Edit Role',
        'url' => array(
            '/backend/role/update',
            'id' => $model->id
        )
    ),
    array(
        'icon' => 'fa fa-eye',
        'label' => 'View Role',
        'url' => array(
            '/backend/role/view',
            'id' => $model->id
        )
    ),
    array(
        'icon' => 'fa fa-trash-o',
        'label' => 'Remove Role',
        'url' => '#',
        'itemOptions' => array(
            'class' => 'pull-right'
        ),
        'linkOptions' => array(
            'submit' => array(
                '/backend/role/delete',
                'id' => $model->id
            ),
            'confirm' => 'Are you sure you want to delete this role?'
        )
    ));

$this->widget('booster.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        array('name' => 'id'),
        array('name' => 'name'),
    ),
));

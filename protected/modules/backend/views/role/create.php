<?php
/* @var $this RoleController */
/* @var $model Role */

$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Roles' => array('/backend/role'),
    'Create',
);

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'label' => 'Role List',
        'active' => $this->id == 'role' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/role'
        )
    ),
    array(
        'icon' => 'fa fa-star',
        'label' => 'Create Role',
        'url' => array(
            '/backend/role/create'
        )
    ));

$this->renderPartial('_form', array('model' => $model)); ?>

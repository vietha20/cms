<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
        'id' => 'user-form',
        'enableAjaxValidation' => false,
        'type' => 'horizontal',
        'htmlOptions' => array('class' => 'well'), // for inset effect
    )); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldGroup(
        $model, 'username',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-4',
            ),
            'prepend' => '<i class="glyphicon glyphicon-user"></i>',
        )
    ); ?>

    <?php echo $form->passwordFieldGroup(
        $model, 'password',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-4',
            ),
            'prepend' => '<i class="glyphicon glyphicon-lock"></i>',
        )
    ); ?>

    <?php echo $form->textFieldGroup(
        $model, 'email',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-4',
            ),
            'prepend' => '@',)
    ); ?>

    <?php echo $form->textFieldGroup(
        $model, 'name',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-4',
            ),
            'prepend' => '<i class="glyphicon glyphicon-asterisk"></i>',)
    ); ?>

    <?php $roles = CHtml::listData(BackendRole::model()->findAll(), 'id', 'name'); ?>
    <?php echo $form->dropDownListGroup($model, 'roleid',
        array(
            'widgetOptions' => array(
                'data' => array_merge(array('--Select Role--'), $roles),
            ),
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-4',
            ),
        )
    ); ?>

    <div class="form-actions">
        <?php $this->widget('booster.widgets.TbButton', array(
            'label' => $model->isNewRecord ? 'Create' : 'Save',
            'context' => 'success',
            'buttonType' => 'submit'
        )); ?>
    </div>

    <?php $this->endWidget();
    unset($form); ?>

</div><!-- form -->

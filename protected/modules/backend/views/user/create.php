<?php
/* @var $this UserController */
/* @var $model BackendUser */

$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Users' => array('/backend/user'),
    'Create User',
);

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'label' => 'User List',
        'url' => array(
            '/backend/user'
        )
    ),
    array(
        'icon' => 'fa fa-users',
        'label' => 'Create User',
        'url' => array(
            '/backend/user/create'
        )
    ));

if (Yii::app()->user->hasFlash('error')) {

    $this->widget('booster.widgets.TbAlert', array(
        'fade' => true,
        'events' => array(),
        'htmlOptions' => array(),
        'alerts' => array( // configurations per alert type
            // success, info, warning, error or danger
            'error' => array('closeText' => false)
        ),
    ));
} else {
    $this->renderPartial('_form', array('model' => $model));
} ?>

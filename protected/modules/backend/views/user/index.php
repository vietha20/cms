<?php
/* @var $this UserController */
/* @var $model BackendUser */

$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Users' => array('/backend/user'),
    'Manage',
);

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'label' => 'User List',
        'active' => $this->id == 'user' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/user'
        )
    ),
    array(
        'icon' => 'fa fa-users',
        'label' => 'Create User',
        'url' => array(
            '/backend/user/create'
        )
    )); ?>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
        &lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php $this->widget('booster.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->search(),
    'enablePagination' => true,
    'template' => "{summary}{items}{pager}",
    'filter' => $model,
    'columns' => array(
        'id',
        'username',
        'email',
        array(
            'name' => 'registered',
            'value' => 'date("d M, Y - H:i:s", $data->registered)'
        ),
        array(
            'name' => 'roleid',
            'value' => '$data->role->name',
            'filter' => CHtml::listData(BackendRole::model()->findAll(), 'id', 'name')
        ),
        array(
            'htmlOptions' => array('nowrap' => 'nowrap'),
            'class' => 'booster.widgets.TbButtonColumn',
            'buttons' => array(
                'view' => array('url' => 'Yii::app()->createUrl("/frontend/profile/view/",
                              array("id"=>$data->id))'),
                'update' => array('url' => 'Yii::app()->createUrl("/backend/user/update/",
                              array("id"=>$data->id))'),
                'delete' => array('url' => 'Yii::app()->createUrl("/backend/user/delete/",
                              array("id"=>$data->id))'),
            ),
        ),
    ),
)); ?>

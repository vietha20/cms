<?php
/* @var $this UserController */
/* @var $model BackendUser */

$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Users' => array('/backend/user'),
    $model->name => array('/frontend/profile/view', 'id' => $model->id),
    'Update',
);

if (Yii::app()->user->hasFlash('success') ||
    Yii::app()->user->hasFlash('error')
) {
    $this->widget('booster.widgets.TbAlert', array(
        'fade' => true,
        'events' => array(),
        'htmlOptions' => array(),
        'alerts' => array( // configurations per alert type
            // success, info, warning, error or danger
            'success' => array('closeText' => 'YEEEEY'),
            'error' => array('closeText' => false)
        ),
    ));
}

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'label' => 'User List',
        'url' => array(
            '/backend/user'
        )
    ),
    array(
        'icon' => 'fa fa-users',
        'label' => 'Create User',
        'url' => array(
            '/backend/user/create'
        )
    ),
    array(
        'icon' => 'fa fa-pencil',
        'label' => 'Edit User',
        'url' => array(
            '/backend/user/update',
            'id' => $model->id
        )
    ),
    array(
        'icon' => 'fa fa-eye',
        'label' => 'View User',
        'url' => array(
            '/frontend/profile/view',
            'id' => $model->id
        )
    ),
    array(
        'icon' => 'fa fa-trash-o',
        'label' => 'Remove User',
        'url' => '#',
        'itemOptions' => array(
            'class' => 'pull-right'
        ),
        'linkOptions' => array(
            'submit' => array(
                '/backend/user/delete',
                'id' => $model->id
            ),
            'confirm' => 'Are you sure you want to delete this user?'
        )
    ));

$this->renderPartial('_form', array('model' => $model)); ?>

<?php
/* @var $this GalleryController */
/* @var $model BackendGallery */

$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Galleries' => array('/backend/gallery'),
    'Create Gallery',
);

if (Yii::app()->user->hasFlash('error')) {
    $this->widget('booster.widgets.TbAlert', array(
        'fade' => true,
        'events' => array(),
        'htmlOptions' => array(),
        'alerts' => array( // configurations per alert type
            // success, info, warning, error or danger
            'error' => array('closeText' => 'ARGGHHHHH')
        ),
    ));
}

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'label' => 'Gallery List',
        'active' => $this->id == 'gallery' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/gallery'
        )
    ),
    array(
        'icon' => 'fa fa-camera-retro',
        'label' => 'Create Gallery',
        'url' => array(
            '/backend/gallery/create'
        )
    ));

echo $this->renderPartial('_form', array('model' => $model)); ?>

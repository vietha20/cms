<?php
/* @var $this GalleryController */
/* @var $model BackendGallery */

$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Galleries' => array('/backend/gallery'),
    'Manage',
);

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'label' => 'Gallery List',
        'active' => $this->id == 'gallery' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/gallery'
        )
    ),
    array(
        'icon' => 'fa fa-camera-retro',
        'label' => 'Create Gallery',
        'url' => array(
            '/backend/gallery/create'
        )
    )); ?>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
        &lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php $this->widget('booster.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->search(),
    'enablePagination' => true,
    'template' => "{summary}{items}{pager}",
    'filter' => $model,
    'columns' => array(
        'id',
        'name',
        'createtime' => array(
            'value' => 'date("F j, Y \a\t h:i a",$data->createtime)',
            'name' => 'createtime',
        ),
        array(
            'htmlOptions' => array('nowrap' => 'nowrap'),
            'class' => 'booster.widgets.TbButtonColumn',
            'buttons' => array(
                'view' => array('url' => 'Yii::app()->createUrl("/frontend/gallery/view/",
                              array("id"=>$data->id))'),
                'update' => array('url' => 'Yii::app()->createUrl("/backend/gallery/update/",
                              array("id"=>$data->id))'),
                'delete' => array('url' => 'Yii::app()->createUrl("/backend/gallery/delete/",
                              array("id"=>$data->id))'),
            ),
        ),
    ),
)); ?>

<?php
$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Galleries' => array('/backend/gallery'),
    $model->name => array('/frontend/gallery/view', 'id' => $model->id),
    'Update',
);

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'label' => 'Gallery List',
        'active' => $this->id == 'category' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/gallery'
        )
    ),
    array(
        'icon' => 'fa fa-camera-retro',
        'label' => 'Create Gallery',
        'url' => array(
            '/backend/gallery/create'
        )
    ),
    array(
        'icon' => 'fa fa-pencil',
        'label' => 'Update Gallery',
        'url' => array(
            '/backend/gallery/update',
            'id' => $model->id
        )
    ),
    array(
        'icon' => 'fa fa-eye',
        'label' => 'View Gallery',
        'url' => array(
            '/frontend/gallery/view',
            'id' => $model->id
        )
    ),
    array(
        'icon' => 'fa fa-trash-o',
        'label' => 'Remove Gallery',
        'url' => '#',
        'itemOptions' => array(
            'class' => 'pull-right'
        ),
        'linkOptions' => array(
            'submit' => array(
                '/backend/gallery/delete',
                'id' => $model->id
            ),
            'confirm' => 'Are you sure you want to delete this category?'
        )
    ));

if (Yii::app()->user->hasFlash('error')) {
    $this->widget('booster.widgets.TbAlert', array(
        'fade' => true,
        'events' => array(),
        'htmlOptions' => array(),
        'alerts' => array( // configurations per alert type
            // success, info, warning, error or danger
            'error' => array('closeText' => 'ARGGHHHHH')
        ),
    ));
}

echo $this->renderPartial('_form', array('model' => $model)); ?>

<?php
/* @var $this CategoryController */
/* @var $model BackendCategory */

$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Categories' => array('/backend/category'),
    'Manage',
);

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'label' => 'Category List',
        'active' => $this->id == 'category' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/category'
        )
    ),
    array(
        'icon' => 'fa fa-folder-open-o',
        'label' => 'Create Category',
        'url' => array(
            '/backend/category/create'
        )
    )); ?>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
        &lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php $this->widget('booster.widgets.TbGridView', array(
    'type' => 'striped bordered condensed',
    'dataProvider' => $model->search(),
    'enablePagination' => true,
    'template' => "{summary}{items}{pager}",
    'filter' => $model,
    'columns' => array(
        'id',
        'name',
        array(
            'htmlOptions' => array('nowrap' => 'nowrap'),
            'class' => 'booster.widgets.TbButtonColumn',
            'buttons' => array(
                'view' => array('url' => 'Yii::app()->createUrl("/frontend/category/view/",
                              array("id"=>$data->id))'),
                'update' => array('url' => 'Yii::app()->createUrl("/backend/category/update/",
                              array("id"=>$data->id))'),
                'delete' => array('url' => 'Yii::app()->createUrl("/backend/category/delete/",
                              array("id"=>$data->id))'),
            ),
        ),
    ),
)); ?>

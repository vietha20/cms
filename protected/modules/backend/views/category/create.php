<?php
/* @var $this CategoryController */
/* @var $model Category */

$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Categories' => array('/backend/category'),
    'Create Category',
);

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'label' => 'Category List',
        'active' => $this->id == 'category' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/category'
        )
    ),
    array(
        'icon' => 'fa fa-folder-open-o',
        'label' => 'Create Category',
        'url' => array(
            '/backend/category/create'
        )
    ));

echo $this->renderPartial('_form', array('model' => $model)); ?>

<?php
$this->breadcrumbs = array(
    'Backend' => array('/backend'),
    'Category' => array('/backend/category'),
    $model->name => array('/frontend/category/view', 'id' => $model->id),
    'Update',
);

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'label' => 'Category List',
        'active' => $this->id == 'category' &&
            $this->action->id == 'index',
        'url' => array(
            '/backend/category'
        )
    ),
    array(
        'icon' => 'fa fa-folder-open-o',
        'label' => 'Create Category',
        'url' => array(
            '/backend/category/create'
        )
    ),
    array(
        'icon' => 'fa fa-pencil',
        'label' => 'Update Category',
        'url' => array(
            '/backend/category/update',
            'id' => $model->id
        )
    ),
    array(
        'icon' => 'fa fa-eye',
        'label' => 'View Category',
        'url' => array(
            '/frontend/category/view',
            'id' => $model->id
        )
    ),
    array(
        'icon' => 'fa fa-trash-o',
        'label' => 'Remove Category',
        'url' => '#',
        'itemOptions' => array(
            'class' => 'pull-right'
        ),
        'linkOptions' => array(
            'submit' => array(
                '/backend/category/delete',
                'id' => $model->id
            ),
            'confirm' => 'Are you sure you want to delete this category?'
        )
    ));

echo $this->renderPartial('_form', array('model' => $model)); ?>

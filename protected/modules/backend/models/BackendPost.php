<?php

class BackendPost extends CActiveRecord
{
    /**
     * The followings are the available columns in table 'tbl_post':
     * @var integer $id
     * @var string $title
     * @var string $content
     * @var string $tags
     * @var integer $status
     * @var integer $createtime
     * @var integer $updatetime
     * @var integer $authorid
     */
    const STATUS_DRAFT = 1;
    const STATUS_PUBLISHED = 2;
    const STATUS_ARCHIVED = 3;

    private $_oldTags;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className
     * @return mixed
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{post}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, content, status, categoryid', 'required'),
            array('status', 'in', 'range' => array(1, 2, 3)),
            array('title', 'length', 'max' => 128),
            array('tags', 'match', 'pattern' => '/^[\w\s,]+$/',
                'message' => 'Tags can only contain word characters.'),
            array('tags', 'normalizeTags'),
            array('categoryid', 'safe'),
            array('title, status, createtime', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'author' => array(self::BELONGS_TO, 'BackendUser', 'authorid'),
            'category' => array(self::BELONGS_TO, 'BackendCategory', 'categoryid'),
            'comments' => array(self::HAS_MANY, 'BackendComment', 'postid',
                'condition' => 'comments.status=' . BackendComment::STATUS_APPROVED,
                'order' => 'comments.createtime DESC'),
            'commentCount' => array(self::STAT, 'BackendComment', 'postid',
                'condition' => 'status=' . BackendComment::STATUS_APPROVED),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('backendmodel', 'ID'),
            'title' => Yii::t('backendmodel', 'Title'),
            'content' => Yii::t('backendmodel', 'Content'),
            'tags' => Yii::t('backendmodel', 'Tags'),
            'status' => Yii::t('backendmodel', 'Status'),
            'createtime' => Yii::t('backendmodel', 'Create Time'),
            'updatetime' => Yii::t('backendmodel', 'Update Time'),
            'authorid' => Yii::t('backendmodel', 'Author'),
            'categoryid' => Yii::t('backendmodel', 'Category'),
        );
    }

    /**
     * @return string the URL that shows the detail of the post
     */
    public function getUrl()
    {
        $module = strtolower(Yii::app()->getModule('frontend')->id);
        $controller = get_class($this);
        $controller[0] = strtolower($controller[0]);
        $params = array('id' => $this->id);

        return Yii::app()->urlManager->createUrl('/' . $module . '/' . $controller . '/view', $params);
    }

    /**
     * Normalizes the user-entered tags.
     */
    public function normalizeTags()
    {
        $this->tags = Tag::array2string(array_unique(BackendTag::string2array($this->tags)));
    }

    /**
     * This is invoked when a record is populated with data from a find() call.
     */
    protected function afterFind()
    {
        parent::afterFind();
        $this->_oldTags = $this->tags;
    }

    /**
     * This is invoked before the record is saved.
     * @return boolean whether the record should be saved.
     */
    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->isNewRecord) {
                $this->createtime = $this->updatetime = time();
                $this->authorid = Yii::app()->user->id;
            } else {
                $this->updatetime = time();
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * This is invoked after the record is saved.
     */
    protected function afterSave()
    {
        parent::afterSave();
        BackendTag::model()->updateFrequency($this->_oldTags, $this->tags);
    }

    /**
     * This is invoked after the record is deleted.
     */
    protected function afterDelete()
    {
        parent::afterDelete();
        BackendComment::model()->deleteAll('postid=' . $this->id);
        BackendTag::model()->updateFrequency($this->tags, '');
    }

    /**
     * Retrieves the list of posts based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the needed posts.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('title', $this->title, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('createtime', $this->createtime, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->params['postsPerPage']
            ),
            'sort' => array(
                'defaultOrder' => 'status, updatetime DESC',
            ),
        ));
    }

    /**
     * @param integer the maximum number of comments that should be returned
     * @return array the most recently added comments
     */
    public function findRecentPosts($limit = 10)
    {
        $criteria = array(
            'condition' => 'status=' . self::STATUS_PUBLISHED,
            'order' => 'createtime DESC',
            'limit' => $limit,
        );
        return $this->findAll($criteria);
    }
}

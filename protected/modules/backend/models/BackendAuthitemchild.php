<?php

/**
 * This is the model class for table "{{authitemchild}}".
 *
 * The followings are the available columns in table '{{authitemchild}}':
 * @property string $parent
 * @property string $child
 *
 * The followings are the available model relations:
 * @property BackendAuthitem $parent0
 * @property BackendAuthitem $child0
 */
class BackendAuthitemchild extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{authitemchild}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('parent, child', 'required'),
            array('parent, child', 'length', 'max' => 64),
            // The following rule is used by search().
            array('parent, child', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'parent0' => array(self::BELONGS_TO, 'BackendAuthitem', 'parent'),
            'child0' => array(self::BELONGS_TO, 'BackendAuthitem', 'child'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'parent' => Yii::t('backendmodel', 'Parent'),
            'child' => Yii::t('backendmodel', 'Child'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('parent', $this->parent, true);
        $criteria->compare('child', $this->child, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->params['postsPerPage']
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Authitemchild the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}

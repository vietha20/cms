<?php

class BackendComment extends CActiveRecord
{
    /**
     * The followings are the available columns in table 'tbl_comment':
     * @var integer $id
     * @var string $content
     * @var integer $status
     * @var integer $createtime
     * @var string $author
     * @var string $email
     * @var string $url
     * @var integer $postid
     */
    const STATUS_PENDING = 1;
    const STATUS_APPROVED = 2;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className
     * @return CActiveRecord the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{comment}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('content, author, email', 'required'),
            array('author, email, url', 'length', 'max' => 128),
            array('email', 'email'),
            array('url', 'url'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, status, content, author, email, url', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'post' => array(self::BELONGS_TO, 'Post', 'postid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('backendmodel', 'ID'),
            'content' => Yii::t('backendmodel', 'Comment'),
            'status' => Yii::t('backendmodel', 'Status'),
            'createtime' => Yii::t('backendmodel', 'Create Time'),
            'author' => Yii::t('backendmodel', 'Author'),
            'email' => Yii::t('backendmodel', 'E-Mail'),
            'url' => Yii::t('backendmodel', 'Website'),
            'postid' => Yii::t('backendmodel', 'Post ID'),
        );
    }

    /**
     * Approves a comment.
     */
    public function approve()
    {
        $this->status = BackendComment::STATUS_APPROVED;
        $this->update(array('status'));
    }

    /**
     * Approves a comment.
     */
    public function unapprove()
    {
        $this->status = BackendComment::STATUS_PENDING;
        $this->update(array('status'));
    }

    /**
     * @param Post the post that this comment belongs to. If null, the method
     * will query for the post.
     * @return string the permalink URL for this comment
     */
    public function getUrl($post = null)
    {
        if ($post === null) {
            $post = $this->post;
        }
        return $post->url . '#c' . $this->id;
    }

    /**
     * @return string the hyperlink display for the current comment's author
     */
    public function getAuthorLink()
    {
        return CHtml::encode($this->author);
    }

    /**
     * @return integer the number of comments that are pending approval
     */
    public function getPendingCommentCount()
    {
        return $this->count('status=' . self::STATUS_PENDING);
    }

    /**
     * This is invoked before the record is saved.
     * @return boolean whether the record should be saved.
     */
    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->isNewRecord) {
                $this->createtime = time();
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('author', $this->author, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('url', $this->url);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->params['postsPerPage']
            )
        ));
    }
}

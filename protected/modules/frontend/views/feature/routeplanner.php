<?php
/* @var $this GooglemapController */

$this->breadcrumbs = array(
    'Route Planner',
); ?>

<div class="col-sm-12">
    <iframe id="routeplanner" src="<?php echo Yii::app()->request->baseUrl; ?>/google/routeplanner.php" width="100%" height="900px" frameborder="0"></iframe>
</div>
<?php
/* @var $this ErrorController */
/* @var $error array */

$this->breadcrumbs = array(
    'Error',
); ?>

<h2>Error <?php echo $code; ?></h2>

<?php if (Yii::app()->user->hasFlash('error')) {
    $this->widget('booster.widgets.TbAlert', array(
        'fade' => true,
        'closeText' => false, // false equals no close link
        'events' => array(),
        'htmlOptions' => array(),
        'alerts' => array( // configurations per alert type
            // success, info, warning, error or danger
            'error' => array('closeText' => false)
        ),
    ));
} ?>

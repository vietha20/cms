<?php
/* @var $this PostController */
/* @var $model Post */
/* @var $form CActiveForm */

$this->breadcrumbs = array(
    'Search Results'
); ?>

<div class="form">
    <?php if (!$search->hasErrors()): ?>
        <legend>
            <?php echo Yii::t('lan', 'Search Results'); ?> "<?php echo CHtml::encode($search->keyword); ?>"
        </legend>
    <?php else: ?>
        <legend>
            <?php echo CHtml::error($search, 'keyword'); ?>
        </legend>
    <?php endif; ?>

    <?php foreach ($models as $model): ?>
        <?php $this->renderPartial('_view', array('model' => $model)); ?>
    <?php endforeach; ?>

    <?php $this->widget('CLinkPager', array(
        'pages' => $pages,
        'nextPageLabel'=>'Older',
        'prevPageLabel'=>'Newer',
    )); ?>

</div>

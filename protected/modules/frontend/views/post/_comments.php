<?php foreach ($comments as $comment): ?>
    <div class="comment" id="c<?php echo $comment->id; ?>">
        <div class="author">
            <?php echo $comment->authorLink; ?> says:
        </div>
        <div class="content">
            <?php
            $this->beginWidget('CMarkdown', array('purifyOutput' => true));
            echo $comment->content;
            $this->endWidget();
            ?>
        </div>
        <div class="time">
            <?php echo 'on '. date('F j, Y \a\t h:i a', $comment->createtime); ?>
        </div>
    </div><!-- comment -->
<?php endforeach; ?>
<?php $this->widget('CLinkPager', array(
    'pages' => $pages,
    'nextPageLabel'=>'Older',
    'prevPageLabel'=>'Newer',
));

if (Yii::app()->user->hasFlash('success')) {
    $this->widget('booster.widgets.TbAlert', array(
        'fade' => true,
        'events' => array(),
        'htmlOptions' => array(),
        'alerts' => array( // configurations per alert type
            // success, info, warning, error or danger
            'success' => array('closeText' => 'YEEEEY')
        ),
    ));
} ?>

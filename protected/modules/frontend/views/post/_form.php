<div class="form">

    <h3> Comments</h3>
    <?php if (Yii::app()->controller->action->id == 'view') : ?>
        <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php endif; ?>

    <?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
        'id' => 'comment-form',
        'type' => 'horizontal',
        'enableAjaxValidation' => true,
        'htmlOptions' => array('class' => 'well'), // for inset effect
    )); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php if (Yii::app()->user->checkAccess('guest')): ?>
        <?php echo $form->textFieldGroup(
            $model, 'author',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),
                'prepend' => '<i class="glyphicon glyphicon-user"></i>'
            )
        ); ?>

        <?php echo $form->textFieldGroup(
            $model, 'email',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),
                'prepend' => '@'
            )
        ); ?>
    <?php else: ?>
        <?php echo $form->textFieldGroup(
            $model, 'author',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),
                'widgetOptions' => array(
                    'htmlOptions' => array('value' => Yii::app()->user->name)
                ),
                'prepend' => '<i class="glyphicon glyphicon-user"></i>'
            )); ?>

        <?php echo $form->textFieldGroup(
            $model, 'email',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),
                'widgetOptions' => array(
                    'htmlOptions' => array('value' => Yii::app()->user->email,)
                ),
                'prepend' => '@'
            )); ?>
    <?php endif; ?>

    <?php echo $form->textFieldGroup(
        $model, 'url',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5',
            ),
            'prepend' => '<i class="glyphicon glyphicon-share"></i>'
        )); ?>

    <?php echo $form->textAreaGroup(
        $model,
        'content',
        array(
            'widgetOptions' => array(
                'htmlOptions' => array('rows' => 5),
            )
        )
    ); ?>

    <?php if (CCaptcha::checkRequirements()): ?>
        <?php echo $form->captchaGroup($model, 'verifyCode'); ?>
    <?php endif; ?>

    <div class="form-actions">
        <?php $this->widget('booster.widgets.TbButton', array(
            'label' => $model->isNewRecord ? 'Post' : 'Save',
            'context' => 'success',
            'buttonType' => 'submit'
        )); ?>
    </div>

    <?php $this->endWidget();
    unset($form); ?>

</div><!-- form -->

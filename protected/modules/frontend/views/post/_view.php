<div class="post-wrapper">
    <div class="post" id="<?php echo $model->id; ?>">
        <div class="post-date">
            <span class="post-month"><?php echo date('M', $model->createtime); ?></span>
            <span class="post-day"><?php echo date('j', $model->createtime); ?></span>
        </div>
        <div class="post-title">
            <h3><?php echo CHtml::link(CHtml::encode($model->title), $model->url); ?></h3>
        </div>
        <div class="post-content">
            <?php $this->beginWidget('CMarkdown', array('purifyOutput' => true));
            if (strlen($model->content) > 300) {
                if (preg_match('/^.{1,300}\b/s', strip_tags($model->content), $content)) {
                    echo $content[0] . CHtml::link('<br />Read More &raquo;', $model->url);
                }
            } else {
                echo $model->content;
            }
            $this->endWidget(); ?>
        </div>
        <div class="post-editormenu">
            <i class="fa fa-tags"></i>
            <?php echo implode(', ', $model->tagLinks); ?> |
            <i class="fa fa-folder-open-o"></i>
            <?php echo CHtml::link(Category::model()->findByPK($model->categoryid)->name,
                Category::model()->findByPK($model->categoryid)->url); ?>
            <br/>
            <?php if (Yii::app()->user->checkAccess('admin')): ?>
                <i class="fa fa-pencil"></i>
                <?php echo CHtml::link('Update', array('/backend/post/update', 'id' => $model->id)); ?> |
            <?php endif; ?>
            <i class="fa fa-comments-o"></i>
            <?php echo CHtml::link("Comments ({$model->commentCount})", $model->url . '#comments'); ?> |
            <i class="fa fa-user"></i>
            <?php echo CHtml::link(CHtml::encode($model->author->username),
                array('/frontend/profile/view', 'id' => $model->authorid)); ?> |
            <i class="fa fa-calendar"></i> Last updated: <?php echo date('F j, Y \a\t h:i a', $model->updatetime); ?>
        </div>
    </div>
</div>
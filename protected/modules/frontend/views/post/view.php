<?php
/* @var $this PostController */
/* @var $model Post */
/* @var $form CActiveForm */

$this->breadcrumbs = array(
    $model->title
); ?>

<?php if (Yii::app()->user->hasFlash('success')) {
    $this->widget('booster.widgets.TbAlert', array(
        'fade' => true,
        'events' => array(),
        'htmlOptions' => array(),
        'alerts' => array( // configurations per alert type
            // success, info, warning, error or danger
            'success' => array('closeText' => false),
        ),
    ));
}

echo $this->renderPartial('_viewbig', array(
    'model' => $model,
    'next' => $next,
    'prev' => $prev,
)); ?>
<div id="comments">
    <legend>
        <?php echo Yii::t('lan', 'There is only a comment | There are {n} comments', $model->commentCount); ?>
    </legend>
</div>

<?php $this->renderPartial('_comments', array(
    'post' => $model,
    'comments' => $comments,
    'pages' => $pages,
));

if (Yii::app()->user->hasFlash('commentSubmitted')) {
    $this->widget('booster.widgets.TbAlert', array(
        'fade' => true,
        'events' => array(),
        'htmlOptions' => array(),
        'alerts' => array( // configurations per alert type
            // success, info, warning, error or danger
            'commentSubmitted' => array('closeText' => false),
        ),
    ));
} else {
    $this->renderPartial('_form', array(
        'model' => $comment,
    ));
} ?>

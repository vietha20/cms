<?php
/* @var $this PostController */
/* @var $model Post */
/* @var $form CActiveForm */

$this->breadcrumbs = array(
    Yii::app()->name
); ?>

<?php if (!empty($_GET['tag'])): ?>
    <legend>Posts tagged with <i><?php echo CHtml::encode($_GET['tag']); ?></i></legend>
<?php endif; ?>

<?php foreach ($models as $model): ?>
    <?php $this->renderPartial('_view', array(
        'model' => $model,
    )); ?>
<?php endforeach; ?>

<?php $this->widget('CLinkPager', array(
    'pages' => $pages,
    'nextPageLabel'=>'Older',
    'prevPageLabel'=>'Newer',
)) ?>

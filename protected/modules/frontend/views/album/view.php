<?php
/* @var $this AlbumController */
/* @var $model Album */
/* @var $form CActiveForm */

$this->breadcrumbs = array(
    'Gallery List' => array('/frontend/gallery'),
    $model->name
); ?>

<div class="col-sm-12">
    <h3><?php echo $model->name; ?></h3>

    <?php $this->widget('application.extensions.fancygallery.FancyGallery', array(
            'target' => '.fancybox',
            'helpersEnabled' => true,
            'config' => array(
                'transitionIn' => 'elastic',
                'transitionOut' => 'elastic',
                'speedIn' => 600,
                'speedOut' => 200,
                'overlayShow' => true,
                'autoPlay' => true,
                'playSpeed' => 4000, //default 3000
                'openEffect' => 'elastic',
                'closeEffect' => 'elastic',
                'showNavArrows' => true,
                'direction' => array(
                    'next' => 'left',
                    'prev' => 'right',
                ),
                'helpers' => array(
                    'buttons' => array(),
                    'title' => array('type' => 'float'), //float,inside,  outside, over
                ),
            )
        )
    );
    ?>
    <?php foreach ($images as $image) : ?>
        <?php //var_dump($image['image']);die; ?>
        <a class="fancybox" rel="fancybox" href="<?php echo $image['image']; ?>" title="<?php echo $image['image']; ?>">
            <img alt="<?php echo $image['image']; ?>" src="<?php echo $image['image']; ?>" class="imagepadding col-sm-3"/></a>
    <?php endforeach; ?>
    <br><br>
</div>
<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    $model->username,
    'Outbox'
);

$this->menu = array(
    array(
        'icon' => 'fa fa-inbox',
        'label' => 'Inbox',
        'url' => array('/frontend/message/inbox',
            'id' => Yii::app()->user->id,
        ),
    ),
    array(
        'icon' => 'fa fa-envelope',
        'label' => 'Outbox',
        'url' => array('/frontend/message/outbox',
            'id' => Yii::app()->user->id,
        ),
    ),
); ?>
<?php if (Yii::app()->user->hasFlash('error') ||
    Yii::app()->user->hasFlash('success')) {
    $this->widget('booster.widgets.TbAlert', array(
        'fade' => true,
        'events' => array(),
        'htmlOptions' => array(),
        'alerts' => array( // configurations per alert type
            // success, info, warning, error or danger
            'success' => array('closeText' => 'Yeyyy'),
            'error' => array('closeText' => 'ARGGGGHHHHHH')
        ),
    ));
} ?>
<?php if ($count > 0) : ?>
    <div class="col-sm-12 message-header">
        <div class="col-sm-2">To</div>
        <div class="col-sm-5">Title</div>
        <div class="col-sm-3 message-sendtime">Send time</div>
    </div>
    <?php foreach($models as $model) :?>
        <div class="col-sm-12 message-row">
            <div class="col-sm-2">
                <?php echo User::model()->findByPk(array('id' => $model->receiverid))->username; ?>
            </div>
            <div id="title-<?php echo $model->id; ?>" class="col-sm-5">
                <?php echo $model->title; ?>
            </div>
            <div class="col-sm-3 message-sendtime">
                <?php echo date('j M, y - H:i', $model->sendtime); ?>

                <?php echo CHtml::link('<button type="button" class="btn-xs btn-danger pull-right"
                    data-toggle="tooltip" data-placement="right" title="Delete message">
                    <i class="fa fa-trash-o"></i></button>', array('/frontend/message/delete', 'id' => $model->id)); ?>
            </div>
        </div>
        <div id="content-<?php echo $model->id; ?>" class="col-sm-12 message-content">
            <div class="col-sm-2 message-info-to">
                <img src="<?php echo Yii::app()->baseUrl . '/uploads/avatar/' .
                    User::model()->findByPk(array('id' => $model->receiverid))->avatar; ?>"
                     width="120px" height="120px">
            </div>
            <div class="col-sm-10 message-content-to">
                <?php echo $model->content; ?>
            </div>
        </div>
    <?php endforeach; ?>
    <?php $this->widget('CLinkPager', array(
        'pages' => $pages,
        'nextPageLabel'=>'Next',
        'prevPageLabel'=>'Previous',
    )) ?>
<?php else : ?>
    <?php echo "<legend>No Requests (" . $count . ")</legend>"; ?>
    There are no requests at the moment.
<?php endif; ?>

<script type="text/javascript">
    $(document).ready(function() {
        var id, oldId, count = 0;

        $('div[id^="content-"]').hide();
        $('div[id^="title-"]').on('click', function() {
            count += 1;
            id = $(this).prop('id').split('-')[1];
            var $activeRow = $('.message-row-active');
            if ($activeRow.length) {
                oldId = $activeRow.next('div').prop('id').split('-')[1];
            }

            if (id == oldId && count%2 == 0) {
                $activeRow.removeClass('message-row-active').next('div').slideToggle();
            } else if(id == oldId && count%2 == 1) {
                $(this).parent('.message-row').toggleClass('message-row-active');
                $('div#content-'+id).slideToggle();
            } else {
                $(this).parent('.message-row').toggleClass('message-row-active');
                $activeRow.removeClass('message-row-active').next('div').slideToggle();
                $('div#content-'+id).slideToggle();
            }
        });
    });
</script>
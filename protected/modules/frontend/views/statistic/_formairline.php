<?php
/* @var $this FeatureController */
/* @var $model Airport/Airline */
/* @var $form CActiveForm */

$this->breadcrumbs = array(
    'Statistics',
    'Airport'
);

$this->menu = array(
    array(
        'label' => 'Air Quality',
        'url' => array('/frontend/statistic/airquality'),
        'active' => $this->action->id == 'airquality',
    ),
    array(
        'label' => 'Airport',
        'url' => array('/frontend/statistic/airportstatistic'),
        'active' => $this->action->id == 'airportstatistic',
    ),
    array(
        'label' => 'Airline',
        'url' => array('/frontend/statistic/airlinestatistic'),
        'active' => $this->action->id == 'airlinestatistic',
    ),
); ?>

<br>
<b>This database of airports and airlines are imported from openflights.org</b>
The data are automatically synced and imported once a day.<br><br>

Below you can search which airports or airlines resides in the country you queried.
Start typing and there will be a suggestion of which country you want to search.
<i><br>Small notice: Some airlines do not have any countries, the owner of the data may not
have the data updated.</i>
<br> <br>

<div class="form">
    <?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
        'id' => 'airport-form',
        'type' => 'horizontal',
        'method' => 'get',
        'action' => Yii::app()->createUrl('/frontend/statistic/airlinestatistic'),  //<- your form action here
        'htmlOptions' => array('class' => 'well'), // for inset effect
    )); ?>

    <div class="col-sm-5">
        <?php $this->widget(
            'booster.widgets.TbTypeahead',
            array(
                'name' => 'country',
                'datasets' => array(
                    'source' => $countries
                ),
                'options' => array(
                    'hint' => true,
                    'highlight' => true,
                    'minLength' => 2,
                ),
                'htmlOptions' => array('placeholder' => 'Choose Country'),
            )
        ); ?>
    </div>

    <button type="submit" class="btn btn-success">Search</button>

    <?php $this->endWidget();
    unset($form); ?>
</div>
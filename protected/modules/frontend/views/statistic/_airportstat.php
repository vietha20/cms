<?php
/* @var $this StatisticController */
/* @var $model Airport */

$this->breadcrumbs = array(
    'Statistics',
    'Airport'
);

$this->menu = array(
    array(
        'label' => 'Air Quality',
        'url' => array('/frontend/statistic/airquality'),
        'active' => $this->action->id == 'airquality',
    ),
    array(
        'label' => 'Airport',
        'url' => array('/frontend/statistic/airportstatistic'),
        'active' => $this->action->id == 'airportstatistic',
    ),
    array(
        'label' => 'Airline',
        'url' => array('/frontend/statistic/airlinestatistic'),
        'active' => $this->action->id == 'airlinestatistic',
    ),
); ?>

<?php $this->renderPartial('_formairport', array(
    'countries' => $countries,
    'cities' => $cities,
)); ?>

<?php if($count > 0) : ?>
    <div class="col-sm-12 airport-header">
        <div class="col-sm-4">Airport Name</div>
        <div class="col-sm-3" >City</div>
        <div class="col-sm-2">Latitude</div>
        <div class="col-sm-2">Longitude</div>
    </div>
    <?php foreach($models as $model) :?>
        <div class="col-sm-12 airport-row">
            <div class="col-sm-4">
                <?php echo $model->airportname; ?>
            </div>
            <div id="title-<?php echo $model->id; ?>" class="col-sm-3">
                <?php echo $model->city; ?>
                <i class="fa fa-eye pull-right"
                   data-toggle="tooltip"
                   data-placement="top"
                   title="Click to view the airport!"></i>
            </div>
            <div class="col-sm-2">
                <?php echo $model->latitude; ?>
            </div>
            <div class="col-sm-2">
                <?php echo $model->longitude; ?>
            </div>
        </div>
        <div id="content-<?php echo $model->id; ?>" class="col-sm-12 airport-content">
            <div class="col-sm-6">
                <div id="map-canvas-<?php echo $model->id; ?>"></div>
            </div>
            <div class="airport-table-header">
                <b>Detailed information</b><br>
            </div>
            <div class="col-sm-6 airport-detailed-header">
                Airport Name: <?php echo $model->airportname; ?><br>
                City: <?php echo $model->city; ?><br>
                Country: <?php echo $model->country; ?><br>
                IATA: <?php echo $model->iata; ?><br>
                ICAO: <?php echo $model->icao; ?><br>
                Latitude: <?php echo $model->latitude; ?><br>
                Longitude: <?php echo $model->longitude; ?><br>
                Altitude: <?php echo $model->altitude; ?><br>
                Timezone: <?php echo $model->timezone; ?><br>
                DST: <?php echo $model->dst; ?><br>
            </div>
        </div>
    <?php endforeach; ?>
    <?php $this->widget('CLinkPager', array(
        'pages' => $pages,
        'nextPageLabel'=>'Next',
        'prevPageLabel'=>'Previous',
    )) ?>
<?php endif; ?>

<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA7s0xwUTeujkuN_O2CSZIlgsryuUGvZkU">
</script>
<script>
    $(document).ready(function() {

        $('div[id^="content-"]').hide();

        var count = 0;
        $('div[id^="title-"]').on('click', function() {

            count += 1;
            var id = $(this).prop('id').split('-')[1];
            var latitude = $(this).next('div').text().trim();
            var longitude = $(this).next('div').next('div').text().trim();
            var airportName = $(this).prev('div').text().trim();
            var oldId;

            var $activeRow = $('.airport-row-active');

            if ($activeRow.length) {
                oldId = $activeRow.find('div[id^="title-"]').prop('id').split('-')[1];
                $activeRow.removeClass('airport-row-active');
            }

            if ((oldId == id && count%2 == 0) ||
                (oldId == id && count%2 == 1)) {
                $activeRow.removeClass('airport-row-active');
                $('div#content-'+id).slideToggle();

            } else {
                var $oldDiv = $('div#content-'+oldId);
                $oldDiv.slideToggle();

                $(this).parent('.airport-row').toggleClass('airport-row-active');
                $('div#content-'+id).slideToggle(function initialize() {

                    var latlng = new google.maps.LatLng(latitude, longitude); //1 location
                    var settings = {
                        zoom: 14,
                        center: latlng,
                        disableDefaultUI: true,
                        zoomControl: true,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    }

                    var map = new google.maps.Map(document.getElementById("map-canvas-"+id), settings);

                    var marker=new google.maps.Marker({
                        position:latlng
                    });
                    marker.setMap(map);

                    var content = 'Airport: ' + airportName +
                        '<br> Latitude: ' + latitude +
                        '<br> Longitude: ' + longitude;

                    var infowindow = new google.maps.InfoWindow({
                        content: content
                    });
                    infowindow.open(map,marker);

                    google.maps.event.addListener(marker, 'click', function() {
                        infowindow.open(map,marker);
                    });
                });
            }
        });
    });
</script>
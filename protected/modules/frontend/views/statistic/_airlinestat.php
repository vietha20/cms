<?php
/* @var $this StatisticController */
/* @var $model Airline */

$this->breadcrumbs = array(
    'Statistics',
    'Airline'
);

$this->menu = array(
    array(
        'label' => 'Air Quality',
        'url' => array('/frontend/statistic/airquality'),
        'active' => $this->action->id == 'airquality',
    ),
    array(
        'label' => 'Airport',
        'url' => array('/frontend/statistic/airportstatistic'),
        'active' => $this->action->id == 'airportstatistic',
    ),
    array(
        'label' => 'Airline',
        'url' => array('/frontend/statistic/airlinestatistic'),
        'active' => $this->action->id == 'airlinestatistic',
    ),
); ?>

<?php if($count > 0) : ?>
<?php $this->renderPartial('_formairline', array(
    'countries' => $countries,
)); ?>

    <div class="col-sm-12 airline-header">
        <div class="col-sm-4">Airline Name</div>
        <div class="col-sm-1">ICAO</div>
        <div class="col-sm-1">IATA</div>
        <div class="col-sm-2">Callsign</div>
        <div class="col-sm-2">Country</div>
        <div class="col-sm-1">Active</div>
    </div>
<?php foreach($models as $model) :?>
    <div class="col-sm-12 airline-row">
        <div class="col-sm-4">
            <?php echo $model->airlinename; ?>
        </div>
        <div class="col-sm-1 airline-float">
            <?php echo $model->icao; ?>
        </div>
        <div class="col-sm-1 airline-float">
            <?php echo $model->iata; ?>
        </div>
        <div class="col-sm-2 airline-float">
            <?php echo $model->callsign; ?>
        </div>
        <div class="col-sm-2 airline-float">
            <?php echo $model->country; ?>
        </div>
        <div class="col-sm-1 airline-float">
            <?php echo $model->active; ?>
        </div>
    </div>
<?php endforeach; ?>
<?php $this->widget('CLinkPager', array(
    'pages' => $pages,
    'nextPageLabel'=>'Next',
    'prevPageLabel'=>'Previous',
)) ?>
<?php endif; ?>
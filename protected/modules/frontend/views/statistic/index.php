<?php
/* @var $this StatisticController */

$this->breadcrumbs = array(
    'Statistics',
);

$this->widget(
    'booster.widgets.TbTabs',
    array(
        'type' => 'tabs', // 'tabs' or 'pills'
        'tabs' => array(
            array(
                'label' => 'Air quality',
                'content' => $this->renderPartial('_airquality', array(
                        'luftdata' => $luftdata,
                        'tidpunkt' => $tidpunkt,
                        'startdate' => $startdate,
                        'enddate' => $enddate
                    ), true),
                'active' => $this->action->id == 'index',
            ),
            array(
                'label' => 'Airport Statistics',
                'content' => $this->renderPartial('_airportstat', array(
                        'countries' => $countries,
                        'airports' => $airports
                    ), true),
                'active' => $this->action->id == 'airportstatistic',
            ),
            /*array(
                'label' => 'Airline Statistics',
                'content' => $this->renderPartial('_airlinestat', array(
                        'airlines' => $airlines,
                        'airports' => $airports
                    ), true),
                'active' => $this->action->id == 'displayflightstat',
            ),*/
        ),
    )
);

$this->menu = array(
    array(
        'label' => 'Air Quality',
        'url' => array('/frontend/statistic/airquality'),
    ),
    array(
        'label' => 'Airport Statistic',
        'url' => array('/frontend/statistic/airportstatistic'),
    ),
);
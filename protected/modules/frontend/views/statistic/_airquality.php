<?php
/* @var $this AirportController */
/* @var $model Airport */

$this->breadcrumbs = array(
    'Statistics',
    'Air Quality'
);

$this->menu = array(
    array(
        'label' => 'Air Quality',
        'url' => array('/frontend/statistic/airquality'),
        'active' => $this->action->id == 'airquality',
    ),
    array(
        'label' => 'Airport',
        'url' => array('/frontend/statistic/airportstatistic'),
        'active' => $this->action->id == 'airportstatistic',
    ),
    array(
        'label' => 'Airline',
        'url' => array('/frontend/statistic/airlinestatistic'),
        'active' => $this->action->id == 'airlinestatistic',
    ),
); ?>

<br>
<b>Linköping has at various times since the late 60s conducted air quality measurements. Today measurements of
    soot, sulfur dioxide, nitrogen dioxide and volatile organic compounds (VOCs) are being conducted in the so-called
    urban background. Particulate matter, PM10, measured at street level.</b> <br><br>

In Linköping, it is above all the traffic that contribute to air quality problems. The air pollutant for which
high concentrations have been measured are particles, PM10. It is primarily in late winter and only in
occasional street environments with highly trafficked streets that high particulate levels have been measured.
Below you can choose dates to check air quality. <br> <br>

Notice: Currently we can only get data until 3rd July.
<br> <br>

<div class="form">
    <?php if (Yii::app()->user->hasFlash('error')) {
        $this->widget('booster.widgets.TbAlert', array(
            'fade' => true,
            'events' => array(),
            'htmlOptions' => array(),
            'alerts' => array( // configurations per alert type
                // success, info, warning, error or danger
                'error' => array('closeText' => 'AAARGHH!!')
            ),
        ));
    }

    $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
        'id' => 'statistic-form',
        'type' => 'horizontal',
        'action' => Yii::app()->createUrl('statistic/airquality'),  //<- your form action here
        'htmlOptions' => array(
            'class' => 'well',
        ) // for inset effect
    ));

    $this->widget(
        'booster.widgets.TbDatePicker',
        array(
            'name' => 'startdate',
            'options' => array(
                'format' => 'yyyy-mm-dd',
                'viewformat' => 'yyyy-mm-dd',
                'endDate' => 'today',
                'autoclose' => true
            ),
            'htmlOptions' => array('class'=>'col-sm-2', 'placeholder' => 'Start date: YYYY-MM-DD',),
        )
    );

    $this->widget(
        'booster.widgets.TbDatePicker',
        array(
            'name' => 'enddate',
            'options' => array(
                'format' => 'yyyy-mm-dd',
                'viewformat' => 'yyyy-mm-dd',
                'endDate' => 'today',
                'autoclose' => true
            ),
            'htmlOptions' => array('class'=>'col-sm-2 col-xs-offset-1', 'placeholder' => 'End date: YYYY-MM-DD',),
        )
    );

    $this->widget('booster.widgets.TbButton',array(
        'buttonType' => 'submit',
        'id' => 'statairquaility',
        'label' => 'Search',
        'size' => 'small',
        'context' => 'success',
        'htmlOptions' => array('class'=>'col-xs-offset-1'),
    )); ?>

    <?php $this->endWidget();
    unset($form); ?>
</div><!-- form -->


<?php if(!empty($luftdata) && !empty($tidpunkt)) {
    $this->widget(
        'booster.widgets.TbHighCharts',
        array(
            'options' => array(
                'credits' => array('enabled' => false),
                'exporting' => array('enabled' => false),
                'title' => array(
                    'text' => 'Air quality (Hamngatan, Linköping)<br> during '.$startdate . ' - ' .$enddate,
                    'x' => -20 //center
                ),
                'xAxis' => array(
                    'categories' => $tidpunkt,
                    'labels' => [
                        'rotation' => -75,
                        'style' => [
                            'fontSize' => '10px'
                        ],

                    ],
                ),
                'yAxis' => array(
                    'title' => array(
                        'text' =>  'Air quality data',
                    ),
                    'plotLines' => [
                        [
                            'value' => 0,
                            'width' => 1,
                            'color' => '#808080'
                        ]
                    ],
                ),
                'series' => array(
                    [
                        'name' => 'Quality',
                        'data' => $luftdata,
                        'type' => 'spline',
                    ]
                ),
            ),
            'htmlOptions' => array(
                'style' => 'min-width: 310px; height: 400px; margin: 0 auto'
            ),
        )
    );
}

$this->widget(
    'booster.widgets.TbHighCharts',
    array(
        'options' => array(
            'credits' => array('enabled' => false),
            'exporting' => array('enabled' => false),
            'title' => array(
                'text' => 'Monthly Average Temperature',
                'x' => -20 //center
            ),
            'subtitle' => array(
                'text' => 'Source: WorldClimate.com',
                'x' -20
            ),
            'xAxis' => array(
                'categories' => ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            ),
            'yAxis' => array(
                'title' => array(
                    'text' =>  'Temperature (°C)',
                ),
                'plotLines' => [
                    [
                        'value' => 0,
                        'width' => 1,
                        'color' => '#808080'
                    ]
                ],
            ),
            'tooltip' => array(
                'valueSuffix' => '°C'
            ),
            'series' => array(
                [
                    'name' => 'Tokyo',
                    'data' => [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
                ],
                [
                    'name' => 'New York',
                    'data' => [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5]
                ],
                [
                    'name' => 'Berlin',
                    'data' => [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0]
                ],
                [
                    'name' => 'London',
                    'data' => [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
                ]
            )
        ),
        'htmlOptions' => array(
            'style' => 'min-width: 310px; height: 400px; margin: 0 auto'
        )
    )
);



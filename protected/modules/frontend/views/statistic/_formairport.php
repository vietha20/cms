<?php
/* @var $this FeatureController */
/* @var $model Airport/Airline */
/* @var $form CActiveForm */

$this->breadcrumbs = array(
    'Statistics',
    'Airport'
);

$this->menu = array(
    array(
        'label' => 'Air Quality',
        'url' => array('/frontend/statistic/airquality'),
        'active' => $this->action->id == 'airquality',
    ),
    array(
        'label' => 'Airport',
        'url' => array('/frontend/statistic/airportstatistic'),
        'active' => $this->action->id == 'airportstatistic',
    ),
    array(
        'label' => 'Airline',
        'url' => array('/frontend/statistic/airlinestatistic'),
        'active' => $this->action->id == 'airlinestatistic',
    ),
); ?>

<br>
<b>This database of airports and airlines are imported from openflights.org</b>
The data are automatically synced and imported once a day.<br><br>

Below you can search which airports or airlines resides in the country you queried.
Start typing and there will be a suggestion of which country/city you want to search.

<br> <br>

<div class="col-sm-12">
    <div class="form col-sm-offset-10">
        <?php $this->widget(
            'booster.widgets.TbSwitch',
            array(
                'name' => 'searchSwitch',
                'options' => array(
                    'onText' => 'Country',
                    'onColor' => 'primary',
                    'offText' => 'City',
                    'offColor' => 'danger',
                    'size' => 'large',
                    'onInit' => 'js:function(){$("#airport-by-city").fadeToggle(0);}',
                    'onSwitchChange' => 'js:function(){$("#airport-by-city").fadeToggle(0);
                    $("#airport-by-country").fadeToggle(0);}',
                )
            )
        ); ?>
    </div>
    <br><br>
    <div id="airport-by-country" class="form col-sm-offset-4">

        <?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
            'id' => 'airport-form',
            'type' => 'horizontal',
            'method' => 'get',
            'action' => Yii::app()->createUrl('/frontend/statistic/airportstatistic'),  //<- your form action here
            'htmlOptions' => array('class' => 'well'), // for inset effect
        )); ?>

        <div class="col-sm-10">
            <?php $form->widget(
                'booster.widgets.TbTypeahead',
                array(
                    'name' => 'country',
                    'datasets' => array(
                        'source' => $countries
                    ),
                    'options' => array(
                        'hint' => true,
                        'highlight' => true,
                        'minLength' => 2,
                    ),
                    'htmlOptions' => array('placeholder' => 'Choose Country'),
                )
            ); ?>
        </div>

        <button type="submit" class="btn btn-success">Search</button>

        <?php $this->endWidget();
        unset($form); ?>
    </div>

    <div id="airport-by-city" class="form col-sm-offset-4">
        <?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
            'id' => 'airport-form',
            'type' => 'horizontal',
            'method' => 'get',
            'action' => Yii::app()->createUrl('/frontend/statistic/airportstatistic'),  //<- your form action here
            'htmlOptions' => array('class' => 'well'), // for inset effect
        )); ?>

        <div class="col-sm-10">
            <?php $form->widget(
                'booster.widgets.TbTypeahead',
                array(
                    'name' => 'city',
                    'datasets' => array(
                        'source' => $cities
                    ),
                    'options' => array(
                        'hint' => true,
                        'highlight' => true,
                        'minLength' => 2,
                    ),
                    'htmlOptions' => array('placeholder' => 'Choose City'),
                )
            ); ?>
        </div>

        <button type="submit" class="btn btn-success">Search</button>

        <?php $this->endWidget();
        unset($form); ?>
    </div>
</div>
<?php
/* @var $this ContactController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->breadcrumbs = array(
    'Contact',
); ?>

<h3>Contact Us</h3>

<?php if (Yii::app()->user->hasFlash('success')) {
    $this->widget('booster.widgets.TbAlert', array(
        'fade' => true,
        'events' => array(),
        'htmlOptions' => array(),
        'alerts' => array( // configurations per alert type
            // success, info, warning, error or danger
            'success' => array('closeText' => false)
        ),
    ));
} else {
    echo $this->renderPartial('_form', array('model' => $model));
} ?>

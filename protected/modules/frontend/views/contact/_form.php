<div class="form">
    <p> If you have business inquiries or other questions,
        please fill out the following form to contact us. Thank you. </p>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
        'id' => 'contact-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'type' => 'horizontal',
        'htmlOptions' => array('class' => 'well'), // for inset effect
    )); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldGroup(
        $model, 'name',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5',
            ),
            'prepend' => '<i class="glyphicon glyphicon-user"></i>',
        )
    ); ?>

    <?php echo $form->textFieldGroup(
        $model, 'email',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5',
            ),
            'prepend' => '@'
        )
    ); ?>

    <?php echo $form->textFieldGroup(
        $model, 'subject',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5',
            ),
            'prepend' => '<i class="glyphicon glyphicon-bullhorn"></i>',
        )
    ); ?>

    <?php echo $form->textAreaGroup(
        $model, 'body',
        array(
            'widgetOptions' => array(
                'htmlOptions' => array('rows' => 15),
            )
        )
    ); ?>


    <?php if (CCaptcha::checkRequirements()): ?>
        <?php echo $form->captchaGroup($model, 'verifyCode',
            array(
                'widgetOptions' => array(
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),
                ),
            )
        ); ?>
    <?php endif; ?>

    <div class="form-actions">
        <?php $this->widget('booster.widgets.TbButton',
            array(
                'label' => 'Send',
                'context' => 'success',
                'buttonType' => 'submit'
            )); ?>
    </div>

    <?php $this->endWidget();
    unset($form); ?>

</div><!-- form -->

<?php
/* @var $this CategoryController */
/* @var $model Category */
/* @var $form CActiveForm */

$this->breadcrumbs = array(
    $model->name
);

if (Yii::app()->user->hasFlash('success') ||
    Yii::app()->user->hasFlash('error')
) {
    $this->widget('booster.widgets.TbAlert', array(
        'fade' => true,
        'events' => array(),
        'htmlOptions' => array(),
        'alerts' => array( // configurations per alert type
            // success, info, warning, error or danger
            'success' => array('closeText' => 'YEEEEY'),
            'error' => array('closeText' => false)
        ),
    ));
} ?>

<div class="form">
    <legend>Posts in category <i><?php echo $model->name; ?></i></legend>
    <?php foreach ($models as $model): ?>
        <?php $this->renderPartial('../post/_view', array('model' => $model)); ?>
    <?php endforeach; ?>
    <?php $this->widget('CLinkPager', array(
        'pages' => $pages,
        'nextPageLabel'=>'Older',
        'prevPageLabel'=>'Newer',
    )); ?>
</div>

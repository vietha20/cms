<div class="form">
    <legend>Create Category</legend>
    <hr class="style-one">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'category-form',
        'enableAjaxValidation' => false,
    )); ?>
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <?php echo $form->labelEx($model, 'name'); ?>
        <?php echo $form->textFieldGroup($model, 'name', array('size' => 30, 'maxlength' => 128)); ?>
        <?php echo $form->error($model, 'name'); ?>
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'button')); ?>
    </div>
    <?php $this->endWidget(); ?>
</div><!-- form -->

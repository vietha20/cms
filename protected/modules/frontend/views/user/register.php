<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Register',
);

if (Yii::app()->user->hasFlash('error')) {
    $this->widget('booster.widgets.TbAlert', array(
        'fade' => true,
        'events' => array(),
        'htmlOptions' => array(),
        'alerts' => array( // configurations per alert type
            // success, info, warning, error or danger
            'error' => array('closeText' => false),
        ),
    ));
} ?>

<h3>Registration</h3>

<?php $this->renderPartial('/user/_form', array('model' => $model)); ?>

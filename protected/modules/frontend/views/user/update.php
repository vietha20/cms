<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Members List' => array('/frontend/memberlist'),
    $model->username,
);

if (Yii::app()->user->hasFlash('error')) {
    $this->widget('booster.widgets.TbAlert', array(
        'fade' => true,
        'events' => array(),
        'htmlOptions' => array(),
        'alerts' => array( // configurations per alert type
            // success, info, warning, error or danger
            'error' => array('closeText' => false)
        ),
    ));
}

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'label' => 'Member List',
        'url' => array('/frontend/memberlist')
    ),
    array(
        'icon' => 'fa fa-wrench',
        'label' => 'Admin Edit',
        'itemOptions' => array('class' => 'pull-right'),
        'visible' => Yii::app()->user->checkAccess('admin'),
        'url' => array('/backend/user/update',
            'id' => $model->id
        ),
    ),
    array(
        'label' => 'User Info',
        'active' => true,
        'items' => array(
            array(
                'icon' => 'fa fa-pencil',
                'label' => 'Edit Profile',
                'visible' => Yii::app()->user->checkAccess(
                        'updateSelf', array(
                            'userid' => $model->id
                        )
                    ),
                'url' => array('/frontend/profile/update',
                    'id' => Yii::app()->user->id
                ),
            ),
            array(
                'icon' => 'fa fa-eye',
                'label' => 'Profile',
                'url' => array('/frontend/profile/view',
                    'id' => Yii::app()->user->id
                ),
            ),
            array(
                'icon' => 'fa fa-file-text-o',
                'label' => 'Posts',
                'url' => array(
                    '/frontend/profile/post',
                    'id' => Yii::app()->user->id
                ),
                'active' => $this->action->id == 'post',
            ),
            array(
                'icon' => 'fa fa-users',
                'label' => 'Friends',
                'url' => array(
                    '/frontend/profile/friend',
                    'id' => Yii::app()->user->id
                ),
                'active' => $this->action->id == 'friend'
            ),
            array(
                'icon' => 'fa fa-question',
                'label' => 'Requests',
                'url' => array(
                    '/frontend/profile/findrequest',
                    'id' => Yii::app()->user->id
                ),
                'visible' => Yii::app()->user->checkAccess(
                        'updateSelf', array(
                            'userid' => $model->id
                        )
                    ),
                'active' => $this->action->id == 'findrequest'
            ),
        ),
    ),
);

echo $this->renderPartial('/user/_form', array('model' => $model));

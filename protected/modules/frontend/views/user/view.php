<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Members List' => array('/frontend/memberlist'),
    $model->username,
);

if (Yii::app()->user->hasFlash('success') ||
    Yii::app()->user->hasFlash('error')) {
    $this->widget('booster.widgets.TbAlert', array(
        'fade' => true,
        'events' => array(),
        'htmlOptions' => array(),
        'alerts' => array( // configurations per alert type
            // success, info, warning, error or danger
            'success' => array('closeText' => 'YEEEEY'),
            'error' => array('closeText' => 'ARGGHHHHH')
        ),
    ));
}

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'label' => 'Member List',
        'url' => array('/frontend/memberlist')
    ),
    array(
        'icon' => 'fa fa-wrench',
        'label' => 'Admin Edit',
        'itemOptions' => array(
            'class' => 'pull-right'
        ),
        'visible' => Yii::app()->user->checkAccess('admin'),
        'url' => array(
            '/backend/user/update',
            'id' => $model->id
        )
    ),
    array(
        'label' => 'User Info',
        'active' => true,
        'items' => array(
            array(
                'icon' => 'fa fa-pencil',
                'label' => 'Edit Profile',
                'visible' => Yii::app()->user->checkAccess(
                        'updateSelf', array(
                            'userid' => $model->id
                        )
                    ),
                'url' => array(
                    '/frontend/profile/update',
                    'id' => $model->id
                )
            ),
            array(
                'icon' => 'fa fa-eye',
                'label' => 'Profile',
                'url' => array(
                    '/frontend/profile/view',
                    'id' => $model->id
                )
            ),
            array(
                'icon' => 'fa fa-file-text-o',
                'label' => 'Posts',
                'url' => array(
                    '/frontend/profile/post',
                    'id' => $model->id
                ),
                'active' => $this->action->id == 'post',
            ),
            array(
                'icon' => 'fa fa-users',
                'label' => 'Friends',
                'url' => array(
                    '/frontend/profile/friend',
                    'id' => $model->id
                ),
                'active' => $this->action->id == 'friend'
            ),
            array(
                'icon' => 'fa fa-question',
                'label' => 'Requests',
                'url' => array(
                    '/frontend/profile/findrequest',
                    'id' => $model->id
                ),
                'visible' => Yii::app()->user->checkAccess(
                        'updateSelf', array(
                            'userid' => $model->id
                        )
                    ),
                'active' => $this->action->id == 'findrequest'
            ),
        ),
    ),
); ?>

<div class="col-sm-3">
    <img src="<?php echo Yii::app()->baseUrl . '/uploads/avatar/' . $model->avatar; ?>">
    <br>
    <?php if(!Friend::model()->isFriend(Yii::app()->user->id, $model->id)) {
        echo CHtml::link('<button type="button" class="btn btn-info pull-left"
                        data-toggle="tooltip" data-placement="bottom"
                        title="Request Friend">
                    <i class="fa fa-user"></i></button>',
            array('/frontend/friend/makefriend',
                'uid' => Yii::app()->user->id,
                'fid' => $model->id));
    }?>
    <?php if(Friend::model()->isFriend(Yii::app()->user->id, $model->id &&
             Yii::app()->user->id !== $model->id)) {
        echo CHtml::link('<button type="button" class="btn btn-info pull-right"
                        data-toggle="tooltip" data-placement="bottom"
                        title="Send PM">
                    <i class="fa fa-envelope"></i></button>',
            array('/frontend/message/sendmessage',
                'fid' => $model->id));
    }?>
</div>
<div class="col-sm-9">
    <?php $this->widget('booster.widgets.TbDetailView', array(
        'data' => $model,
        'attributes' => array(
            array('name' => 'username'),
            array('name' => 'name'),
            array('name' => 'email'),
            array('name' => 'registered',
                'value' => date("d M, Y - H:i:s", $model->registered)),
            array('name' => 'role',
                'value' => $model->role->name)
        )
    )); ?>
</div>
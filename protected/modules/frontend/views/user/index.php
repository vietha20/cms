<?php
/* @var $this UserController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Members List',
);

$this->menu = array(
    array(
        'icon' => 'fa fa-list',
        'label' => 'Member List',
        'active' => $this->id == 'memberlist' &&
            $this->action->id == 'index',
        'url' => array('/frontend/memberlist')
    ),
    array(
        'label' => 'User Info',
        'items' => array(
            array(
                'icon' => 'fa fa-pencil',
                'label' => 'Edit Profile',
                'visible' => Yii::app()->user->checkAccess(
                        'updateSelf', array(
                            'userid' => Yii::app()->user->id
                        )
                    ),
                'url' => array('/frontend/profile/update',
                    'id' => Yii::app()->user->id,
                ),
            ),
            array(
                'icon' => 'fa fa-eye',
                'label' => 'View Profile',
                'url' => array('/frontend/profile/view',
                    'id' => Yii::app()->user->id,
                ),
            ),
            array(
                'icon' => 'fa fa-file-text-o',
                'label' => 'Posts',
                'url' => array(
                    '/frontend/profile/post',
                    'id' => Yii::app()->user->id,
                ),
                'active' => $this->action->id == 'post',
            ),
            array(
                'icon' => 'fa fa-users',
                'label' => 'Friends',
                'url' => array(
                    '/frontend/profile/friend',
                    'id' => Yii::app()->user->id,
                ),
                'active' => $this->action->id == 'friend'
            ),
            array(
                'icon' => 'fa fa-question',
                'label' => 'Requests',
                'url' => array(
                    '/frontend/profile/findrequest',
                    'id' => Yii::app()->user->id
                ),
                'visible' => Yii::app()->user->checkAccess(
                        'updateSelf', array(
                            'userid' => Yii::app()->user->id,
                        )
                    ),
                'active' => $this->action->id == 'findrequest'
            ),
        ),
    ),
);

$this->widget('booster.widgets.TbGridView', array(
    'dataProvider' => $model->search(),
    'template' => "{summary}{items}{pager}",
    'type' => 'striped bordered',
    'columns' => array(
        'id',
        array(
            'name' => 'username',
            'type' => 'raw',
            'value' => 'CHtml::link($data->username,
                              Yii::app()->createUrl("/frontend/profile/view",
                              array("id"=>$data->id)))',
        ),
        'name',
        'email',
        array(
            'name' => 'registered',
            'value' => 'date("d M, Y - H:i:s", $data->registered)'
        )
    ),
));
<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">
    <?php if (Yii::app()->controller->action->id == 'register' ||
        Yii::app()->controller->action->id == 'login'
    ) :  ?>
        <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php endif; ?>

    <?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
        'id' => 'user-form',
        'enableAjaxValidation' => false,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'type' => 'horizontal',
        'htmlOptions' => array(
            'class' => 'well',
            'enctype' => 'multipart/form-data') // for inset effect
    )); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldGroup(
        $model, 'username',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5',
            ),
            'prepend' => '<i class="fa fa-user"></i>'
        )
    ); ?>

    <?php echo $form->passwordFieldGroup(
        $model, 'password',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5',
            ),
            'prepend' => '<i class="fa fa-lock"></i>',
        )
    ); ?>

    <?php if (Yii::app()->controller->action->id == 'register' ||
        Yii::app()->controller->action->id == 'update') : ?>
        <?php echo $form->textFieldGroup(
            $model, 'email',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),
                'prepend' => '@'
            )
        ); ?>

        <?php echo $form->textFieldGroup(
            $model, 'name',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),
                'prepend' => '<i class="fa fa-asterisk"></i>'
            )
        ); ?>
    <?php endif; ?>

    <?php if (Yii::app()->controller->action->id == 'register' ||
        Yii::app()->controller->action->id == 'update') :  ?>
        <?php echo $form->fileFieldGroup($model, 'avatar',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),
            )
        ); ?>
    <?php endif; ?>

    <?php if (CCaptcha::checkRequirements()): ?>
        <?php echo $form->captchaGroup($model, 'verifyCode'); ?>
    <?php endif; ?>

    <?php if (Yii::app()->controller->action->id == 'login') : ?>
        <?php echo $form->checkBoxGroup($model, 'rememberMe'); ?>
    <?php endif; ?>

    <div class="form-actions">
        <?php if (Yii::app()->controller->action->id == 'register' ||
            Yii::app()->controller->action->id == 'update'
        ) : ?>
            <?php $this->widget('booster.widgets.TbButton', array(
                'label' => $model->isNewRecord ? 'Register' : 'Save',
                'context' => 'success',
                'buttonType' => 'submit'
            )); ?>
        <?php else: ?>
            <?php $this->widget('booster.widgets.TbButton', array(
                'label' => 'Login',
                'context' => 'success',
                'buttonType' => 'submit'
            )); ?>
        <?php endif; ?>
    </div>

    <?php $this->endWidget();
    unset($form); ?>

</div><!-- form -->

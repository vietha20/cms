<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Login',
); ?>

<h3>Login</h3>

<?php $this->renderPartial('/user/_form', array('model' => $model)); ?>

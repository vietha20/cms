<?php
/* @var $this GalleryController */
/* @var $model Gallery */
/* @var $form CActiveForm */

$this->breadcrumbs = array(
    'Gallery List' => array('/frontend/gallery'),
); ?>

<div class="col-sm-12">
    <?php foreach($models as $model) : ?>
        <div class="col-sm-6 divalbum">
            <?php echo CHtml::link('<h4>'.$model->name.'</h4>', array('/frontend/gallery/view/', 'id' => $model->id)); ?>
            <img class="gallerythumbnail" src="<?php echo Yii::app()->baseUrl . '/uploads/gallery/'.$model->name.'/'.$model->icon; ?>">
            <br><br>
        </div>
    <?php endforeach; ?>
</div>

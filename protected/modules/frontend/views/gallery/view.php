<?php
/* @var $this GalleryController */
/* @var $model Gallery/Album */
/* @var $form CActiveForm */

$this->breadcrumbs = array(
    'Gallery List' => array('/frontend/gallery'),
    $model->name
); ?>

<div class="col-sm-12">
    <h3><?php echo $model->name. ' ('.count($albums).')'; ?></h3>
    <?php foreach($albums as $album) : ?>
        <div class="col-sm-6 divalbum">
            <?php echo CHtml::link('<h4>'.$album->name.'</h4>', array('/frontend/album/view/', 'id' => $album->id)); ?>
            <img class="albumthumbnail" src="<?php echo Yii::app()->baseUrl . '/uploads/gallery/'.$model->name.'/'.$album->name.'/'.$album->icon; ?>">
            <br><br>
        </div>
    <?php endforeach; ?>
</div>

<?php
/* @var $this LyricController */
/* @var $data Lyric */
?>

<div class="view">
    <b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
    <?php echo CHtml::encode($data->title); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('artist')); ?>:</b>
    <?php echo CHtml::encode($data->artist); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('content')); ?>:</b>
    <?php echo CHtml::encode($data->content); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('sendername')); ?>:</b>
    <?php echo CHtml::encode($data->sendername); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('senderinfo')); ?>:</b>
    <?php echo CHtml::encode($data->senderinfo); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('postdate')); ?>:</b>
    <?php echo CHtml::encode($data->postdate); ?>
    <br />

    <?php /*
    <b><?php echo CHtml::encode($data->getAttributeLabel('lyriccount')); ?>:</b>
    <?php echo CHtml::encode($data->lyriccount); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('file')); ?>:</b>
    <?php echo CHtml::encode($data->file); ?>
    <br />

    */ ?>

</div>
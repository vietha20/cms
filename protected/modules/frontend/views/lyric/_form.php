<?php
/* @var $this LyricController */
/* @var $model Lyric */
/* @var $form CActiveForm */
?>

<div class="form">
    <?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
        'id' => 'lyric-form',
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
        'type' => 'horizontal',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'well',
            'enctype' => 'multipart/form-data') // for inset effect
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldRow(
        $model, 'title',
        array('class' => 'span2'),
        array('prepend' => '<i class="fa fa-user"></i>')); ?>

    <?php echo $form->textFieldRow(
        $model, 'artist',
        array('class' => 'span2'),
        array('prepend' => '<i class="fa fa-user"></i>')); ?>

    <?php echo $form->markdownEditorRow(
        $model, 'content',
        array('height' => '200px')); ?>

    <?php echo $form->textFieldRow(
        $model, 'sendername',
        array('class' => 'span2'),
        array('prepend' => '<i class="fa fa-user"></i>')); ?>

    <?php echo $form->textFieldRow(
        $model, 'senderinfo',
        array('class' => 'span2'),
        array('prepend' => '<i class="fa fa-user"></i>')); ?>

    <?php echo $form->fileFieldRow($model, 'file'); ?>

    <div class="form-actions">
        <?php $this->widget('booster.widgets.TbButton', array(
            'label' => $model->isNewRecord ? 'Create' : 'Save',
            'type' => 'success',
            'buttonType' => 'submit'
        )); ?>
    </div>

    <?php $this->endWidget();
    unset($form); ?>

</div><!-- form -->

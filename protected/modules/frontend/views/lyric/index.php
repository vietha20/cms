<?php
/* @var $this LyricController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Lyrics',
);

$this->menu = array(
    array('label' => 'Create Lyric', 'url' => array('create')),
    array('label' => 'Manage Lyric', 'url' => array('admin')),
);
?>

Linköpings kommun har i olika omgångar sedan slutet av 60-talet genomfört luftkvalitetsmätningar. Idag genomförs mätningar av sot, svaveldioxid, kvävedioxid och flyktiga organiska ämnen (VOC) i såkallad urban bakgrund. Partiklar, PM10, mäts i gaturum.

I Linköping är det framförallt trafiken som bidrar till luftproblem. Den luftförorening för vilken höga halter har uppmätts är partiklar, PM10. Det är framförallt på vårvintern och endast i enstaka gatumiljöer med starkt trafikerade gator som höga partikelhalter har uppmätts.



<?php $this->widget(
    'booster.widgets.TbHighCharts',
    array(
        'options' => array(
            'title' => array(
                'text' => 'Luftkvalitet (Hamngatan, Linköping)',
                'x' => -20 //center
            ),
            'xAxis' => array(
                'labels' => [
                    'rotation' => -45,
                    'style' => [
                        'fontSize' => '13px',
                        'fontFamily' => 'Verdana, sans-serif',

                    ],

                ],
            ),
            'yAxis' => array(
                'title' => array(
                    'text' =>  'Luftdata',
                ),
                'plotLines' => [
                    [
                        'value' => 0,
                        'width' => 1,
                        'color' => '#808080'
                    ]
                ],
            ),
            'legend' => array(
                'layout' => 'vertical',
                'align' => 'right',
                'verticalAlign' => 'middle',
                'borderWidth' => 0
            ),
            'series' => array(
                [
                    'name' => 'Luftkvalitet',
                ]
            )
        ),
        'htmlOptions' => array(
            'style' => 'min-width: 310px; height: 400px; margin: 0 auto'
        )
    )
); ?>
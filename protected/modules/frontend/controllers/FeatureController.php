<?php

class FeatureController extends Controller
{
    /**
     * This is a partial rendering of the external youtube video search
     * This is how you can actually integrate google youtube api into yii
     * without extensions.
     */
    public function actionSearchVideo()
    {
        $this->render('videosearch');
    }

    /**
     * This is a partial rendering of the external google maps search API
     * This is how you can actually integrate google maps api into yii
     * without extensions. Google map API will return the routes by drive,
     * walk or bicycle.
     */
    public function actionRoutePlanner()
    {
        $this->render('routeplanner');
    }
}

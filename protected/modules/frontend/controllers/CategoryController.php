<?php

class CategoryController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow', // allow all users to perform 'view' actions
                'roles' => array('guest', 'user', 'admin'),
            ),
        );
    }

    /**
     * Views particular model.
     */
    public function actionView()
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'categoryid = :categoryid';
        $criteria->order = 'updatetime DESC';
        $criteria->params = array(':categoryid' => $_GET['id']);

        $count = Post::model()->count($criteria);
        $pages = new CPagination($count);

        // results per page
        $pages->pageSize = Yii::app()->params['postsPerPage'];
        $pages->applyLimit($criteria);
        $models = Post::model()->findAll($criteria);

        $model = $this->loadModel();

        if (isset($_GET['page']) && $pages->getPageCount() < $_GET['page']) {
            throw new CHttpException(400);
        } else {
            $this->render('view', array(
                'model' => $model,
                'models' => $models,
                'pages' => $pages));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @return mixed
     * @throws CHttpException
     */
    public function loadModel()
    {
        $model = Category::model()->findByPk($_GET['id']);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Category $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'category-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}

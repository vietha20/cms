<?php

class FriendController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow', // allow all users to perform 'view' actions
                'roles' => array('user', 'editor', 'admin'),
            ),
            array(
                'deny', // deny all guests
                'roles' => array('guest'),
            ),
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param $uid
     * @param $fid
     */
    public function actionMakeFriend($uid, $fid)
    {
        $user = $this->loadUserModel($uid);
        $model = new Friend();
        $model->userid = $uid;
        $model->friendid = $fid;
        //try/catch database errors
        try {
            if ($model->save()) {
                Yii::app()->user->setFlash('success', 'Your request has been sent to the user.');
                $message = new Message;
                $message->senderid = $uid;
                $message->receiverid = $fid;
                $message->title = 'Friend request';
                $message->content = 'Hi, I would like to make a friend request with you.
                                     You can accept or deny in friends section of your profile.';
                $message->sendtime = time();
                $message->save();
                $this->redirect(array('/frontend/profile/view', 'id' => $uid));
            }
        } catch (CDbException $e) {
            Yii::app()->user->setFlash('error', $e->getMessage());
            $this->redirect(array('/frontend/profile/view', 'id' => $uid));
        }

        $this->render('/user/view', array(
            'model' => $user,
        ));
    }

    public function actionConfirmRequest($uid, $fid, $b) {
        $model = $this->loadModel($uid,$fid);

        if ($model->accepted == 1) {
            Yii::app()->user->setFlash('error', 'The request was invalid.');
            $this->redirect(array('/frontend/profile/view', 'id' => $fid));
        } else {
            $model->accepted = 1;
            if($b == 0) {
                try {
                    if($model->delete()) {
                        Yii::app()->user->setFlash('success', 'You have canceled the request.');
                        $this->redirect(array('/frontend/profile/view', 'id' => $fid));
                    }
                } catch (CDbException $e) {
                    Yii::app()->user->setFlash('error', $e->getMessage());
                    $this->redirect(array('/frontend/profile/view', 'id' => $fid));
                }
            } else {
                try {
                    if($model->update()) {
                        Yii::app()->user->setFlash('success', 'You have accepted '.
                            User::model()->findByPk(array('id' => $uid))->username.' as your friend');
                        $this->redirect(array('/frontend/profile/view', 'id' => $fid));
                    }
                } catch (CDbException $e) {
                    Yii::app()->user->setFlash('error', $e->getMessage());
                    $this->redirect(array('/frontend/profile/view', 'id' => $fid));
                }
            }
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param $userid
     * @param $friendid
     * @return mixed
     * @throws CHttpException
     */
    public function loadModel($userid, $friendid)
    {
        $model = Friend::model()->findByPk(array(
            'userid' => $userid, 'friendid' => $friendid));
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param $id
     * @return mixed
     * @throws CHttpException
     */
    public function loadUserModel($id)
    {
        $model = User::model()->findByPk(array('userid' => $id));
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }
}

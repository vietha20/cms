<?php

class MemberlistController extends Controller
{
    public $defaultAction = 'index';
    public $layout = '//layouts/column2';

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $model = new User('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['User'])) {
            $model->attributes = $_GET['User'];
        }
        $this->render('/user/index', array(
            'model' => $model,
        ));
    }
}

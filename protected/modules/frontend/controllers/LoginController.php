<?php

class LoginController extends Controller
{
    public $defaultAction = 'login';
    public $layout = '//layouts/column2';

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'transparent' => 'true',
            ),
        );
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow', // allow guests to login
                'roles' => array('guest'),
            ),
            array(
                'deny', // deny all users
                'roles' => array('user', 'admin'),
            ),
        );
    }

    /**
     * Displays the login page
     */
    public function actionLogin()
    {
        $model = new User('login');
        // collect user input data
        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
                $this->redirect(Yii::app()->user->returnUrl);
            }
        }
        // display the login form
        $this->render('/user/login', array('model' => $model));
    }
}

<?php

class PostController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $defaultAction = 'list';

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xF5F5F5,
            ),
        );
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow', // allow all users to do the actions
                'roles' => array('admin', 'guest', 'user'),
            ),
        );
    }

    public function actionArchive()
    {
        /**
         * SELECT
        FROM_UNIXTIME(createtime, '%Y') AS year
        FROM
        tbl_post
        GROUP BY year
         */

        $criteria = new CDbCriteria();
        $criteria->select = 'FROM_UNIXTIME(createtime, "%Y") AS year';
        $criteria->group = 'year';

        $year = Post::model()->findAll($criteria);
        var_dump($year);die;

    }

    /**
     * Displays a particular model.
     */
    public function actionView($id)
    {
        if($id <= -1) {
            throw new CHttpException(400);
        } else {
            $post = $this->loadModel($id);

            $criteria = new CDbCriteria();
            $criteria->condition = 'postid=:postid AND status=' . Comment::STATUS_APPROVED;
            $criteria->order = 'id DESC';
            $criteria->params = array(':postid' => $id);

            $count = Comment::model()->count($criteria);
            $pages = new CPagination($count);

            // results per page
            $pages->pageSize = Yii::app()->params['postsPerPage'];
            $pages->applyLimit($criteria);
            $comments = Comment::model()->findAll($criteria);

            $comment = $this->newComment($post);

            $nextid = $post->getNextId();
            $previd = $post->getPreviousId();

            if (isset($_GET['page']) && $pages->getPageCount() < $_GET['page']) {
                throw new CHttpException(400);
            } else {
                $this->render('view', array(
                    'model' => $post,
                    'comments' => $comments, //comments related to a post
                    'comment' => $comment, //comment form
                    'pages' => $pages,
                    'next' => $nextid,
                    'prev' => $previd,
                ));
            }
        }
    }

    /**
     * Lists all models.
     */
    public function actionList()
    {
        $criteria = new CDbCriteria(array(
            'condition' => 'status=' . Post::STATUS_PUBLISHED,
            'order' => 'updatetime DESC',
            'with' => 'commentCount',
        ));

        $this->pageTitle = '';
        $modelsCount = Post::model()->count($criteria);

        $pages = new CPagination($modelsCount);
        $pages->pageSize = Yii::app()->params['postsPerPage'];
        $pages->applyLimit($criteria);

        $models = Post::model()->findAll($criteria);

        if (isset($_GET['page']) && $pages->getPageCount() < $_GET['page']) {
            throw new CHttpException(400);
        } else {
            $this->render('list', array(
                'models' => $models,
                'pages' => $pages,
            ));
        }
    }


    /**
     * Lists all models.
     */
    public function actionTag()
    {
        $criteria = new CDbCriteria(array(
            'condition' => 'status=' . Post::STATUS_PUBLISHED,
            'order' => 'updatetime DESC',
            'with' => 'commentCount',
        ));

        $criteria->addSearchCondition('tags', $_GET['tag']);
        $modelsCount = Post::model()->count($criteria);

        $pages = new CPagination($modelsCount);
        $pages->pageSize = Yii::app()->params['postsPerPage'];
        $pages->applyLimit($criteria);

        $models = Post::model()->findAll($criteria);

        if (isset($_GET['page']) && $pages->getPageCount() < $_GET['page']) {
            throw new CHttpException(400);
        } else {
            $this->render('tag', array(
                'models' => $models,
                'pages' => $pages,
            ));
        }
    }

    /**
     * Sitewide search.
     * Shows a particular post searched.
     */
    public function actionSearch()
    {
        $search = new SearchForm;

        if (isset($_POST['SearchForm'])) {
            $search->attributes = $_POST['SearchForm'];
            $_GET['query'] = $search->keyword;
        } else {
            $search->keyword = $_GET['query'];
        }

        if ($search->validate()) {
            $criteria = new CDbCriteria;
            $criteria->condition = 'status=' . Post::STATUS_PUBLISHED;
            $criteria->order = 'createtime DESC';

            $criteria->condition .= ' AND content LIKE :keyword';
            $criteria->params = array(':keyword' => '%' . CHtml::encode($search->keyword) . '%');

            $postCount = Post::model()->count($criteria);
            $pages = new CPagination($postCount);
            $pages->pageSize = Yii::app()->params['postsPerPage'];
            $pages->applyLimit($criteria);

            $models = Post::model()->findAll($criteria);
            $this->pageTitle = Yii::t('lan', 'Search Results') . ' "' . CHtml::encode($_GET['query']) . '"';

            $this->render('search', array(
                'models' => ($models) ? $models : array(),
                'pages' => $pages,
                'search' => $search,
            ));
        } else {
            throw new CHttpException(400);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Post the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Post::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }

    /**
     * Creates a new comment.
     * This method attempts to create a new comment based on the user input.
     * If the comment is successfully created, the browser will be redirected
     * to show the created comment.
     * @param Post the post that the new comment belongs to
     * @return Comment the comment instance
     */
    protected function newComment($post)
    {
        $comment = new Comment;

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'comment-form') {
            echo CActiveForm::validate($comment);
            Yii::app()->end();
        }

        if (isset($_POST['Comment'])) {
            $comment->attributes = $_POST['Comment'];
            if ($post->addComment($comment)) {
                if ($comment->status == Comment::STATUS_PENDING) {
                    Yii::app()->user->setFlash('commentSubmitted', 'Thank you for your comment. Your comment will be posted once it is approved.');
                }
                $this->refresh();
            }
        }
        return $comment;
    }
}

<?php

class ErrorController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    private $_e400 = 'The request had bad syntax or was inherently impossible to be satisfied.';
    private $_e403 = 'The request is for something forbidden. Authorization will not help.';
    private $_e404 = 'The server has not found anything matching the URI given.';
    private $_e500 = 'The server encountered an unexpected condition which prevented it from fulfilling the request.';

    /**
     * This is the action to handle external exceptions.
     */
    public function actionIndex()
    {
        if ($error = Yii::app()->errorHandler->error) {
            switch ($error['code']) {
                case 400:
                    $error['message'] = $this->_e400;
                    break;
                case 403:
                    $error['message'] = $this->_e403;
                    break;
                case 404:
                    $error['message'] = $this->_e404;
                    break;
                case 500:
                    $error['message'] = $this->_e500;
                    break;
            }

            Yii::app()->user->setFlash('error', $error['message']);

            if (Yii::app()->request->isAjaxRequest) {
                Yii::app()->user->setFlash('error', $error['message']);
            } else {
                $this->render('index', $error);
            }
        }
    }
}

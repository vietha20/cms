<?php

class ProfileController extends Controller
{
    public $defaultAction = 'profile';
    public $layout = '//layouts/column2';

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'transparent' => 'true',
            ),
        );
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow', // allow normal user to update and view
                'actions' => array('view', 'friend', 'post'),
                'roles' => array('user', 'editor', 'admin'),
            ),
            array(
                'allow',
                'actions' => array('findrequest', 'update'),
                'roles' => array('updateSelf'),
            ),
            array(
                'deny', // deny all users
                'roles' => array('guest'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param $id the ID of the model to be displayed
     * @throws CHttpException
     */
    public function actionView($id)
    {
        if($id <= -1) {
            throw new CHttpException(400);
        } else {
            $model = $this->loadModel($id);

            $this->render('/user/view', array(
                'model' => $model,
            ));
        }
    }

    /**
     * Displays all friends belonging to one user.
     * @param $id the ID of the model to be displayed
     * @throws CHttpException
     */
    public function actionFriend($id)
    {
        $model = $this->loadModel($id);

        $criteria = new CDbCriteria();
        $criteria->select = 't.id, t.username, t.avatar';

        /**
            SELECT u.id, u.name, u.avatar from tbl_user u
            INNER JOIN tbl_friend f ON u.id = f.friendid
            WHERE f.friendid = 2

            SELECT u.id, u.name, u.avatar from tbl_user u
            INNER JOIN tbl_friend f ON u.id = f.userid
            WHERE f.userid = 1

            To get all of my friends;
            SELECT u.name, f.friendid , IF(f.userid = $userid, f.friendid, f.userid) friendid
            FROM friends f
            inner join user u  ON ( u.userid = IF(f.userid = $userid, f.friendid, f.userid))
            WHERE ( f.userid = '$userid' or f.friendid = '$userid' )
         */

        $criteria->join = 'INNER JOIN tbl_friend f ON (t.id = IF(f.userid = '.$id.', f.friendid, f.userid))';
        $criteria->condition = '(f.userid=:uid OR f.friendid=:fid) AND f.accepted=1';
        $criteria->params = array(':uid' => $id, ':fid' => $id);

        $count = User::model()->count($criteria);
        $pages = new CPagination($count);

        // results per page
        $pages->pageSize = Yii::app()->params['postsPerPage'];
        $pages->applyLimit($criteria);
        $friends = User::model()->findAll($criteria);

        if (isset($_GET['page']) && $pages->getPageCount() < $_GET['page']) {
            throw new CHttpException(400);
        } else{
            $this->render('/user/viewfriend', array(
                'model' => $model,
                'pages' => $pages,
                'count' => $count,
                'friends' => $friends,
            ));
        }
    }

    /**
     * Displays all posts belonging to one user.
     * @param $id the ID of the model to be displayed
     * @throws CHttpException
     */
    public function actionPost($id)
    {
        $model = $this->loadModel($id);

        $criteria = new CDbCriteria();
        $criteria->condition = 'authorid=:authorid AND status=' . Post::STATUS_PUBLISHED;
        $criteria->order = 'id DESC';
        $criteria->params = array(':authorid' => $id);

        $count = Post::model()->count($criteria);
        $pages = new CPagination($count);

        // results per page
        $pages->pageSize = Yii::app()->params['postsPerPage'];
        $pages->applyLimit($criteria);
        $posts = Post::model()->findAll($criteria);

        if (isset($_GET['page']) && $pages->getPageCount() < $_GET['page'])  {
            throw new CHttpException(400);
        } else{
            $this->render('/user/viewpost', array(
                'model' => $model,
                'pages' => $pages,
                'count' => $count,
                'posts' => $posts,
            ));
        }
    }

    /**
     * Find all the friend request
     * @throws CHttpException
     */
    public function actionFindRequest($id)
    {
        if(Yii::app()->user->checkAccess('updateSelf', array('userid' => $id))) {
            $model = $this->loadModel($id);

            $criteria = new CDbCriteria();
            $criteria->select = 't.id, t.username, t.avatar';

            $criteria->join = 'INNER JOIN tbl_friend f ON (t.id = IF(f.userid = '.Yii::app()->user->id.', f.friendid, f.userid))';
            $criteria->condition = '(f.userid=:uid OR f.friendid=:fid) AND f.accepted=0';
            $criteria->params = array(':uid' => Yii::app()->user->id, ':fid' => Yii::app()->user->id);

            $count = User::model()->count($criteria);
            $pages = new CPagination($count);

            // results per page
            $pages->pageSize = Yii::app()->params['postsPerPage'];
            $pages->applyLimit($criteria);
            $requests = User::model()->findAll($criteria);

            if (isset($_GET['page']) && $pages->getPageCount() < $_GET['page'])  {
                throw new CHttpException(400);
            } else{
                $this->render('/user/viewfriendrequest', array(
                    'model' => $model,
                    'pages' => $pages,
                    'count' => $count,
                    'friends' => $requests,
                ));
            }
        } else {
            throw new CHttpException(403);
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     * @throws CHttpException
     */
    public function actionUpdate($id)
    {
        if(Yii::app()->user->checkAccess('updateSelf', array('userid' => $id))) {
            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);
            $avatarPath = Yii::app()->params['avatarPath'];
            $model = $this->loadModel($id);
            $avatarWidth = Yii::app()->params['avatarWidth'];
            $avatarHeight = Yii::app()->params['avatarHeight'];
            if (isset($_POST['User'])) {
                $_POST['User']['avatar'] = $model->avatar;
                $avatar = CUploadedFile::getInstance($model, 'avatar');
                $model->verifyCode = $_POST['User']['verifyCode'];
                $password = $model->hashPassword($_POST['User']['password']);
                $username = $_POST['User']['username'];
                $email = $_POST['User']['email'];

                //if changed username
                if ($model->username !== $username) {
                    $model->username = $username;
                } else {
                    unset($model->username);
                }

                //if changed email
                if ($model->email !== $email) {
                    $model->email = $email;
                } else {
                    unset($model->email);
                }

                //if changed password
                if (isset($_POST['User']['password'])) {
                    if ($model->password == $_POST['User']['password']) {
                        unset($model->password);
                    } elseif ($model->password !== $password) {
                        $model->password = $password;
                    } else {
                        unset($model->password);
                    }
                }

                //if changed avatar
                if (!empty($avatar)) { // check if uploaded file is set or not
                    if ("{$avatar}" == 'noavatar.png') { //if upload some image like noavatar.png
                        @unlink($avatarPath . $model->avatar);
                        $model->avatar = 'noavatar.png';
                    } else {
                        $tempimage = Yii::app()->image->load($avatar->tempName);
                        $tempvalues = getimagesize($avatar->tempName);
                        /**
                         * if the image size exceed the allowed (120x120)
                         * auto resize and retain proportion
                         * */
                        if($tempvalues[0] > $avatarWidth &&
                            $tempvalues[1] > $avatarHeight) {
                            $tempimage->resize($avatarWidth,$avatarHeight);
                        }

                        $rnd = rand(0, 9999); // generate random number between 0-9999
                        $fileName = "{$rnd}-{$avatar}"; // random number + file name
                        if ($model->avatar !== NULL) {
                            if ($model->avatar == "noavatar.png") {
                                //if noavatar then set new filename
                                $model->avatar = $fileName;
                            } else { //unline previous avatar and set new
                                @unlink($avatarPath . $model->avatar);
                                $model->avatar = $fileName;
                            }
                        }
                        $tempimage->save($avatarPath . $fileName);
                    }
                }

                //try/catch database errors
                try {
                    if ($model->save()) {
                        Yii::app()->user->setFlash('success', 'Your profile was updated successfully.');
                        $this->redirect(array('/frontend/profile/view/', 'id' => $model->id));
                    }
                } catch (CDbException $e) {
                    Yii::app()->user->setFlash('error', $e->getMessage());
                    $this->redirect(array('/frontend/profile/update/', 'id' => $model->id));
                }
            }
            $this->render('/user/update', array(
                'model' => $model,
            ));
        } else {
            throw new CHttpException(403);
        }

    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return User the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = User::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }
}

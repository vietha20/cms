<?php

class MessageController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $defaultAction = 'inbox';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow', // allow all users to perform 'view' actions
                'roles' => array('editor', 'user', 'admin'),
            ),
            array(
                'deny', // deny all guests
                'roles' => array('guest'),
            ),
        );
    }

    /**
     * @throws CHttpException
     */
    public function actionInbox($id)
    {
        if($id !== Yii::app()->user->id) {
            Yii::app()->user->setFlash('error', 'You cannot view someone else\'s inbox.');
            $this->redirect(array('/frontend/message/inbox', 'id' => Yii::app()->user->id));
        } else {
            $user = $this->loadUserModel($id);

            $criteria = new CDbCriteria();
            $criteria->select = 't.*';
            $criteria->condition = 'receiverid=:receiverid AND receiverdelete = 0';
            $criteria->order = 'sendtime DESC';
            $criteria->params = array(':receiverid' => $id);

            $count = Message::model()->count($criteria);
            $pages = new CPagination($count);

            // results per page
            $pages->pageSize = Yii::app()->params['postsPerPage'];
            $pages->applyLimit($criteria);
            $models = Message::model()->findAll($criteria);

            if (isset($_GET['page']) && $pages->getPageCount() < $_GET['page']) {
                throw new CHttpException(400);
            } else {
                $this->render('inbox', array(
                        'model' => $user,
                        'models' => $models,
                        'pages' => $pages,
                        'count' => $count,
                    )
                );
            }
        }
    }

    /**
     * @throws CHttpException
     */
    public function actionOutbox($id)
    {
        if($id !== Yii::app()->user->id) {
            Yii::app()->user->setFlash('error', 'You cannot view someone else\'s outbox.');
            $this->redirect(array('/frontend/message/outbox', 'id' => Yii::app()->user->id));
        } else {
            $user = $this->loadUserModel($id);

            $criteria = new CDbCriteria();
            $criteria->distinct = true;
            $criteria->select = 't.*';
            $criteria->condition = 'senderid=:senderid AND senderdelete=0';
            $criteria->order = 'sendtime DESC';
            $criteria->params = array(':senderid' => $id);

            $count = Message::model()->count($criteria);
            $pages = new CPagination($count);

            // results per page
            $pages->pageSize = Yii::app()->params['postsPerPage'];
            $pages->applyLimit($criteria);
            $models = Message::model()->findAll($criteria);

            if (isset($_GET['page']) && $pages->getPageCount() < $_GET['page']) {
                throw new CHttpException(400);
            } else {
                $this->render('outbox', array(
                        'model' => $user,
                        'models' => $models,
                        'pages' => $pages,
                        'count' => $count,
                    )
                );
            }
        }
    }

    public function actionDelete($id)
    {
        $model = $this->loadModel($id);

        if(Yii::app()->user->id == $model->senderid) {
            $url = '/frontend/message/outbox';
            $model->senderdelete = 1;
        } elseif (Yii::app()->user->id == $model->receiverid) {
            $url = '/frontend/message/inbox';
            $model->receiverdelete = 1;
        } else {
            throw new CHttpException(400);
        }

        if($model->save()) {
            Yii::app()->user->setFlash('success', 'The message was removed');
            $this->redirect(array($url, 'id' => Yii::app()->user->id));
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param $id
     * @throws CHttpException
     * @return mixed
     */
    public function loadModel($id)
    {
        $model = Message::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param $id
     * @return mixed
     * @throws CHttpException
     */
    public function loadUserModel($id)
    {
        $model = User::model()->findByPk(array('userid' => $id));
        if ($model === null) {
            throw new CHttpException(404);
        }
        return $model;
    }
}

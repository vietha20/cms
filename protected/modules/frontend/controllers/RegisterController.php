<?php

class RegisterController extends Controller
{
    public $defaultAction = 'register';
    public $layout = '//layouts/column2';

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'transparent' => 'true',
            ),
        );
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow', // allow normal user to update and view
                'roles' => array('guest'),
            ),
            array(
                'deny', // deny all users
                'roles' => array('user', 'admin'),
            ),
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionRegister()
    {
        $model = new User('register');
        $avatarWidth = Yii::app()->params['avatarWidth'];
        $avatarHeight = Yii::app()->params['avatarHeight'];
        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            $avatar = CUploadedFile::getInstance($model, 'avatar');
            $avatarPath = Yii::app()->params['avatarPath'];
            if ($model->validate()) {
                $unhashedPassword = $model->password;
                $model->password = $model->hashPassword($model->password);
                $model->registered = time();
                $model->roleid = 2;

                if (!empty($avatar)) {
                    $tempimage = Yii::app()->image->load($avatar->tempName);
                    $tempvalues = getimagesize($avatar->tempName);
                    /**
                     * if the image size exceed the allowed (120x120)
                     * auto resize and retain proportion
                     * */
                    if($tempvalues[0] > $avatarWidth &&
                        $tempvalues[1] > $avatarHeight) {
                        $tempimage->resize($avatarWidth,$avatarHeight);
                    }
                    $rnd = rand(0, 9999); // generate random number between 0-9999
                    $fileName = "{$rnd}-{$avatar}"; // random number + file name
                    $model->avatar = $fileName;
                    $tempimage->save($avatarPath . $fileName);
                } else {
                    $model->avatar = "noavatar.png";
                }

                //try/catch database errors
                try {
                    if ($model->save()) {
                        $auth = Yii::app()->authManager;
                        $auth->assign('user', $model->id);
                        $model->password = $unhashedPassword;
                        $model->login();
                        $this->redirect(array('/frontend/profile/view/', 'id' => $model->id));
                    }

                } catch (CDbException $e) {
                    Yii::app()->user->setFlash('error', $e->getMessage());
                    $this->redirect(array('/frontend/register'));
                }
            }
        }

        $this->render('/user/register', array(
            'model' => $model,
        ));
    }
}  


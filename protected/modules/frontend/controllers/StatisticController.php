<?php

class StatisticController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    protected $apikey = 'b84f7f3a67054c35a980e28cfea4cf18';
    public $defaultAction = 'airquality';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array( //remember to add here option for user and guests to be able to add
                //lyrics to the site. all the addition will be moderated.
                'allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'search'),
                'roles' => array('guest', 'user', 'admin'),
            ),
        );
    }

    /**
     * The default search will call private member functions, for instance
     * air quality in Linkoping during a period.
     */
    public function actionAirQuality()
    {
        if(isset($_POST['startdate']) && isset($_POST['enddate'])) {
            $this->actionSearchAirQuality($_POST['startdate'], $_POST['enddate'], $this->apikey);
        } else {
            $startdate = '2014-03-01';
            $enddate = '2014-03-03';
            $this->actionSearchAirQuality($startdate, $enddate, $this->apikey);
        }
    }

    /**
     * This function parse the XML and get the data.
     *
     * @param $startdate YYYY-MM-DD
     * @param $enddate YYYY-MM-DD
     */
    private function actionSearchAirQuality($startdate, $enddate) {
        if(empty($startdate) || empty($enddate)) {
            Yii::app()->user->setFlash('error', 'You need to fill in the parameters.');
            $this->redirect(array('/frontend/statistic'));
        } elseif(!(strtotime($startdate) < strtotime($enddate))) {
            Yii::app()->user->setFlash('error', 'You cannot pick that combination.');
            $this->redirect(array('/frontend/statistic'));
        } else {
            $url = "http://opendata.linkoping.se/ws_opendata/Main.asmx/LuftDataLista?CustomKey=$this->apikey&SystemCodeNumber=linkoping&from=$startdate&tom=$enddate";
            $qualities = simplexml_load_file($url, 'SimpleXMLElement', LIBXML_NOCDATA);

            $luftdata = array();
            $tidpunkt = array();
            foreach($qualities->ListaLuftDataobjekt as $ql) {
                foreach($ql as $temp) {
                    $tempval = explode(' ', $temp->LastUpdated);
                    $temptime = explode(':', $tempval[1]);
                    $tempdate = date('j M', strtotime($tempval[0]));
                    $luftdata[] = (float)$temp->PM10;
                    $tidpunkt[] = (string)$tempdate.', '.$temptime[0].':'.$temptime[1];
                }
            }

            $newluftdata = array();
            $newtidpunkt = array();

            foreach($luftdata as $key => $temp1) {
                if($key%5 == 0) {
                    $newluftdata[] = $temp1;
                }
            }

            foreach($tidpunkt as $key => $temp2) {
                if($key%5 == 0) {
                    $newtidpunkt[] = $temp2;
                }
            }

            if(count($newtidpunkt) > 45 || count($newluftdata) > 45) {
                Yii::app()->user->setFlash('error', 'Please narrow your search (3 days at a time is a good option)');
                $this->redirect(array('/frontend/statistic/airquality'));
            } else {
                $this->render('_airquality', array(
                    'luftdata' => array_reverse($newluftdata),
                    'tidpunkt' => array_reverse($newtidpunkt),
                    'startdate' => $startdate,
                    'enddate' => $enddate
                ));
            }
        }
    }


    /**
     * The data are imported from openFlight dat files. Index is a search form
     * which you can search for countries and the results will be displayed in
     * terms of airports/airlines belonging to a country queried.
     */
    public function actionAirportStatistic()
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 't.country=:country';
        $criteria->order = 't.airportname ASC';
        $countries = $this->actionSuggestCountry('airport');
        $cities = $this->actionSuggestCity('airport');

        if(isset($_GET['country'])) {
            if($_GET['country'] == '') {
                $this->redirect(array('statistic/airportstatistic'));
            } else {
                $criteria->condition = 't.country=:country';
                $criteria->params = array(':country' => $_GET['country']);

                $count = Airport::model()->count($criteria);

                $pages = new CPagination($count);
                $pages->pageSize = 15;
                $pages->applyLimit($criteria);

                $airports = Airport::model()->findAll($criteria);

                if (isset($_GET['page']) && $pages->getPageCount() < $_GET['page']) {
                    throw new CHttpException(400);
                } else {
                    $this->render('_airportstat', array(
                        'countries' => $countries,
                        'cities' => $cities,
                        'models' => $airports,
                        'count' => $count,
                        'pages' => $pages,
                    ));
                }
            }
        } elseif(isset($_GET['city'])) {
            if($_GET['city'] == '') {
                $this->redirect(array('statistic/airportstatistic'));
            } else {
                $criteria->condition = 't.city=:city';
                $criteria->params = array(':city' => $_GET['city']);

                $count = Airport::model()->count($criteria);

                $pages = new CPagination($count);
                $pages->pageSize = 15;
                $pages->applyLimit($criteria);

                $airports = Airport::model()->findAll($criteria);

                if (isset($_GET['page']) && $pages->getPageCount() < $_GET['page']) {
                    throw new CHttpException(400);
                } else {
                    $this->render('_airportstat', array(
                        'countries' => $countries,
                        'cities' => $cities,
                        'models' => $airports,
                        'count' => $count,
                        'pages' => $pages,
                    ));
                }
            }
        } else {
            $this->render('_formairport', array(
                'countries' => $countries,
                'cities' => $cities,
            ));
        }
    }

    /**
     * The data are imported from openFlight dat files. Index is a search form
     * which you can search for countries and the results will be displayed in
     * terms of airports/airlines belonging to a country queried.
     */
    public function actionAirlineStatistic()
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 't.country=:country';
        $criteria->order = 't.active DESC';
        $countries = $this->actionSuggestCountry('airline');
        if(isset($_GET['country'])) {
            $criteria->params = array(':country' => $_GET['country']);

            $count = Airline::model()->count($criteria);

            $pages = new CPagination($count);
            $pages->pageSize = 15;
            $pages->applyLimit($criteria);
            $airlines = Airline::model()->findAll($criteria);

            if (isset($_GET['page']) && $pages->getPageCount() < $_GET['page']) {
                throw new CHttpException(400);
            } else {
                $this->render('_airlinestat', array(
                    'countries' => $countries,
                    'models' => $airlines,
                    'count' => $count,
                    'pages' => $pages,
                ));
            }

        } else {
            $this->render('_formairline', array(
                'countries' => $countries,
            ));
        }
    }

    /**
     * Find all the countries either request from airport or
     * airline statistics.
     * @param $param
     * @return array
     */
    private function actionSuggestCountry($param) {
        $criteria = new CDbCriteria();
        $criteria->select = 'DISTINCT t.country';
        $criteria->order = 'country ASC';

        if($param == 'airport') {
            $temp = Airport::model()->findAll($criteria);
        } elseif ($param == 'airline') {
            $temp = Airline::model()->findAll($criteria);
        }

        $countries = array();
        foreach($temp as $t) {
            $countries[] = $t->country;
        }

        return $countries;
    }

    /**
     * Find all the cities either request from airport or
     * airline statistics.
     * @param $param
     * @return array
     */
    private function actionSuggestCity($param) {
        $criteria = new CDbCriteria();
        $criteria->select = 'DISTINCT t.city';
        $criteria->order = 'city ASC';

        if($param == 'airport') {
            $temp = Airport::model()->findAll($criteria);
        } elseif ($param == 'airline') {
            $temp = Airline::model()->findAll($criteria);
        }

        $cities = array();
        foreach($temp as $t) {
            $cities[] = $t->city;
        }

        return $cities;
    }

}

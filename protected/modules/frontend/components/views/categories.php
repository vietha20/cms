<?php foreach ($this->getCategories() as $category): ?>
    <?php $this->widget(
        'booster.widgets.TbMenu', array(
        'type' => 'list',
        'items' => array(
            array(
                'label' => $category->name . ' (' . count($category->posts) . ')',
                'url' => $category->getUrl(),
                'icon' => 'fa fa-folder-open-o',
            ),
        )
    )); ?>
<?php endforeach; ?>

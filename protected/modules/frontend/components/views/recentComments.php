<?php foreach ($this->getRecentComments() as $comment): ?>
    <?php $this->widget(
        'booster.widgets.TbMenu', array(
        'type' => 'list',
        'stacked' => true,
        'items' => array(
            array(
                'label' => $comment->authorLink . ' on ' . $comment->post->title,
                'url' => $comment->getUrl(),
                'icon' => 'fa fa-comments-o',
            ),
        )
    )); ?>
<?php endforeach; ?>

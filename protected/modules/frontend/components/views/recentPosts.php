<?php foreach ($this->getRecentPosts() as $post): ?>
    <?php $this->widget(
        'booster.widgets.TbMenu', array(
        'type' => 'list',
        'stacked' => true,
        'items' => array(
            array(
                'label' => $post->title,
                'url' => $post->getUrl(),
                'icon' => 'fa fa-file-text-o',
            )
        )
    )); ?>
<?php endforeach; ?>


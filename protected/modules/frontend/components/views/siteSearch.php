<?php
echo CHtml::beginForm($this->getController()->createUrl('post/search'));
echo CHtml::activeTextField($form, 'keyword', array(
    'value' => 'Search...press Enter',
    'onclick' => "this.value=''",
    'size' => 43,
    'onblur' => "if(this.value=='') this.value='Search...press Enter';"));
echo CHtml::endForm();

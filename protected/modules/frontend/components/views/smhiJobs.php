<?php $jobs = $this->getJobFeed(); ?>

<div class="title">
    <?php echo '<h4>' . $jobs->title . '</h4>'; ?>
</div>

<?php
$items = $jobs->item;
?>
<div id="smhiJobs">
    <?php foreach ($items as $item): ?>
        <ul>
            <li class="title">
                <?php echo $item->title; ?> |
                <a target="_blank" href="<?php echo $item->guid; ?>">Permanent länk</a>
            </li>
        </ul>
    <?php endforeach; ?>
</div>
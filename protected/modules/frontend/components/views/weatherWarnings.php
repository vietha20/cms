<?php $warnings = $this->getWeatherWarnings(); ?>

<div class="title">
    <?php echo '<h4>' . $warnings->title . '</h4>'; ?>
</div>

<?php
$items = $warnings->item;
?>
<div id="weatherWarnings">
    <?php foreach ($items as $item): ?>
        <ul>
            <li class="title">
                <?php echo $item->title; ?>
                <?php echo $item->description; ?>
                <?php echo 'Last update: ' . $item->pubDate; ?>
            </li>
        </ul>
    <?php endforeach; ?>
</div>
<?php

Yii::import('zii.widgets.CPortlet');

class SiteSearch extends CPortlet
{

    public $title = '<h4>Site Search</h4>';

    public function renderContent()
    {
        $form = new SearchForm;

        $this->render('siteSearch', array('form' => $form));

    }
}

<?php

Yii::import('zii.widgets.CPortlet');

class Frontend extends CPortlet
{
    protected function renderContent()
    {
        $module = Yii::app()->controller->module->id;

        if ($module == 'frontend') {
            $this->widget('SiteSearch');
            $this->widget('Categories');
            $this->widget('RecentPosts');
            $this->widget('RecentComments', array('maxComments'=>Yii::app()->params['recentCommentCount'],));
            //$this->widget('SmhiJobs');
            //$this->widget('WeatherWarnings');

        }
    }
}

<?php

Yii::import('zii.widgets.CPortlet');

class Categories extends CPortlet
{
    public $title = '<h4>Categories</h4>';

    public function getCategories()
    {
        return Category::model()->findAll();
    }

    protected function renderContent()
    {
        $this->title = Yii::t('lan', $this->title);
        $this->render('categories');
    }
}

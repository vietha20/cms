<?php
Yii::import('zii.widgets.CPortlet');

class WeatherWarnings extends CPortlet
{
    public function getWeatherWarnings()
    {
        $url = 'http://www.smhi.se/weatherSMHI2/varningar/smhi_alla_varningar.xml';
        $warnings = simplexml_load_file($url, 'SimpleXMLElement', LIBXML_NOCDATA);

        $channel = $warnings->channel;

        return $channel;
    }

    protected function renderContent()
    {
        $this->render('weatherWarnings');
    }
}

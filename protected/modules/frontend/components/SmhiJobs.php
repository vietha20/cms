<?php
Yii::import('zii.widgets.CPortlet');

class SmhiJobs extends CPortlet
{
    public function getJobFeed()
    {
        $url = 'http://www.smhi.se/cm/lediga-tjanster-pa-smhi-1.12180?polopolylocale=sv_SE';
        $jobs = simplexml_load_file($url, 'SimpleXMLElement', LIBXML_NOCDATA);

        $channel = $jobs->channel;

        return $channel;
    }

    protected function renderContent()
    {
        $this->render('smhiJobs');
    }

}

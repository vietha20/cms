<?php

Yii::import('zii.widgets.CPortlet');

class TagCloud extends CPortlet
{
    public $title = '<h4>Tags Cloud</h4>';
    public $maxTags = 20;

    protected function renderContent()
    {
        $tags = Tag::model()->findTagWeights($this->maxTags);

        foreach ($tags as $tag => $weight) {
            $link = CHtml::link(CHtml::encode($tag), array('/frontend/post/tag', 'tag' => $tag));
            echo CHtml::tag('span', array(
                    'class' => 'tag',
                    'style' => "font-size:{$weight}pt",
                ), $link) . "\n";
        }
    }
}
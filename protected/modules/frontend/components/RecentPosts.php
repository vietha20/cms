<?php

Yii::import('zii.widgets.CPortlet');

class RecentPosts extends CPortlet
{
    public $title = '<h4>Recent Posts</h4>';

    public function getRecentPosts()
    {
        return Post::model()->findRecentPosts();
    }

    protected function renderContent()
    {
        $this->render('recentPosts');
    }
}

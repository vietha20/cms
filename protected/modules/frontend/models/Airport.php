<?php

/**
 * This is the model class for table "{{airport}}".
 *
 * The followings are the available columns in table '{{airport}}':
 * @property integer $id
 * @property string $airportname
 * @property string $city
 * @property string $country
 * @property string $iata
 * @property string $icao
 * @property string $latitude
 * @property string $longitude
 * @property integer $altitude
 * @property string $timezone
 * @property string $dst
 */
class Airport extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{airport}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('airportname, city, country, latitude, longitude, altitude, timezone, dst', 'required'),
            array('altitude', 'numerical', 'integerOnly'=>true),
            array('airportname, city, country', 'length', 'max'=>120),
            array('iata', 'length', 'max'=>3),
            array('icao', 'length', 'max'=>4),
            array('latitude, longitude', 'length', 'max'=>10),
            array('timezone', 'length', 'max'=>2),
            array('dst', 'length', 'max'=>1),
            // The following rule is used by search().
            array('id, airportname, city, country, iata, icao, latitude, longitude, altitude, timezone, dst', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('frontendmodel', 'ID'),
            'airportname' => Yii::t('frontendmodel', 'Airport Name'),
            'city' => Yii::t('frontendmodel', 'City'),
            'country' => Yii::t('frontendmodel', 'Country'),
            'iata' => Yii::t('frontendmodel', 'IATA'),
            'icao' => Yii::t('frontendmodel', 'ICAO'),
            'latitude' => Yii::t('frontendmodel', 'Latitude'),
            'longitude' => Yii::t('frontendmodel', 'Longitude'),
            'altitude' => Yii::t('frontendmodel', 'Altitude'),
            'timezone' => Yii::t('frontendmodel', 'Timezone'),
            'dst' => Yii::t('frontendmodel', 'DST'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('airportname',$this->airportname,true);
        $criteria->compare('city',$this->city,true);
        $criteria->compare('country',$this->country,true);
        $criteria->compare('iata',$this->iata,true);
        $criteria->compare('icao',$this->icao,true);
        $criteria->compare('latitude',$this->latitude,true);
        $criteria->compare('longitude',$this->longitude,true);
        $criteria->compare('altitude',$this->altitude);
        $criteria->compare('timezone',$this->timezone,true);
        $criteria->compare('dst',$this->dst,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Airport the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
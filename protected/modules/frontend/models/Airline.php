<?php

/**
 * This is the model class for table "{{airline}}".
 *
 * The followings are the available columns in table '{{airline}}':
 * @property integer $id
 * @property string $airlinename
 * @property string $alias
 * @property string $iata
 * @property string $icao
 * @property string $callsign
 * @property string $country
 * @property string $active
 */
class Airline extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{airline}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('airlinename, alias, iata, icao, callsign, country, active', 'required'),
            array('airlinename, alias, country', 'length', 'max'=>64),
            array('iata', 'length', 'max'=>2),
            array('icao', 'length', 'max'=>3),
            array('callsign', 'length', 'max'=>20),
            array('active', 'length', 'max'=>1),
            // The following rule is used by search().
            array('id, airlinename, alias, iata, icao, callsign, country, active', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('frontendmodel', 'ID'),
            'airlinename' => Yii::t('frontendmodel', 'Airline Name'),
            'alias' => Yii::t('frontendmodel', 'Airline Alias'),
            'iata' => Yii::t('frontendmodel', 'IATA'),
            'icao' => Yii::t('frontendmodel', 'ICAO'),
            'callsign' => Yii::t('frontendmodel', 'Callsign'),
            'country' => Yii::t('frontendmodel', 'Country'),
            'active' => Yii::t('frontendmodel', 'Active'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('airlinename',$this->airlinename,true);
        $criteria->compare('alias',$this->alias,true);
        $criteria->compare('iata',$this->iata,true);
        $criteria->compare('icao',$this->icao,true);
        $criteria->compare('callsign',$this->callsign,true);
        $criteria->compare('country',$this->country,true);
        $criteria->compare('active',$this->active,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Airline the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
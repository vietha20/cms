<?php

/**
 * This is the model class for table "{{message}}".
 *
 * The followings are the available columns in table '{{message}}':
 * @property integer $id
 * @property integer $senderid
 * @property integer $receiverid
 * @property string $title
 * @property string $content
 * @property integer $sendtime
 * @property integer $viewtime
 * @property integer $viewed
 *
 * The followings are the available model relations:
 * @property User $receiver
 * @property User $sender
 */
class Message extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{message}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('senderid, receiverid, title, content, sendtime', 'required'),
            array('senderid, receiverid, sendtime', 'numerical', 'integerOnly'=>true),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'receiver' => array(self::BELONGS_TO, 'User', 'receiverid'),
            'sender' => array(self::BELONGS_TO, 'User', 'senderid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('frontendmodel', 'ID'),
            'senderid' => Yii::t('frontendmodel', 'Sender ID'),
            'receiverid' => Yii::t('frontendmodel', 'Receiver ID'),
            'title' => Yii::t('frontendmodel', 'Title'),
            'content' => Yii::t('frontendmodel', 'Content'),
            'sendtime' => Yii::t('frontendmodel', 'Send Time'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Message the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
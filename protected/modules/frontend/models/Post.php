<?php

class Post extends CActiveRecord
{
    /**
     * The followings are the available columns in table 'tbl_post':
     * @var integer $id
     * @var string $title
     * @var string $content
     * @var string $tags
     * @var integer $status
     * @var integer $createtime
     * @var integer $updatetime
     * @var integer $authorid
     */
    const STATUS_DRAFT = 1;
    const STATUS_PUBLISHED = 2;
    const STATUS_ARCHIVED = 3;

    private $_oldTags;

    public $verifyCode;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className
     * @return mixed
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{post}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, content, status, categoryid', 'required'),
            array('status', 'in', 'range' => array(1, 2, 3)),
            array('title', 'length', 'max' => 128),
            array('tags', 'match', 'pattern' => '/^[\w\s,]+$/',
                'message' => 'Tags can only contain word characters.'),
            array('tags', 'normalizeTags'),
            array('categoryid', 'safe'),
            array('title, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'author' => array(self::BELONGS_TO, 'User', 'authorid'),
            'category' => array(self::BELONGS_TO, 'Category', 'categoryid'),
            'comments' => array(self::HAS_MANY, 'Comment', 'postid',
                'condition' => 'comments.status=' . Comment::STATUS_APPROVED,
                'order' => 'comments.createtime DESC'),
            'commentCount' => array(self::STAT, 'Comment', 'postid',
                'condition' => 'status=' . Comment::STATUS_APPROVED),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('frontendmodel', 'ID'),
            'title' => Yii::t('frontendmodel', 'Title'),
            'content' => Yii::t('frontendmodel', 'Content'),
            'tags' => Yii::t('frontendmodel', 'Tags'),
            'status' => Yii::t('frontendmodel', 'Status'),
            'createtime' => Yii::t('frontendmodel', 'Create Time'),
            'updatetime' => Yii::t('frontendmodel', 'Update Time'),
            'authorid' => Yii::t('frontendmodel', 'Author'),
            'categoryid' => Yii::t('frontendmodel', 'Category ID'),
        );
    }

    /**
     * @return string the URL that shows the detail of the post
     */
    public function getUrl()
    {
        $module = strtolower(Yii::app()->getModule('frontend')->id);
        $controller = get_class($this);
        $controller[0] = strtolower($controller[0]);
        $params = array('id' => $this->id);

        return Yii::app()->urlManager->createUrl('/' . $module . '/' . $controller . '/view', $params);
    }

    /**
     * @return array a list of links that point to the post list filtered by every tag of this post
     */
    public function getTagLinks()
    {
        $links = array();
        $module = strtolower(Yii::app()->getModule('frontend')->id);
        $controller = get_class($this);
        $controller[0] = strtolower($controller[0]);

        foreach (Tag::string2array($this->tags) as $tag)
            $links[] = CHtml::link(CHtml::encode($tag), array('/' . $module . '/' . $controller . '/tag', 'tag' => $tag));
        return $links;
    }

    /**
     * Normalizes the user-entered tags.
     */
    public function normalizeTags()
    {
        $this->tags = Tag::array2string(array_unique(Tag::string2array($this->tags)));
    }

    /**
     * Adds a new comment to this post.
     * This method will set status and postid of the comment accordingly.
     * @param Comment the comment to be added
     * @return boolean whether the comment is saved successfully
     */
    public function addComment($comment)
    {
        if (Yii::app()->params['commentNeedApproval'] &&
            Yii::app()->user->checkAccess('guest')
        )
            $comment->status = Comment::STATUS_PENDING;
        else
            $comment->status = Comment::STATUS_APPROVED;
        $comment->postid = $this->id;
        return $comment->save();
    }

    /**
     * This is invoked when a record is populated with data from a find() call.
     */
    protected function afterFind()
    {
        parent::afterFind();
        $this->_oldTags = $this->tags;
    }

    /**
     * This is invoked before the record is saved.
     * @return boolean whether the record should be saved.
     */
    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->isNewRecord) {
                $this->createtime = $this->updatetime = time();
                $this->authorid = Yii::app()->user->id;
            } else {
                $this->updatetime = time();
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * This is invoked after the record is saved.
     */
    protected function afterSave()
    {
        parent::afterSave();
        Tag::model()->updateFrequency($this->_oldTags, $this->tags);
    }

    /**
     * This is invoked after the record is deleted.
     */
    protected function afterDelete()
    {
        parent::afterDelete();
        Comment::model()->deleteAll('postid=' . $this->id);
        Tag::model()->updateFrequency($this->tags, '');
    }

    /**
     * Retrieves the list of posts based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the needed posts.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('title', $this->title, true);
        $criteria->compare('status', $this->status, true);

        return new CActiveDataProvider('Post', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'status, updatetime DESC',
            ),
        ));
    }

    /**
     * @param integer the maximum number of comments that should be returned
     * @return array the most recently added comments
     */
    public function findRecentPosts($limit = 10)
    {
        $criteria = array(
            'condition' => 'status=' . self::STATUS_PUBLISHED,
            'order' => 'createtime DESC',
            'limit' => $limit,
        );
        return $this->findAll($criteria);
    }

    /**
     * @return null or the id
     */
    public function getNextId()
    {
        $record=self::model()->find(array(
            'condition' => 'id>:current_id',
            'order' => 'id ASC',
            'limit' => 1,
            'params'=>array(':current_id'=>$this->id),
        ));
        if($record!==null)
            return $record->id;
        return null;
    }

    /**
     * @return null or the id
     */
    public function getPreviousId()
    {
        $record=self::model()->find(array(
            'condition' => 'id<:current_id',
            'order' => 'id DESC',
            'limit' => 1,
            'params'=>array(':current_id'=>$this->id),
        ));
        if($record!==null)
            return $record->id;
        return null;
    }
}

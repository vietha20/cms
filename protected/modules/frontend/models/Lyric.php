<?php

/**
 * This is the model class for table "{{lyric}}".
 *
 * The followings are the available columns in table '{{lyric}}':
 * @property integer $id
 * @property string $title
 * @property string $artist
 * @property string $content
 * @property string $sendername
 * @property string $senderinfo
 * @property integer $postdate
 * @property integer $lyriccount
 * @property string $file
 */
class Lyric extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{lyric}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, artist, content, sendername, senderemail, postdate, file', 'required'),
            array('postdate, lyriccount', 'numerical', 'integerOnly'=>true),
            array('title, artist, sendername', 'length', 'max'=>80),
            array('senderemail', 'length', 'max'=>100),
            // The following rule is used by search().
            array('id, title, artist, content, postdate, lyriccount, file', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('frontendmodel', 'ID'),
            'title' => Yii::t('frontendmodel', 'Title'),
            'artist' => Yii::t('frontendmodel', 'Artist'),
            'content' => Yii::t('frontendmodel', 'Content'),
            'sendername' => Yii::t('frontendmodel', 'Sender'),
            'postdate' => Yii::t('frontendmodel', 'Create Time'),
            'lyriccount' => Yii::t('frontendmodel', 'Views'),
            'file' => Yii::t('frontendmodel', 'Music '),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('artist',$this->artist,true);
        $criteria->compare('content',$this->content,true);
        $criteria->compare('sendername',$this->sendername,true);
        $criteria->compare('senderinfo',$this->senderinfo,true);
        $criteria->compare('postdate',$this->postdate);
        $criteria->compare('lyriccount',$this->lyriccount);
        $criteria->compare('file',$this->file,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Lyric the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
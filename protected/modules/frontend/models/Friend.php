<?php

/**
 * This is the model class for table "{{userfriend}}".
 *
 * The followings are the available columns in table '{{userfriend}}':
 * @property integer $id
 * @property integer $userid
 * @property integer $friendid
 * @property integer $accepted
 */
class Friend extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{friend}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('userid, friendid', 'required'),
            array('userid, friendid', 'numerical', 'integerOnly'=>true),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'userid'),
            'friend' => array(self::BELONGS_TO, 'User', 'friendid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'userid' => Yii::t('frontendmodel', 'User ID'),
            'friendid' => Yii::t('frontendmodel', 'Friend ID'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Friend the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Checks if two users are friend or not.
     * @param $userid
     * @param $friendid
     * @return bool
     */
    public function isFriend($userid, $friendid)
    {
        if($userid == $friendid) { //same id => can't be friend with yourself
            return true;
        } elseif(!is_null(self::model()->findByAttributes(array('userid'=>$userid, 'friendid'=>$friendid, 'accepted'=>1)))) {
            return true; //if realationship already exists return true.
        } elseif (!is_null(self::model()->findByAttributes(array('userid'=>$friendid, 'friendid'=>$userid, 'accepted'=>1)))) {
            return true; //if realationship already exists return true.
        } elseif (!is_null(self::model()->findByAttributes(array('userid'=>$userid, 'friendid'=>$friendid, 'accepted'=>0)))) {
            return true; //if realationship is already sent return false.
        } elseif (!is_null(self::model()->findByAttributes(array('userid'=>$friendid, 'friendid'=>$userid, 'accepted'=>0)))) {
            return true; //if realationship is already sent return false.
        } else {
            //or else establish a new relationship
            return false;
        }
    }

}
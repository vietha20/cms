<?php

// Call set_include_path() as needed to point to your client library.
set_include_path("src/" . PATH_SEPARATOR . get_include_path());
require_once 'Google/Client.php';
require_once 'Google/Service/YouTube.php';

/*
 * Set $DEVELOPER_KEY to the "API key" value from the "Access" tab of the
 * Google Developers Console <https://console.developers.google.com/>
 * Please ensure that you have enabled the YouTube Data API for your project.
 */
$DEVELOPER_KEY = 'AIzaSyA7s0xwUTeujkuN_O2CSZIlgsryuUGvZkU';

$client = new Google_Client();
$client->setDeveloperKey($DEVELOPER_KEY);

// Define an object that will be used to make all API requests.
$youtube = new Google_Service_YouTube($client);

if (empty($_GET)) {
    // This code will execute if the user entered a search query in the form
    // and submitted the form. Otherwise, the page displays the form above.
    try {
        // Call the search.list method to retrieve results matching the specified
        // query term.
        $searchResponse = $youtube->videos->listVideos('snippet', array(
            'chart' => 'mostPopular',
            'maxResults' => '50',
        ));

        $videos = array();

        // Add each result to the appropriate list, and then display the lists of
        // matching videos, channels, and playlists.
        foreach ($searchResponse['items'] as $searchResult) {
            //width = 120, height = 90
            $videos[$searchResult['id']]['title'] = $searchResult['snippet']['title'];
            $videos[$searchResult['id']]['image'] = $searchResult['snippet']['thumbnails']['default']['url'];
            $videos[$searchResult['id']]['description'] = $searchResult['snippet']['description'];
        }

    } catch (Google_ServiceException $e) {
        $htmlBody .= sprintf('<p>A service error occurred: <code>%s</code></p>',
            htmlspecialchars($e->getMessage()));
    } catch (Google_Exception $e) {
        $htmlBody .= sprintf('<p>An client error occurred: <code>%s</code></p>',
            htmlspecialchars($e->getMessage()));
    }
}

if($_POST['q']) {
    // This code will execute if the user entered a search query in the form
    // and submitted the form. Otherwise, the page displays the form above.
    try {
        // Call the search.list method to retrieve results matching the specified
        // query term.
        $searchResponse = $youtube->search->listSearch('id,snippet', array(
            'part' => 'snippet',
            'q' => $_POST['q'],
            'maxResults' => '50',
        ));

        $videos = array();
        foreach ($searchResponse['items'] as $searchResult) {
            $modelData = $searchResult['modelData'];
            $videoId = $modelData['id']['videoId'];
            $videoTitle = $modelData['snippet']['title'];

            //width = 120, height = 90
            $videos[$videoId]['title'] = $modelData['snippet']['title'];
            $videos[$videoId]['image'] = $modelData['snippet']['thumbnails']['default']['url'];
            $videos[$videoId]['description'] = $modelData['snippet']['description'];
        }

    } catch (Google_ServiceException $e) {
        $htmlBody .= sprintf('<p>A service error occurred: <code>%s</code></p>',
            htmlspecialchars($e->getMessage()));
    } catch (Google_Exception $e) {
        $htmlBody .= sprintf('<p>An client error occurred: <code>%s</code></p>',
            htmlspecialchars($e->getMessage()));
    }
} ?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <style type="text/css">
        body {
            color: #333;
            padding: 0 0 30px 0;
            margin: 0 auto;
            overflow-y: scroll;
        }
        #description {
            margin-top: 375px;
            height: 250px;
            width: 610px;
            left: 485px;
            position: absolute;
            overflow-y: scroll;
            overflow-x: hidden;
        }
        #resultlist {
            padding: 2px;
            overflow-y: scroll;
            overflow-x: hidden;
            height: 600px;
            width: 510px;
            position: absolute;
        }
        li {
            display: inline;
            overflow: hidden;
        }
        .player {
            padding-top: 17px;
            height: 350px;
            width: 615px;
            left: 520px;
            position: absolute;
        }
        .wrapper {
            width: 100%;
            margin: 0 auto;
            padding: 0;
            overflow: auto;
        }
        #leftcolumn {
            width: auto;
            padding: 0;
            margin: 0;
            display: block;
            position: fixed;
        }
        #rightcolumn {
            width: auto;
            display: block;
            float: right;
        }
        #q {
            background-color: #fff;
            padding: 0 11px 0 13px;
            width: 355px;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            text-overflow: ellipsis;
            padding: 2px;
            display: inline;
        }
    </style>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
    <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.min.js"></script>
    <script>
        var tag = document.createElement('script');

        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        var player;
        var videoId = 'M7lc1UVf-VE';

        function onPlayerReady(event) {
            event.target.playVideo();
        }

        function onYouTubeIframeAPIReady() {
            player = new YT.Player('player', {
                height: '390',
                width: '640',
                videoId: videoId
            });
        }

        function stopVideo() {
            player.stopVideo();
        }

        $(function() {
            $("img").click(function() {
                videoId = $(this).prop('id');
                var title = $(this).prop('title');
                var description = $(this).prop('alt');
                player.loadVideoById(videoId);
                console.log(description);
                $('li.description').empty().html("<b>Title: </b>" + title + "<br> <b>Description: </b>" + description);

            });
        });
    </script>
    <title>YouTube Search</title>
</head>
<body>
<div class="wrapper">
    <div id="leftcolumn">
        <form method="POST">
            Search Term: <input type="search" id="q" name="q" placeholder="Enter Search Term">
        </form>
    </div>
    <div id="rightcolumn">
        <a class="button" href="#" onClick="document.location.reload(true); return false;">
            <button><i class="fa fa-star"></i> Most Popular</button>
        </a>
    </div>
</div>
<div class="wrapper">
    <ul id="resultlist">
        <?php foreach($videos as $key => $value) : ?>
        <li id="resultitem:<?php echo $key; ?>" data="<?php echo $key; ?>">
            <a href="#" onClick="return false;">
                <img id="<?php echo $key; ?>"
                     src="<?php echo $value['image']; ?>"
                     title="<?php echo $value['title']; ?>"
                     alt="<?php echo $value['description']; ?>">
            </a>
            <?php endforeach; ?>
        </li>
    </ul>
    <ul id="description">
        <li class="description"></li>
    </ul>
    <div id="player" class="player"></div>
</div>
</body>
</html>

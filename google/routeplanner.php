<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <style>
        body {
            color: #333;
            padding: 0 0 30px 0;
            margin: 0px auto;
            overflow-y: auto;
        }
        #map_canvas {
            height: 400px;
            background: #FFFFFF;
            width: 950px;
            padding: 20px;
            margin: 10px auto;
            box-shadow: 0px 0px 2px #999;
            border-radius: 10px;
            font-size: 20px;
        }
        #directionsPanel, #routeForm {
            background: #FFFFFF;
            width: 950px;
            padding: 20px;
            margin: 10px auto;
            box-shadow: 0px 0px 2px #999;
            border-radius: 10px;
            font-size: 20px;
        }
        .controls {
            margin-top: 16px;
            border: 1px solid transparent;
            border-radius: 2px 0 0 2px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            height: 32px;
            outline: none;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        }
        #routeEnd, #routeStart {
            background-color: #fff;
            padding: 0 11px 0 13px;
            width: 355px;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            text-overflow: ellipsis;
            padding: 2px;
            display: inline;
        }
        select, input[type="submit"] {
            padding: 4px;
            font-size: 15px;
            margin-left: 2px;
            display: inline;
        }
        .pac-container {
            font-family: Roboto;
        }
        .clear-both {
            clear: both;
        }
    </style>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&sensor=false"></script>
    <script type="text/javascript">
        var directionDisplay;
        var directionsService = new google.maps.DirectionsService();

        function initialize() {
            //the first loaded place
            var latlng = new google.maps.LatLng(58.4116315,15.6189274);
            directionsDisplay = new google.maps.DirectionsRenderer();
            var myOptions = {
                zoom: 14,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false
            };
            var map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
            directionsDisplay.setMap(map);
            directionsDisplay.setPanel(document.getElementById("directionsPanel"));

            var start = /** @type {HTMLInputElement} */(
                document.getElementById('routeStart'));
            var end = /** @type {HTMLInputElement} */(
                document.getElementById('routeEnd'));

            var autocompletestart = new google.maps.places.Autocomplete(start);
            var autocompleteend = new google.maps.places.Autocomplete(end);

            autocompletestart.bindTo('bounds', map);
            autocompleteend.bindTo('bounds', map);
        }

        function calcRoute() {
            var start = document.getElementById("routeStart").value;
            var end = document.getElementById("routeEnd").value;

            var travelMode = $("select option:selected").val();

            var request = {
                origin:start,
                destination:end,
                travelMode: google.maps.DirectionsTravelMode[travelMode]
            };

            directionsService.route(request, function(response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                }
            });
        }

        $(document).ready(function(){
            initialize();
        });
    </script>
</head>
<body onload="initialize()">
<div id="map_canvas"></div>
<form onsubmit="calcRoute();return false;" id="routeForm">
    <input type="text" id="routeStart" value="" placeholder="Start Location">
    <input type="text" id="routeEnd" value="" placeholder="End Location">
    <select name="travelmode">
        <option value="DRIVING">DRIVING</option>
        <option value="BICYCLING">BICYCLING</option>
        <option value="WALKING">WALKING</option>
    </select>
    <input type="submit" value="Find Route">
</form>
<div id="directionsPanel"></div>
</body>
</html>
